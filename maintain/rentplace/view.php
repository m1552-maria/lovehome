<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>list.css">
	<title><?=$pageTitle?></title>
	<style type="text/css">
		#u_setting{
			display:none;
			overflow: auto;
			height:250px; 
			width:400px; 
			position: absolute;
			top:80px;
			left:120px;
			background-color:#dedede;
			border:1px solid #666;
			padding:0px;
			margin:0px;
			font-size:16px;
		}
		#statusDiv, #isPayDiv{
	        position:absolute;
	        left: 50%; top:50%;
	        background-color:#888;
	        padding: 20px;
	        display:none;
	        margin-left: -153px;
	        margin-top: -150px;
	    }
	</style>
	<script type="text/javascript" src="/Scripts/jquery.min.js"></script>
	<script>
		/*** forward php variants to javascript ***/
		var cp = <?=$pages?>;		//目前所在頁次
		var selid = '<?=$selid?>';	
		var keyword = '<?=$keyword?>';
		var cancelFG = true;

		function changeDate(obj){
			$("[name='eDate']").val($(obj).val());
		}
		
		function goPage(p) {
			var pp;
			switch(p) {
				case 0 : pp=0; break;
				case 1 : pp=<?=$pages?>-1; if(pp<0) pp=0; break;
				case 2 : pp=<?=$pages?>+1; if(pp><?=$pageCount?>) pp=<?=$pageCount?>; break;
				case 3 : pp=<?=$pageCount?>;
			}
			location.href="?pages="+pp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>";
		}
		function DoEdit() { 
			if(selid==-1) alert('請選擇一個項目！'); 
			else location.href='edit.php?ID='+selid+"&pages="+cp+"&keyword=<?=$keyword?>&fieldname=<?=$fieldname?>&sortDirection=<?=$sortDirection?>"; 
		}
		function clickEdit(aid) {
			selid = aid;
			DoEdit();
		}	
		function doSubmit() {
			form1.submit();
		}
		function doExcel(){
			$('#u_setting').show();
		}
		function statusDiv(status, id, statusNote){
			$('#statusDiv').show();
			$("[name='status']").val(status);
			$("[name='mid']").val(id);
			$("[name='statusNote']").val(statusNote);
		}
		function isPayDiv(isPay, id, price, fold){
			$('#isPayDiv').show();
			$("[name='isPay']").val(isPay);
			$("[name='pid']").val(id);
			$("[name='price']").val(price);
			$("[name='fold']").val(fold);
		}
		function statusSubmit(){
			var formData = new FormData($('#statusFm')[0]); 
			$.ajax({
                url: "api.php",
                type: 'POST',
                data: formData,
                dataType: 'json',

                processData : false, 
				contentType : false,
                success: function(d){                    
                	if(d == 1){
                        location.reload();
                    }
                    if(d == 'error'){
                    	alert("請勿上傳此種格式的檔案");
                    	location.reload();
                    }
                }
            });
		}
		function isPaySubmit(){
			$.ajax({
                url: "api.php",
                type: 'POST',
                data: {
                	mode : 'isPay',
                	id : $("[name='pid']").val(),
                	isPay : $("[name='isPay']").val(),
                	price : $("[name='price']").val(),
                	fold  : $("[name='fold']").val()
                },
                dataType: 'json',
                success: function(d){
                	console.log(d);
                    if(d == 1){
                        location.reload();
                    }
                }
            });
		}
	</script>
	<script Language="JavaScript" src="<?=$extfiles?>list.js"></script> 
</head>

<body>
	<a name="top"/>
	<form name="form1" method="post" action="list.php" onSubmit="return Form1_Validator(this);" style="margin:0">
		<table width="100%">
			<tr><td><font class="headTX"><?php echo $pageTitle?></font></td></tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name="keyword" value="">
					<input type="button" value="第一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(0)">
					<input type="button" value="前一頁" class="<?=$pages>0?'sBtn':'suBtn'?>" onClick="goPage(1)">
					<input type="button" value="下一頁" class="<?=$pages<$pageCount?'sBtn':'suBtn'?>" onClick="goPage(2)">
					<input type="button" value="最末頁" class="<?=$pages!=$pageCount?'sBtn':'suBtn'?>" onClick="goPage(3)">
					<input type="button" name="search" value="◎搜尋" class="sBtn" onClick="SearchKeyword();">
					<?php if ($_SESSION['privilege'] >= 16) { ?>
					<input type="button" name="append" value="＋新增" class="<?=$bE?'sBtn':'suBtn'?>" onclick='location.href="append.php";'>
					<input type="button" name="edit" value="－修改" class="<?=$bE?'sBtn':'suBtn'?>" onClick="DoEdit()">
					<input type="submit" name="action" value="Ｘ刪除" class="<?=$bE?'sBtn':'suBtn'?>" onClick="MsgBox()">
					<input type="button" value="匯出統計excel" onClick="excelForm.submit()">
					<input type="button" value="匯出申請清單excel" onClick="excelForm2.submit()">
					<?php } ?>
					<select name="places">
						<option value='all'>-全部場地-</option>
						<?php 
							if(isset($_SESSION['places'])){
								if(!empty($_REQUEST['places'])) $_SESSION['places'] = $_REQUEST['places'];
								$placesStr = $_SESSION['places'];
							}else{
								$placesStr = isset($_REQUEST['places']) ? $_REQUEST['places'] : '';
								$_SESSION['places'] = $placesStr;
								
							}

							foreach ($places as $key => $value) {
								if($placesStr == $key) $seled = "selected";
								else $seled = "";
								echo "<option value='$key' $seled>$value</option>";
							}

							$bDate = date("Y-m-d");
							$eDate = date("Y-m-d");

							if(isset($_SESSION['bDate'])){
								if(!empty($_REQUEST['bDate'])) $_SESSION['bDate'] = $_REQUEST['bDate'];
								$bDate = $_SESSION['bDate'];
							}else{
								$_SESSION['bDate'] = isset($_REQUEST['bDate']) ? $_REQUEST['bDate'] : '';
							}

							if(isset($_SESSION['eDate'])){
								if(!empty($_REQUEST['eDate'])) $_SESSION['eDate'] = $_REQUEST['eDate'];
								$eDate = $_SESSION['eDate'];
							}else{
								$_SESSION['eDate'] = isset($_REQUEST['eDate']) ? $_REQUEST['eDate'] : '';
							}

						?>
					</select>
					<input type="date" name="bDate" value="<?php echo $bDate;?>" onchange="changeDate(this)">
					<input type="date" name="eDate" value="<?php echo $eDate;?>">
					<input type="button" value="確定" onClick="doSubmit()">
				</td>
			</tr>
			<tr>
				<td><font size="2" color="gray">※ 如欲修改或刪除請先選取一個項目。</font></td>
				<td align="right">
					<?php
						$ss='目前頁數：'.(int)($pages+1).' / '.(int)($pageCount+1)." (總筆數：$recordCt)"; 
						if ($keyword!="") $ss = "搜尋：$keyword ".$ss;
					?>
					<label class="sText"><?=$ss?></label>
				</td>
			</tr>
		</table>
		<table border="1" cellspacing="0" cellpadding="2" width="100%" bordercolordark="#DFF7FD" bordercolorlight="#80A0D0" class="sTable">
			<!--## 表頭  ##-->
			<tr bgcolor="#336699" height="25" valign="bottom" align="center" bordercolordark="#336699">
				<?php for ($i=0; $i<count($fnAry); $i++) { ?>
					<th>
						<a href="#" onclick='SortList("<?=$fnAry[$i]?>","<?=(isset($sortFG)) ? $sortFG : ''?>","<?=$keyword?>");' class="head">
							<?=$ftAry[$i]?>&nbsp;&nbsp;
							<?php if ($fieldname==$fnAry[$i]) { ?>
								<font class="bulDir">
									<?php if ($sortDirection=="ASC") echo '▲'; else echo '▼'; ?>
								</font>
							<?php } ?>
						</a>
					</th>
				<?php } ?>
				<th>列印</th>
			</tr>
			<?php
				//過濾條件
				if(isset($filter)) $sql="select * from $tableName where ($filter)"; 
				else $sql="select * from $tableName where 1";
				// 搜尋處理
				if ($keyword!="") $sql .= " and (($searchField1 like '%$keyword%') or ($searchField2 like '%$keyword%') or ($searchField3 like '%$keyword%') or ($searchField4 like '%$keyword%'))";
				if (!empty($whereStr)) $sql .= " AND ".join(' AND ', $whereStr);
				// 排序處理
				if ($fieldname=="") $sql.=" order by $defaultOrder $sortDirection"; else	$sql.=" order by  $fieldname $sortDirection";
				//分頁
				$sql .= " limit ".$pageSize*$pages.",$pageSize";  
				// echo $sql; //exit;
				
				// $rs = db_query($sql,$conn);
				// while ($r=db_fetch_array($rs)) { 
				
				$stmt = $db->prepare($sql);
				$stmt->execute();
				while($r = $stmt->fetch()) {
					$id = $r[$fnAry[0]];
			?>
					<tr valign="top" id="tr_<?=$id?>" onMouseOver="SelRowIn('<?=$id?>',this)" onMouseOut="SelRowOut('<?=$id?>',this)" onClick="SelRow('<?=$id?>',this)"<?php if($id==$selid) echo ' class="onSel"'?>>
					<td <?php if ($flAry[0]>"") echo "width='".$flAry[0]."'"?>><input type="checkbox" value="<?=$id?>" name="ID[]" <?php if (isset($nextfg)) echo "checked" ?>>
					<a href="javascript: clickEdit('<?=$id?>')"><?=$id?></a></td>
					<?php for($i=1; $i<count($fnAry); $i++) { ?>
						<td <?php if (isset($flAry[$i])>"") echo "width='$flAry[$i]'" ?> >
						<?php $fldValue = $r[$fnAry[$i]] ?>
						<?php 
							if ($fldValue>""){
								switch ($ftyAy[$i]) {
									case "select": 
										// $tm=$$fnAry[$i];
										$tm=${$fnAry[$i]};  
										echo $tm[$fldValue]; 
										break;
									case "image" : echo "<img src='$ulpath$fldValue' ".($flAry[$i]?"width='$flAry[$i]'":"")."/>"; break;
									case "link"  : echo "<a href='$fldValue' target='view'>$fldValue</a>"; break;
									case "flink" : echo "<a href='$ulpath$fldValue' target='view'>$fldValue</a>"; break;
									case "email" : echo "<a href='mailto:$fldValue'>$fldValue</a>"; break;
									case "bool"  : $pic=$fldValue?'checked.gif':'uncheck.gif'; echo "<img src='$pic'>"; break;					
									case "datetime" : echo date('Y/m/d H:n:s',strtotime($fldValue)); break;
									case "date" : echo date('Y/m/d',strtotime($fldValue)); break;
									case "EXTRA1" : echo date('Y/m/d',strtotime($r['bDate'])).'~'.date('Y/m/d',strtotime($r['eDate'])); break;
									case "EXTRA2" : echo date('H:i',strtotime($r['bDate'])).'~'.date('H:i',strtotime($r['eDate'])); break;
									case "status" :
										$tm=${$fnAry[$i]}; 
										if($fldValue == 1){
											echo "<span style='color:red'>".$tm[$fldValue]."</span>";
										}else{
											echo "<span style='color:blue'>".$tm[$fldValue]."</span>";
										}
										
										$statusNote = $r['statusNote'];
										echo "　<button type='button' onclick='statusDiv(\"$fldValue\",\"$id\",\"$statusNote\")'>辦理</button>";
										break;
									case "isPay":
										$tm = ${$fnAry[$i]}; 
										if($fldValue == 0){
											echo "<span style='color:red'>".$tm[$fldValue]."</span>";
										}else{
											echo "<span style='color:blue'>".$tm[$fldValue]."</span>";
										}

										$price = $r['price'];
										$fold  = $r['fold'];
										if($fldValue == 0 || $fldValue == 2){
											echo "　<button type='button' onclick='isPayDiv(\"$fldValue\",\"$id\",\"$price\",\"$fold\")'>費用</button>";
										}
										break;
									default : echo $fldValue;
								} 
							}else echo "&nbsp";
						?>
						</td>
					<?php } ?>	
					<td><a href="printform.php?rentPlace.php?id=<?=$id?>" target="print"><img src="../images/print.png" border="0"></a></td>
					</tr>
			<?php } ?>
		</table>
	</form>
	<form name="excelForm" action="excel.php" method="post" target="excel">
		<input type='hidden' name='excelSql' value="<?php echo join(' AND ', $whereStr);?>">
		<input type='hidden' name='mode' value="statistics">
	</form>
	<form name="excelForm2" action="excel.php" method="post" target="excel">
		<input type='hidden' name='excelSql' value="<?php echo join(' AND ', $whereStr);?>">
		<input type='hidden' name='mode' value="allData">
	</form>
	<div id="isPayDiv">
		<table bgcolor="#FFFFFF" cellpadding="4">
			<tr>
				<td>
					<input type="hidden" name="pid">
					<input type="hidden" name="mode" value="isPay">
				</td>
			</tr>
			<tr>
				<td>
					<select name="isPay">
						<option value="0">未繳費</option>
						<option value="1">已繳費</option>
						<option value="2">無</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					價格：
				</td>
				<td>
					<input type="text" name="price">
				</td>
			</tr>
			<tr>
				<td>
					折扣：
				</td>
				<td>
					<input type="text" name="fold">
				</td>
			</tr>
			<tr>
                <td colspan="2" align="center">
                    <input type="button" onclick="isPaySubmit()" value="確定" />
                    <button type="button" onclick="$('#isPayDiv').hide()">取消</button><br>
                </td>
            </tr>
		</table>
	</div>
	<div id="statusDiv">
		<form name="statusFm" method="post" enctype="multipart/form-data" id="statusFm">
			<table bgcolor="#FFFFFF" cellpadding="4">
				<tr>
					<td>
						<input type="hidden" name="mid">
						<input type="hidden" name="mode" value="status">
						<input type="hidden" name="email" value="<?=isset($email) ? $email : ''?>">
					</td>
				</tr>
				<tr>
					<td>
						<select name="status">
							<option value="0">不核准</option>
							<option value="1">待處理</option>
							<option value="2">核准</option>
							<option value="3">已取消</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						備　　　　　註：
					</td>
					<td>
						<input type="text" name="statusNote">
					</td>
				</tr>
				<tr>
					<td>
						檔　　　　　案：
					</td>
					<td>
						<input type="file" name="mailFile[]" multiple="multiple" accept=".pdf,.doc,.docx,image/*,">
					</td>
				</tr>
				<tr>
	                <td colspan="2" align="center">
	                    <input type="button" onclick="statusSubmit()" value="確定" />
	                    <button type="button" onclick="$('#statusDiv').hide()">取消</button><br>
	                </td>
	            </tr>
			</table>
		</form>
	</div>
	<div align="right"><a href="#top">Top ↑</a></div>
</body>
</html>