<?php	
	//::for Access Permission Control
	if (session_status() === PHP_SESSION_NONE) {
		session_start();
	}
	if ($_SESSION['privilege']<15) {
		header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');
    include_once('../../connect_db.inc.php');		
	$bE = true;	//異動權限
	if ($_SESSION['privilege']==15) {
		$pageTitle = "場地借用查詢";
	}
	if ($_SESSION['privilege']==16) {
		$pageTitle = "場地借用申請";
	}
	
	$tableName = "rentplace";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/visitform/';
	$delField = 'recept';
	$delFlag = false;
	$thumbW = 150;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "places, title, bDate, id";
	$searchField1 = "applier";
	$searchField2 = "leadman";
	$searchField3 = "contactman";
	$searchField4 = "title";

	$status = array('0'=>'不核准','1'=>'待處理','2'=>'核　准','3'=>'已取消');
	$isPay  = array('0'=>'未繳費','1'=>'已繳費','2'=>'無');
	$isView = array('0'=>'不顯示','1'=>'顯示');



	$sql = "select a.id, a.title, b.title as floor from space a join spaceclass b on(a.sId = b.id) order by a.sId, a.title, a.id";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	// $rs  = db_query($sql,$conn);
	$places = array();
	// while($r = db_fetch_array($rs)){
	while($r = $stmt->fetch()) {
		$places[$r['id']] = $r['floor'].' - '.$r['title'];
	}
	
	$sql = "select id, title from space_item order by type";
	// $rs  = db_query($sql, $conn);
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$options = array();
	// while($r = db_fetch_array($rs)){
	while($r = $stmt->fetch()) {
		$options[$r['title']] = $r['title'];
	}

	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,applier,places,contactman,tel,mobile,title,bDate,bDate,peoples,rdate,UserNote,status,isPay");
	$ftAry = explode(',',"編號,申請單位,場地,聯絡人,電話,手機,活動名稱,活動日期,時段,使用人數,登錄日期,備註,辦理狀況,繳費狀況");
	$flAry = explode(',',"60,,,,,,,,,,,");
	$ftyAy = explode(',',"ID,text,select,text,text,text,text,EXTRA1,EXTRA2,text,date,text,status,isPay");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"applier,leadman,contactman,tel,fax,mobile,email,address,title,peoples,places,options,Date,isView,UserNote");
	$newftA = explode(',',"申請單位,負責人,聯絡人,電話,傳真,手機,EMail,地址,活動名稱,研習人數,借用場地,加借(購)明細,日期,是否顯示電視牆,備註");
	$newflA = explode(',',"60,,,,,60,60,60,,,,60,,,,150");
	$newfhA = explode(',',",,,,,,,,,,,,,,,5");
	$newfvA = array('','','','','','','','','','','','','',$isView,'');
	$newetA = explode(',',"text,text,text,text,text,text,text,text,text,text,extra1,extra2,extraDate,checkbox,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,applier,leadman,contactman,tel,fax,mobile,email,title,peoples,places,Date,Time,options,status,isView,UserNote");
	$editftA = explode(',',"編號,申請單位,負責人,聯絡人,電話,傳真,手機,EMail,活動名稱,研習人數,借用場地,日期,時段,加借(購)明細,辦理況狀,是否顯示電視牆,備註");
	$editflA = explode(',',",60,,,,,60,60,60,,,60,,,,,150");
	$editfhA = explode(',',",,,,,,,,,,,,,,,,5");
	$editfvA = array('','','','','','','','','','','','','','',$status,$isView,'');
	$editetA = explode(',',"text,text,text,text,text,text,text,text,text,text,extra1,extraDate,extraTime,text,select,checkbox,textbox");
?>