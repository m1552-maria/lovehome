<?php
	include("init.php");
	switch ($_POST['mode']) {
		case 'placeTime':
			// $sql = "select * from space_detail where visitId = '".$_POST['id']."' order by visitId,week_day,CAST(startTime as time) ASC";
			// $rs  = db_query($sql,$conn);

			$timeData = array();
			// while($r = db_fetch_array($rs)){
			$query = "select * "
					."from space_detail "
					."where visitId = :id "
					."order by visitId,week_day,CAST(startTime as time) ASC ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'id' => $_POST['id']
			));
			while($r = $stmt->fetch()) {
				$timeData[] = str_pad($r['startTime'], 5, "0", STR_PAD_LEFT)." ~ ".$r['endTime'];
			}
			echo json_encode(array_unique($timeData));
			break;

		case 'placeTimeEDIT':
			$week_day = date("w", strtotime($_POST['date']));
			// $sql = "select * from space_detail where visitId = '".$_POST['id']."' and week_day = '$week_day' order by visitId,week_day,CAST(startTime as time) ASC";
			// $rs  = db_query($sql,$conn);

			$timeData = array();
			// while($r = db_fetch_array($rs)){
			$query = "select * from space_detail "
					."where visitId = :id "
					."and week_day = :week_day "
					."order by visitId,week_day,CAST(startTime as time) ASC ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'id' => $_POST['id'],
				'week_day' => $week_day
			));
			while($r = $stmt->fetch()) {
				$timeData[] = str_pad($r['startTime'], 5, "0", STR_PAD_LEFT)." ~ ".$r['endTime'];
			}
			echo json_encode($timeData);
			break;

		case 'DateCheck':
			$places = $_POST['places'];
			$time  = array();
			$time  = explode(" ~ ", $_POST['time']);
			$bDate = $_POST["date"]." ".$time['0'];
			$eDate = $_POST["date"]." ".$time['1'];

			// $sql = "select count(1) as _count from rentPlace where bDate = '$bDate' AND eDate = '$eDate' AND places = '$places'";
			// $rs  = db_query($sql,$conn);
			// $r   = db_fetch_array($rs);
			$query = "select count(1) as _count "
					."from rentPlace "
					."where bDate = '".$bDate."' AND eDate = '".$eDate."' "
					."AND places = :places ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'places' => $places
			));
			$r = $stmt->fetch();
			if($r["_count"] > 0){
				echo json_encode("error");
			}else{
				$date  = array();
				$date  = explode("-", $_POST['date']);
				$year  = $date['0'];
				$mont  = $date['1'];
				$days  = $date['2'];


				$sql = "select count(1) as _count from space_close where close_y = '$year' AND close_m = '$mont' AND close_d = '$days' AND (visitId = '$places' or visitId = 'all')";
				// $rs  = db_query($sql,$conn);
				// $r   = db_fetch_array($rs);
				$query = "select count(1) as _count "
						."from space_close "
						."where close_y = '$year' AND close_m = '$mont' "
						."AND close_d = '$days' AND (visitId = :places or visitId = 'all')";
				$stmt = $db->prepare($query);
				$stmt->execute(array(
					'places' => $places
				));
				$r = $stmt->fetch();
				if($r["_count"] > 0){
					echo json_encode("error2");
				}else{
					echo json_encode("success");
				}
			}
			break;

		case 'status':
			include_once '../../domail.php';

			$pathinfo_extArr = array("doc", "docx", "pdf", "jpeg", "jpg", "gif", "png");
			if(!empty($_FILES["mailFile"]['type']['0'])){
				foreach ($_FILES as $key => $value) {
					$n = count($value['type']);
					for($i = 0; $i < $n; $i++){
						$ext = pathinfo($value['name'][$i], PATHINFO_EXTENSION);
						if(!in_array($ext, $pathinfo_extArr)){
							echo json_encode("error");
							exit;
						}
					}
				}
			}
			
			$sql = "update rentPlace set 
					status     = '" . $_POST['status']     . "',
					statusNote = '" . $_POST['statusNote'] . "'
					where id   = '" . $_POST['mid'] . "'";
			// db_query($sql, $conn);
			$db->exec($sql);

			// $sql     = "select * from rentPlace where id='{$_POST['mid']}'";
			// $rs      = db_query($sql);
			// $r    	 = db_fetch_array($rs);
			$query = "Select * "
					."From rentPlace "
					."Where id = :mid ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'mid' => $_POST['mid']
			));
			$r = $stmt->fetch();
			$email   = $r['email'];

			$fn 	 = '../../mail/rentPlaceUserMail.html'; 
			$content = file_get_contents($fn);
			$content = str_replace("{id}",         $_POST["mid"],    $content);
			$content = str_replace("{price}",      $r["price"],      $content);
			$content = str_replace("{fold}",       $r["fold"],       $content);
			$content = str_replace("{statusNote}", $r["statusNote"], $content);
			$content = str_replace("{applier}",    $r["applier"],    $content);
			$content = str_replace("{bDate}",      $r["bDate"],      $content);
			$content = str_replace("{eDate}",      $r["eDate"],      $content);
			// $content = str_replace("{placeName}",  $r["placeName"],  $content);
			$content = str_replace("{title}",      $r["title"],      $content);
			$content = str_replace("{leadman}",    $r["leadman"],    $content);
			$content = str_replace("{mobile}",     $r["mobile"],     $content);

			$subtx   = '愛心家園場地租借回覆信函:';
			$rzt     = send_mail($email, $subtx, $content, $_FILES);

			echo json_encode(1);
			break;

		case 'isPay':
			$sql = "update rentPlace set 
					isPay    = '" . $_POST['isPay'] . "',
					price    = '" . ($_POST['price'] ?? 0) . "',
					fold     = '" . ($_POST['fold'] ?? 0)  . "'
					where id = '" . $_POST['id']    . "'";
			// db_query($sql, $conn);
			$db->exec($sql);
			echo json_encode(1);
			break;

		case 'uper_limit':
			// $sql="select * from space where id='".$_POST['id']."'";
			// $rs = db_query($sql);
			// $r  =  db_fetch_array($rs);
			$query = "Select * "
					."From space "
					."Where id = :id ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'id' => $_POST['id']
			));
			$r = $stmt->fetch();
			echo json_encode($r['uper_limit']);

			break;

		default:
			# code...
			break;
	}
?>