<?php
    include 'init.php';
    $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
    // $sql = "select a.*, b.title as spaceName,b.peopleNum,b.cost, c.title as floorName from rentplace a join space b ON(a.places = b.id) join spaceclass c ON(b.sId = c.id) where a.id=$id";
    // $rs = db_query($sql);
    // $r  = db_fetch_array($rs);
    $query = "select a.*, b.title as spaceName, b.peopleNum, b.cost, c.title as floorName "
            ."from rentplace a "
            ."join space b "
            ."  ON(a.places = b.id) "
            ."join spaceclass c "
            ."  ON(b.sId = c.id) "
            ."where a.id = :id";
    $stmt = $db->prepare($query);
    $stmt->execute(array(
      'id' => $id
    ));
    $r = $stmt->fetch();
?>
<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">
  <style>
    .cellL1 {
      border-width: 1px;
      border-color: #BBB;
      border-style: dotted;
    }
  </style>
  <title>臺中市愛心家園</title>
</head>

<body>
  <table width="92%" border="0" align="center" style="font-family:'微軟正黑體',Verdana; font-size:16px">
    <tr>
      <th style="font-size:24px">場地借用申請單<span style="float: right; font-size:16px; padding-top: 10px; padding-right: 10px;">單號：<?=$r['id']?></span></th>
    </tr>
    <tr>
      <td>
        <table width="100%" border="1" align="center" cellpadding="4" cellspacing="0">
          <tr>
            <td width="120" align="right" bgcolor="#f0f0f0">申請單位</td>
            <td colspan="3"><?= $r['applier'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">負責人</td>
            <td><?= $r['leadman'] ?></td>
            <td width="120" align="right" bgcolor="#f0f0f0">聯絡人</td>
            <td><?= $r['contactman'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">聯絡電話</td>
            <td><?= $r['tel'] ?></td>
            <td width="120" align="right" bgcolor="#f0f0f0">行動電話</td>
            <td><?= $r['mobile'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">傳真</td>
            <td colspan="3"><?= $r['fax'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">地址</td>
            <td colspan="3"><?= $r['address'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">電子郵件</td>
            <td colspan="3"><?= $r['email'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">活動名稱</td>
            <td colspan="3"><?= $r['title'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">租用時間</td>
            <td colspan="3"><?php echo date("Y-m-d　H:i", strtotime($r["bDate"]))." 起<br>".date("Y-m-d　H:i", strtotime($r["eDate"])). " 迄 "; ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#f0f0f0">預估參加人數</td>
            <td colspan="3"><?= $r["peoples"] ?> 人</td>
          </tr>
          <tr>
            <td align="right" valign="top" bgcolor="#f0f0f0">借用場地</td>
            <td colspan="3">
              <table border="0" cellpadding="4" cellspacing="0" width="100%" class="cellL1">
                <tr bgcolor="#F0F0F0">
                  <th class="cellL1">場地別</th>
                  <th class="cellL1">編號</th>
                  <th class="cellL1">使用人數</th>
                  <th class="cellL1">每場次維護費</th>
                  <th class="cellL1">樓別</th>
                  <th class="cellL1">備註</th>
                </tr>
                <tr>
                    <td><?php echo $r['spaceName']; ?></td>
                    <td><?php echo $r['places']; ?></td>
                    <td><?php echo $r['peopleNum']; ?></td>
                    <td><?php echo $r['cost']; ?></td>
                    <td><?php echo $r['floorName']; ?></td>
                </tr>
                <!-- <tr> 
                  <td><img src="/images/<?= strpos($r['places'], 'B10') === false ? 'uncheck.gif' : 'check.gif' ?>" />禮堂</td>
                  <td>B10</td>
                  <td>200</td>
                  <td>5,000</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B09') === false ? 'uncheck.gif' : 'check.gif' ?>" />視聽室</td>
                  <td>B09</td>
                  <td>90</td>
                  <td>4,000</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B03') === false ? 'uncheck.gif' : 'check.gif' ?>" />研習教室１</td>
                  <td>B03</td>
                  <td>40</td>
                  <td>2,500</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B05') === false ? 'uncheck.gif' : 'check.gif' ?>" />研習教室２</td>
                  <td>B05</td>
                  <td>20</td>
                  <td>2,000</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B07') === false ? 'uncheck.gif' : 'check.gif' ?>" />研習教室３</td>
                  <td>B07</td>
                  <td>20</td>
                  <td>2,000</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B08') === false ? 'uncheck.gif' : 'check.gif' ?>" />研習教室 4</td>
                  <td>B08</td>
                  <td>30</td>
                  <td>3,000</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'B24') === false ? 'uncheck.gif' : 'check.gif' ?>" />研習教室 5</td>
                  <td>B24</td>
                  <td>50</td>
                  <td>2,500</td>
                  <td>地下室</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], '301') === false ? 'uncheck.gif' : 'check.gif' ?>" />視聽室 </td>
                  <td>301</td>
                  <td>90</td>
                  <td>4,000</td>
                  <td>三樓</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], '328') === false ? 'uncheck.gif' : 'check.gif' ?>" />音樂教室</td>
                  <td>328</td>
                  <td>40</td>
                  <td>3,000</td>
                  <td>三樓</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><img src="/images/<?= strpos($r['places'], 'SS01') === false ? 'uncheck.gif' : 'check.gif' ?>" />專業空間</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>須電洽</td>
                </tr> -->
              </table>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top" bgcolor="#f0f0f0">加借(購)明細</td>
            <td colspan="3">借用器材（$300/按項另計）：<?= $r['options'] ?></td>
          </tr>
          <tr>
            <td align="right" bgcolor="#F0F0F0">發票</td>
            <td colspan="3">
                <?php  switch($r["isInv"]) {
            			case 2: echo "二聯式發票"; break;
            			case 3: echo "三聯式發票<br>抬頭：$r[invoiceTitle] &nbsp;統一編號：$r[invoiceNo]";
    		        }
                ?>
            </td>
          </tr>
          <tr>
            <td align="right" bgcolor="#F0F0F0">合計新台幣</td>
            <td colspan="3" align="center"><?php echo $r['price']; ?>　元整。　優惠 <?php echo $r['fold']; ?> 折　　　收款日期:<input type='text' size='2'>年<input type='text' size='1'>月<input type='text' size='1'>日</td>
          </tr>
          <tr>
            <td bgcolor="#F0F0F0">&nbsp;</td>
            <td colspan="3">
                <input type="checkbox" name="check1" checked>申請單位已詳閱「場地租借申請流程」 <br>
                <input type="checkbox" name="check2" checked>申請單位已詳閱「場地租借辦法」 <br>
                <input type="checkbox" name="check3" checked>申請單位已詳閱「場地租借共同遵守約定」 <br>
                <input type="checkbox" name="check4" checked>申請單位已詳閱「專業空間借用要點」<br>
                <input type="checkbox" name="check5" checked>申請單位已詳閱「愛心家園停車須知」 <br>
                <input type="checkbox" name="check6" checked>申請單位已詳閱「提前或延後開關大門申請單」 <br>
                <input type="checkbox" name="check7" checked>申請單位已詳閱「場地租借聯絡人資訊」 <br>
                <input type="checkbox" name="check8" checked>申請單位已詳閱「水療池租借同意書」 <br>
          </tr>
          <tr>
            <td align="right" bgcolor="#F0F0F0">備    註</td>
            <td colspan="3"><?php echo $r['UserNote']; ?></td>
          </tr>
        </table>
        <table width="650" border="0" cellspacing="0" cellpadding="8" align="center">
          <tr>
            <td width="100" align="right">業務主管：</td>
            <td>&nbsp;</td>
            <td width="100" align="right">
              <p>承辦人 :</p>
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>

      </td>
    </tr>
  </table>
</body>

</html>