<?php
	include("init.php");
	include('../../thumb.php');
	unset($_SESSION['bDate']);
	unset($_SESSION['eDate']);


	//處理上傳的附件
	foreach($_FILES as $k=>$v) {
		$fn = $_FILES[$k]['name'];
		if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
		if ($_FILES[$k][tmp_name] != "") {
			/* if (!copy($_FILES[$k][tmp_name],"$ulpath$fn2")) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			} */
			imageResize($_FILES[$k][tmp_name],"$ulpath$fn2",$thumbW,$thumbW);	
		}
	}

	foreach ($_REQUEST['tDate'] as $key => $value) {
		$sql = "insert into $tableName";
		$fields = "";
		$values = "";
		$n = count($newfnA);
		for ($i=0; $i<$n; $i++) {
			if($newfnA[$i] == 'Time') continue;
			
			if($newfnA[$i] == 'Date'){
				$fields = $fields . "status, isPay, bDate, eDate";
				if ($i<($n-1)) $fields = $fields.',';
			}else{
				$fields = $fields . $newfnA[$i];
				if ($i<($n-1)) $fields = $fields.',';
			}
	  		switch($newetA[$i]) {
				case "hidden" :
				case "select" :
				case "readonly" :
				case "textbox":
		  		case "text" :	$fldv = "'".addslashes($_REQUEST[$newfnA[$i]])."'"; break;
		  		case "textarea" :	$fldv = "'".trim($_REQUEST[$newfnA[$i]])."'"; break;
		  		case "checkbox" : if(isset($_REQUEST[$newfnA[$i]])) $fldv=1; else $fldv=0; break;
				case "date" : $fldv = "'".$_REQUEST[$newfnA[$i]]."'"; break;
				case "file" : $fldv = "'$fn'"; break;
				case "extra1":
					$_SESSION['places'] = $_REQUEST[$newfnA[$i]]['0'];
					$fldv = "'".$_REQUEST[$newfnA[$i]]['0']."'"; break;
				case 'extra2':
					$options = is_array($_REQUEST["options"]) ? join('，',$_REQUEST["options"]) : $_REQUEST["options"]['0'];
					$fldv = "'".$options."'";
					break;
				case 'extraTime':
					break;
				case 'extraDate':
					$time  = array();
					$time  = explode(" ~ ", $_REQUEST['tTime'][$key]);
					$bDate = $value." ".$time['0'];
					$eDate = $value." ".$time['1'];
					DateSize($value);
					$fldv = "'2','2','".$bDate."','".$eDate."'";
					break;
	  		}	
	  		$values = $values . $fldv;
			if ($i<($n-1)) $values = $values.',';
		}
		$sql = $sql.'('.$fields.') values('.$values.')';
		// echo $sql;
		// exit;
		// db_query($sql,$conn);
		$db->exec($sql);
	}

	function DateSize($date){ 
		if(isset($_SESSION['bDate'])){
			$minDate = strtotime($_SESSION['bDate']);
    		$dateCol = strtotime($date);

    		if($minDate > $dateCol){
    			$_SESSION['bDate'] = date("Y-m-d", $dateCol);
    		}
		}else{
			$_SESSION['bDate'] = $date;
		}

		if(isset($_SESSION['eDate'])){
			$maxDate = strtotime($_SESSION['eDate']);
    		$dateCol = strtotime($date);

    		if($maxDate < $dateCol){
    			$_SESSION['eDate'] = date("Y-m-d", $dateCol);
    		}
		}else{
			$_SESSION['eDate'] = $date;
		}
	}
	
	// db_close($conn);
	header("Location: list.php");
?>
