<?php 
	include("init.php");
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>append.css">
	<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
	<style type="text/css">
		.ui-widget {font-size:12px; }
		.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom; }
		table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
		table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center; }
		#d_setting_time{	
			position:fixed;
			left:20%;
			top:15%;
			width:400px;
			display:none;
			height: 80%;
			overflow-y: auto;
		}
		.divTitle{
			color:#fff;
			background-color:#900;
			border: 0px;
			padding: 5px;
			margin: 0px;
			font-size:12px;
			width:95%;
		}
		.divBorder2 { /* 內容區 第二層外框 */
			padding:8px; 
			border:1px solid #CCC;
			background:#FFFFFF;
			font-size:12px;
			width:372px;
		}
		.dBottom{	
			background:#dedede;
			text-align:center;
			padding:8px;
			border:1px solid #ccc;
			width:372px;
		}
		#tb_setting_time{
			width:98%;
		}
		.delBtn{
			width:26px;
			cursor:pointer;
		}
	</style>
	<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<title><?=$pageTitle?> - 新增</title>
</head>

<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>

	<?php for ($i=0; $i<count($newfnA); $i++) { ?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
		<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  	<td><?php switch ($newetA[$i]) { 
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>"; break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  			case "checkbox": echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=>$v) echo "<option value='$k'>$v</option>"; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "extra1":
				$aa = explode('，',$r[$newfnA[$i]]);
				$ba = ${$newfnA[$i]};
				foreach($ba as $k=>$v) { echo "<input type='radio' name='$newfnA[$i]"."[]"."' class='input' ".(in_array($k,$aa)?'checked':'')." value='$k'>$v &nbsp;&nbsp;"; }
				break;
			case "extra2":
				$aa = explode('，',$r[$newfnA[$i]]);
				$ba = ${$newfnA[$i]};
				foreach($ba as $k=>$v) { echo "<input type='checkbox' name='$newfnA[$i]"."[]"."' class='input' ".(in_array($k,$aa)?'checked':'')." value='$k'>$v &nbsp;&nbsp;"; }
				break;
			case "extraDate": 
				echo '<input value="設定日期" type="button" class="gray" onClick="settingTime()" />';
				break;
  	} ?></td>
  </tr><?php } ?>
	
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="確定新增" class="btn">&nbsp;
			<input type="reset" value="重新輸入" class="btn">&nbsp;
			<button type="button" class="btn" onClick="history.back()">取消新增</button>
		</td>
	</tr>
</table>
</form>
<div id='d_setting_time'>
	<div id='dTitle' class='divTitle'>日期多選</div>
	<div class='divBorder2' id='d_container'>
	</div>
	<div class='dBottom'><input type="button" value='確定' onclick='saveTime()'/>&nbsp;<input type="button" value='取消' onclick='doCancel()'/></div>
</div>
</body>
<script type="text/javascript">
	var timeArr = [];
	var dateArr = [];
	var option  = "";
	var uper_limit = 0;

	$("input:radio").change(function(){
		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "uper_limit",
				id   : $(this).val()
			},
			dataType: 'json',
			success: function(d){
				uper_limit = d*1;
			}
		});
	});

	function checkDate(obj){
		var tr    = $(obj).parent().parent();
		var index = $(tr)[0].rowIndex-2;
		var date  = $("input[name='date[]']")[index];
		var time  = $("select[name='time[]']")[index];

		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "DateCheck",
				date : date.value,
				time : time.value,
				places : $("input[name='places[]']:checked").val()
			},
			dataType: 'json',
			success: function(d){
				if(d == 'error'){
					$(date).next().remove();
					$(date).after("<div style='color:red;'>"+date.value+" "+time.value+" 已有人借用</div>");
				}else if(d == 'error2'){
					$(date).next().remove();
					$(date).after("<div style='color:red;'>"+date.value+" 為關閉日</div>");
				}else{
					$(date).next().remove();
				}
			}
		});
	}
	
	function settingTime(){//設定時段
		var str="<table class='grid' id='tb_setting_time'>";
			str+="<tr><td colspan='3'><input type='button' value='新增日期' onclick='addItem()' style='float:left'/></td></tr>";
			str+="<tr><td>日期</td><td>時段</td><td>刪除</td></tr>";

			if(dateArr.length > 0){
				$.each(dateArr, function(k,v){
					str+="<tr bgcolor='#E3E3E3' style='height: 30px;'><td align='center'><input type='date' name='date[]' value='"+v+"' onchange='checkDate(this)'></td>";
					str+="<td><select name='time[]' onchange='checkDate(this)'>";
					$.each(option, function(ko,vo){
						if(timeArr[k] == vo) str += "<option value='"+vo+"' selected>"+vo+"</option>";
						else str += "<option value='"+vo+"'>"+vo+"</option>";
					});
					str+="</select></td>";
					str+="<td align='center'><img src='/images/del.png' class='delBtn' onClick='delItem(this,event)'></td></tr>"; 

				});
			}
			str+="</table>";

		$("#d_container").html(str);
		$('#d_setting_time').show();
	}
	function doCancel(){
		$('#d_setting_time').hide();
	}
	function addItem(){//新增開放時段
		
		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "placeTime",
				id   : $("input:radio:checked").val()
			},
			dataType: 'json',
			async: false,
			success: function(d){
				option = d;
			}
		});

		var tb       = document.getElementById('tb_setting_time');
		var tbObj    = tb.tBodies[0];
		var newRow   = document.createElement('tr');
		var newcell1 = document.createElement('td');
		var newcell2 = document.createElement('td');
		var newcell3 = document.createElement('td');

		newRow.style.height='30px';
		newRow.bgColor='#E3E3E3';
		var strDate = "<input type='date' name='date[]' onchange='checkDate(this)'>"; 
		var optionStr = "";
		$.each(option, function(k,v){
			optionStr +="<option value='"+v+"'>"+v+"</option>";
		});
		var strTime = "<select name='time[]' onchange='checkDate(this)'>"+optionStr+"</select>";		
		newcell1.align='center';
		newcell1.innerHTML=strDate;
		newcell2.align='center';
		newcell2.innerHTML=strTime;
		newcell3.align='center'; 
		newcell3.innerHTML='<img src="/images/del.png" class="delBtn" onClick="delItem(this,event)">';
		newRow.appendChild(newcell1);
		newRow.appendChild(newcell2);
		newRow.appendChild(newcell3);

		tbObj.appendChild(newRow);
	}
	function delItem(obj,e){//刪除開放時段
		var evt = e || window.event;
		var current = evt.target || evt.srcElement;
		var currRowIndex=current.parentNode.parentNode.rowIndex; 
		var tb = document.getElementById("tb_setting_time");
		if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
		if(timeStart[day]!=undefined) {timeStart[day].splice(currRowIndex, 1);}
		if(timeEnd[day]!=undefined) {timeEnd[day].splice(currRowIndex, 1);}
		if(dayData[day]>0){	
			dayData[day]=parseInt(dayData[day])-1;
		}else if(dayData[day]==0){	dayData[day]=undefined;}
	}
	function saveTime(){
		var bSave=true;
		$.each($("[name='date[]']"), function(k,v){
			dateArr[k] = $(this).val();
		});
		$.each($("[name='time[]']"), function(k,v){
			timeArr[k] = $(this).val();
		});
		if(bSave){
			$('#d_setting_time').hide();
		}
	}
	function formvalid(tfm) {

		if(!tfm.tel.value)        { alert('請填寫 聯絡電話');     tfm.tel.focus();        return false; }
		if(!tfm.title.value)      { alert('請填寫 活動名稱');     tfm.title.focus();      return false; }
		if(!tfm.applier.value)    { alert('請填寫 申請單位');     tfm.applier.focus();    return false; }
		if(!tfm.peoples.value)    { alert('請填寫 預估研習人數'); tfm.peoples.focus();    return false; }
		if(!tfm.contactman.value) { alert('請填寫 聯絡人');       tfm.contactman.focus(); return false; }
		if(dateArr.length <= 0)   { alert("請最少填寫一筆日期");  return false; }
		if(timeArr.length <= 0)   { alert("請最少填寫一筆時段");  return false; }
		
		if(tfm.peoples.value > uper_limit) {
			alert('預估研習人數超過此場地人數上限 '+uper_limit+' 人'); 
			tfm.peoples.focus();
			return false;
		}

		$.each(dateArr, function(k,v){
			$(tfm).append("<input type='hidden' name='tDate[]' value='"+v+"'>");
		});
		$.each(timeArr, function(k,v){
			$(tfm).append("<input type='hidden' name='tTime[]' value='"+v+"'>");
		});
		return true;
	}
</script>
</html>
