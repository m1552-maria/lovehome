<?php
	include("init.php");
	$ID = isset($_REQUEST['ID']) ? $_REQUEST['ID'] : '0';
  	// $sql = "select * from $tableName where $editfnA[0]='$ID'";
	// $rs = db_query($sql, $conn);
	// $r = db_fetch_array($rs);
	$query = "Select * "
			."From ".$tableName." "
			."Where ".$editfnA[0]." = :ID ";
	$stmt = $db->prepare($query);
	$stmt->execute(array(
		'ID' => $ID
	));
	$r = $stmt->fetch();
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="<?=$extfiles?>edit.css">
 <link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
 <style type="text/css">
 .ui-widget {font-size:12px; }
 .ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;} 
 </style>
 <script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
 <script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <script type="text/javascript" src="/ckeditor/ckeditor.js"></script> 
 <title><?=$pageTitle?> - 編輯</title>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data">
<table align="center" class="sTable" width="98%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<?php for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
  	  case "text" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
  		case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";	break;
			case "extra1":
				$aa = explode('，',$r[$editfnA[$i]]);
				$ba = ${$editfnA[$i]};
				foreach($ba as $k=>$v) { echo "<input type='radio' name='$editfnA[$i]"."[]"."' class='input' ".(in_array($k,$aa)?'checked':'')." value='$k'>$v &nbsp;&nbsp;"; }
				break;	
			case "extraTime":
				$sT = date("H:i", strtotime($r["bDate"]));
				$eT = date("H:i", strtotime($r["eDate"]));

				echo "<select name='time' data-default='" . $sT . ' ~ ' . $eT . "'></select>";
				break;
			case "extraDate": 
				echo '<input type="date" name="bDate" value="'.date("Y-m-d", strtotime($r["bDate"])).'" />';
				break;
			default: echo $r[$editfnA[$i]];	
  	} ?></td>
  </tr><?php } ?>

<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
    <input type="reset" value="重新輸入" class="btn">&nbsp; 
    <button class="btn" onClick="history.back()">取消編輯</button>
  </td>
</tr>
</table>
</form>
</body>
<script type="text/javascript">
	$(function () {
		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "placeTimeEDIT",
				date : $("input[name='bDate']").val(),
				id   : $("input[name='places[]']:checked").val()
			},
			dataType: 'json',
			success: function(d){
				$("select[name='time']").empty();
				var defaultVal = $("select[name='time']").attr('data-default');
				console.log(defaultVal);
				$.each(d, function(k,v){
					if(defaultVal == v) $("select[name='time']").append(new Option(v, v, false, true));
					else $("select[name='time']").append(new Option(v, v, false, false));
				});
			}
		});
	});
	$("input[name='bDate']").change(function(){
		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "placeTimeEDIT",
				date : $(this).val(),
				id   : $("input[name='places[]']:checked").val()
			},
			dataType: 'json',
			success: function(d){
				$("select[name='time']").empty();
				$.each(d, function(k,v){
					$("select[name='time']").append(new Option(v, v));
				});
			}
		});
	});
	$("input:radio").change(function(){
		$.ajax({ 
			url:  'api.php',
			type: "POST",
			data: {
				mode : "placeTimeEDIT",
				date : $("input[name='bDate']").val(),
				id   : $(this).val()
			},
			dataType: 'json',
			success: function(d){
				$("select[name='time']").empty();
				$.each(d, function(k,v){
					$("select[name='time']").append(new Option(v, v));
				});
			}
		});
	});
</script>
</html>
