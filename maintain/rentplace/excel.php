<?php
	error_reporting(0);
	session_start();
	if(!isset($_SESSION['vid'])) header("Location: /index.php");

	$filename = '場地借用申請-統計資料'.date("Y-m-d H:i:s",time()).'.xlsx';
	header("Content-type:application/vnd.ms-excel");
	header("Content-Disposition:attachment;filename=$filename");
	header("Pragma:no-cache");
	header("Expires:0");

	include_once("init.php");
	require_once('../../vendor/autoload.php');
	// require_once('../Classes/PHPExcel.php');
	// require_once('../Classes/PHPExcel/IOFactory.php');

	$objPHPExcel = new PHPExcel(); 
	$objPHPExcel->setActiveSheetIndex(0);
	$sheet = $objPHPExcel->getActiveSheet();

	switch ($_POST['mode']) {
		case 'statistics':
			$title = array('西元年度','民國年度','月份','場地編號','場地','場次總計','人數總計');
			$fnAry = array('Year','ROCYear','month','places','title','rentCount','peopleCount');

			$sql   = "select DATE_FORMAT(a.bDate, '%Y') as Year, DATE_FORMAT(a.bDate, '%Y')-1911 as ROCYear, DATE_FORMAT(a.bDate, '%m') as month, a.places, b.title, sum(a.peoples) as peopleCount, count(1) as rentCount from $tableName a join space b ON(a.places = b.id) WHERE 1";
			if(!empty($_POST['excelSql'])) $sql .= " AND ".$_POST['excelSql'];
			$sql  .= " group by a.places";
			$stmt = $db->prepare($sql);
			$stmt->execute();
			// $rs    = db_query($sql);

			//轉換日期區間
			$day   = explode('or', $_POST['excelSql']);
			$day   = explode('BETWEEN', $day['0']);
			$day   = explode('AND', str_replace("'",'',$day['1']));
			$bTime = $day['0'];
			$eTime = str_replace(')','',$day['1']);
			$bDayArr = explode('-', date('Y-m-d',strtotime($bTime)));
			$bDayArr[0] -= 1911;
			$bTime = implode('.', $bDayArr);
			$eDayArr = explode('-', date('Y-m-d',strtotime($eTime)));
			$eDayArr[0] -= 1911;
			$eTime = implode('.', $eDayArr);

			//標題
			$sheet->mergeCells("A1:F1");
			$sheet->setCellValue("A1","愛心家園");
			$sheet->mergeCells("A2:F2");
			$sheet->setCellValue("A2","場地借用申請統計($bTime~$eTime)");
			$sheet->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ftAryCount = count($title);
			for($i = 0; $i < $ftAryCount; $i++) {
				$sheet->setCellValue(chr(65+$i).'3', $title[$i]);
			}
			$i = 4;
			//內容
			// while  ($r = db_fetch_array($rs) ) {
			while($r = $stmt->fetch()) {
				$sheet->setCellValueExplicit('A'.$i, $r['Year']);
				$sheet->setCellValueExplicit('B'.$i, $r['ROCYear']);
				$sheet->setCellValueExplicit('C'.$i, $r['month']);
				$sheet->setCellValueExplicit('D'.$i, $r['places']);
				$sheet->setCellValueExplicit('E'.$i, $r['title']);
				$sheet->setCellValueExplicit('F'.$i, $r['rentCount']);
				$sheet->setCellValueExplicit('G'.$i, $r['peopleCount']);
				$i++;
			}
			break;
		case 'allData':
			$title = array('單號','申請單位','負責人','聯絡人','地址','電話','傳真','手機','EMail','活動名稱','借用場地','借用時間(起)','借用時間(迄)','使用人數','額外借用器材/餐點','發票','抬頭','統一編號','備註','辦理狀況','是否繳費','價格','優惠折扣');
			$fnAry = array('id','applier','leadman','contactman','address','tel','fax','mobile','email','title','placeName','bDate','eDate','peopleNum','option','isInv','invoiceNo','invoiceTitle','UserNote','status','isPay','price','fold');
			$status = array('0'=>'不核准','1'=>'待處理','2'=>'核准','3'=>'已取消');
			$isInv  = array('2'=>'二聯式發票','3'=>'三聯式發票');
			$isPay  = array('0'=>'未繳費','1'=>'已繳費');

			$sql   = "select a.*, b.title as placeName from $tableName a join space b ON(a.places = b.id) WHERE 1";
			if(isset($_POST['excelSql'])) $sql .= " AND ".$_POST['excelSql'];
			// $rs    = db_query($sql);
			$stmt = $db->prepare($sql);
			$stmt->execute();

			//轉換日期區間
			$day   = explode('or', $_POST['excelSql']);
			$day   = explode('BETWEEN', $day['0']);
			$day   = explode('AND', str_replace("'",'',$day['1']));
			$bTime = $day['0'];
			$eTime = str_replace(')','',$day['1']);
			$bDayArr = explode('-', date('Y-m-d',strtotime($bTime)));
			$bDayArr[0] -= 1911;
			$bTime = implode('.', $bDayArr);
			$eDayArr = explode('-', date('Y-m-d',strtotime($eTime)));
			$eDayArr[0] -= 1911;
			$eTime = implode('.', $eDayArr);

			//標題
			$sheet->mergeCells("A1:W1");
			$sheet->setCellValue("A1","愛心家園");
			$sheet->mergeCells("A2:W2");
			$sheet->setCellValue("A2","場地借用申請清單($bTime~$eTime)");
			$sheet->getStyle('A1:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ftAryCount = count($title);
			for($i = 0; $i < $ftAryCount; $i++) {
				$sheet->setCellValue(chr(65+$i).'3', $title[$i]);
			}
			$i = 4;
			//內容
			// while  ($r = db_fetch_array($rs) ) {
			while($r = $stmt->fetch()) {
				$sheet->setCellValueExplicit('A'.$i, $r['id']);
				$sheet->setCellValueExplicit('B'.$i, $r['applier']);
				$sheet->setCellValueExplicit('C'.$i, $r['leadman']);
				$sheet->setCellValueExplicit('D'.$i, $r['contactman']);
				$sheet->setCellValueExplicit('E'.$i, $r['address']);
				$sheet->setCellValueExplicit('F'.$i, $r['tel']);
				$sheet->setCellValueExplicit('G'.$i, $r['fax']);
				$sheet->setCellValueExplicit('H'.$i, $r['mobile']);
				$sheet->setCellValueExplicit('I'.$i, $r['email']);
				$sheet->setCellValueExplicit('J'.$i, $r['title']);
				$sheet->setCellValueExplicit('K'.$i, $r['placeName']);
				$sheet->setCellValueExplicit('L'.$i, $r['bDate']);
				$sheet->setCellValueExplicit('M'.$i, $r['eDate']);
				$sheet->setCellValueExplicit('N'.$i, $r['peopleNum']);
				$sheet->setCellValueExplicit('O'.$i, $r['option']);
				$sheet->setCellValueExplicit('P'.$i, $isInv[$r['isInv']]);
				$sheet->setCellValueExplicit('Q'.$i, $r['invoiceNo']);
				$sheet->setCellValueExplicit('R'.$i, $r['invoiceTitle']);
				$sheet->setCellValueExplicit('S'.$i, $r['UserNote']);
				$sheet->setCellValueExplicit('T'.$i, $status[$r['status']]);
				$sheet->setCellValueExplicit('U'.$i, $isPay[$r['isPay']]);
				$sheet->setCellValueExplicit('V'.$i, $r['price']);
				$sheet->setCellValueExplicit('W'.$i, $r['fold']);

				$i++;
			}

			break;
		default:
			# code...
			break;
	}
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('php://output');
?>