<?php
	session_start();
	//::帶参數傳入(整頁方式)
	$cpPage = "http://".$_SERVER['SERVER_NAME'];  		//首頁 網址
	$cpRoot = str_replace("\\","/",realpath('../'));  //檔案 真實路徑		
	//::直接開(編輯按鈕)的圖檔路徑
	$_SESSION["heroot"] = $cpRoot.'/data/sys/';
	$_SESSION["heurl"]  = $cpPage.'/data/sys/';

	//::format = Type, Link Path, Target, bullet picture
	if(!isset($_SESSION['Account']) || $_SESSION['Account']=='') {
		$Menu = array(
	      	'登入'     => array(0,'login.php','main','loginB.gif'),
			'訪客首頁' => array(0,'/','_parent','homeB.gif')
		);	
	} else {
		switch($_SESSION["privilege"]) {
			case 255: 
				$Menu = array(
						'主選單維護	'=>	array(0,'menu/list.php?filter=0','main',''),
						'網站貼紙區	'=>	array(0,'stickers/list.php','main',''),
						'首頁貼紙區	'=>	array(0,'stickers2/list.php','main',''),
						'首頁輪撥圖片'=>	array(0,'newspic/list.php','main',''),
						'關於愛心家園' => array(0,"../htmledit.php?fn=about.htm",'main',''),
						'最新消息' =>	array(0,'contents/list.php','main',''),
						'市府最新消息' =>	array(0,'contentsCity/list.php','main',''),
						'認識愛心家園' =>	array(
							'成立緣起' => array(0,"../htmledit.php?fn=inc_cause.htm",'main',''),
							'組織圖' => array(0,"../htmledit.php?fn=inc_menu2_1.html",'main',''),
							'大事件' => array(0,"history/list.php",'main',''),
							'得獎紀錄' => array(0,"awards/list.php",'main',''),
							'服務成果' => array(0,"../htmledit.php?fn=inc_menu2_4.html",'main',''),
							'計畫執行' => array(0,"../htmledit.php?fn=inc_menu2_5_1.htm",'main','')												
						 ),
						'我們的服務' =>	array(
							'認識心智障礙' => array(0,"../htmledit.php?fn=inc_service01.htm",'main',''),
							'日間托育' => array(0,"../htmledit.php?fn=inc_menu3_2.htm",'main',''),
							'早期療育中心' => array(0,"../htmledit.php?fn=inc_menu3_3_1.htm",'main',''),
							'寶寶啟蒙中心' => array(0,"../htmledit.php?fn=inc_menu3_3_2.htm",'main',''),
							'臨托短托' => array(0,"../htmledit.php?fn=inc_menu3_4.htm",'main',''),
							'職業訓練' => array(0,"../htmledit.php?fn=inc_menu3_5.htm",'main',''),
							'瑪利亞社區就業中心' => array(0,"../htmledit.php?fn=inc_menu3_6_1.htm",'main',''),
							'瑪利亞庇護就業中心' => array(0,"../htmledit.php?fn=inc_menu3_6_2.htm",'main',''),
							'醫療復健' => array(0,"../htmledit.php?fn=inc_menu3_7.htm",'main',''),
							'輔具服務' => array(0,"../htmledit.php?fn=inc_menu3_8.htm",'main',''),
							'運動休閒' => array(0,"../htmledit.php?fn=inc_menu3_9.htm",'main',''),
							'讀書閱覽' => array(0,"../htmledit.php?fn=inc_menu3_10.htm",'main',''),
							'教育訓練' => array(0,"../htmledit.php?fn=inc_menu3_11.htm",'main',''),
							'按摩服務' => array(0,"../htmledit.php?fn=inc_menu3_12.htm",'main','')
						 ),
						'聯絡我們' => array(0,"../htmledit.php?fn=inc_contact.html",'main',''),
						'場地管理' =>	array(
							'場地樓層' => array(0,"spaceClass/list.php",'main',''),
							'場地空間' => array(0,"space/list.php",'main',''),
							'場地借用物品/便當' => array(0,"space_item/list.php",'main','')
						),
						'關閉場地時間查詢' => array(0,'space/booking_close_View.php','main',''),
	 					'場地借用申請' => array(0,'rentplace/list.php','main',''),
	 					'場地借用時間查詢' => array(0,'../rentPlace.php?userGroup=99&timeS=&act=chgUnit&visitId=B03&view=maintain','_blank',''),
	 					'場地借用黑名單紀錄' => array(0,'rentBlack/list.php','main',''),
	 					'場地借用Mail修改' => array(0,'../htmledit.php?fn=mail/rentPlaceUserMail.html','main',''),
	 					'場地租借文件' => array(0,'rentPlaceFiles/list.php','main',''),
						'預約參訪報名表' => array(0,'visitform/list.php','main',''),
						'交通資訊' => array(0,"../htmledit.php?fn=inc_map.htm",'main',''),
						//'場地借用申請' => array(0,'#','main',''),			
						'帳號管理'    => array(0,'admins/list.php','main',''),			
						//'文件管理'    => array(0,'../data/doc/','main',''),					
						'登出'    => array(0,'logout.php','main','logoutB.gif'),
						'訪客首頁' => array(0,'/','_parent','homeB.gif')
					);	break;
			case 11:
				$Menu = array(
					'變更密碼' => array(0,'admins/changepwd.php','main',''),
					'預約參訪報名表' => array(0,'visitform/list.php','main',''),
					'登出'    => array(0,'logout.php','main','logoutB.gif'),
					'訪客首頁' => array(0,'/','_parent','homeB.gif')
				);	break;
			case 15: //場地櫃檯
				$Menu = array(
					'場地借用查詢' => array(0,'rentplace/list.php','main',''),
					'登出'    => array(0,'logout.php','main','logoutB.gif'),
					'訪客首頁' => array(0,'/','_parent','homeB.gif')
				);
				break;
			case 16: //場地管理
				$Menu = array(
					'場地管理' =>	array(
						'場地樓層' => array(0,"spaceClass/list.php",'main',''),
						'場地空間' => array(0,"space/list.php",'main',''),
						'場地借用物品/便當' => array(0,"space_item/list.php",'main','')
					),
					'變更密碼' => array(0,'admins/changepwd.php','main',''),
					'關閉場地時間查詢' => array(0,'space/booking_close_View.php','main',''),
					'場地借用申請' => array(0,'rentplace/list.php','main',''),
					'場地借用時間查詢' => array(0,'../rentPlace.php?userGroup=99&timeS=&act=chgUnit&visitId=B03&view=maintain','_blank',''),
					'場地借用黑名單紀錄' => array(0,'rentBlack/list.php','main',''),
					'登出'    => array(0,'logout.php','main','logoutB.gif'),
					'訪客首頁' => array(0,'/','_parent','homeB.gif')
				);	break;
			case 1:
				$Menu = array(
					'變更密碼' => array(0,'admins/changepwd.php','main',''),
					'登出'    => array(0,'logout.php','main','logoutB.gif'),
					'訪客首頁' => array(0,'/','_parent','homeB.gif')
				);	break;								
		}
	}
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="matintain.css" rel="stylesheet" type="text/css">
<title>主選單</title>
<script>
function swtichDiv(ly,img) {
	ly.style.display = (ly.style.display=='none'?'':'none');
	img.src = (ly.style.display=='none'?'images/fold.gif':'images/open.gif');
}
</script>
<style type="text/css">
<!--
a:link { color: #224B59; text-decoration:none }
a:visited {	color: #224B59;	text-decoration: none;}
a:hover {	color: #FF0000;	text-decoration: none;}
a:active { color: #FFFF00; text-decoration: none;	font-weight: bold;}
.itemTxt {
	font-family: Verdana, "細明體";
	font-size: 16px;
}
.itemTxt2 {
	font-family: Verdana, "細明體";
	font-size: 13px;
}
body {
	margin:0;
}
-->
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

<body>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="4" class="top"></td>
    <td valign="top" class="map-c">
			<table width="100%" border="0">
			<?php $seq =1 ?>
			<?php 
				foreach($Menu as $k => $v) if (isset($v[0]) && $v[0]===0) { //項目
			   	 $img = $v[3]=='' ? 'table.gif' : $v[3]; 
				   echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt'><a href='$v[1]'";
					 if($v[2]>'') echo "target='$v[2]'";
					 echo ">$k</a></td></tr>";
				} else {	//目錄
					 $lyName = "ly$seq";
					 $imName = "im$seq";
					 $img = 'fold.gif'; 
					 echo "<tr><td width='16'><img name='im$seq' src='images/$img' onclick='swtichDiv($lyName,this)' style='cursor:hand'></td>";
					 echo "<td class='itemTxt'><a href='javascript:swtichDiv($lyName,$imName)'>$k</a></td></tr>";
 					 $seq++;
					 //目錄清單
					 echo "<tr><td width='16'></td><td>";
					 echo "<div id='$lyName' style='display:none'><table width='100%'>";
					 foreach($v as $k2 => $v2)  {
					 		$img = $v2[3]=='' ? 'table.gif' : $v2[3];
							echo "<tr><td width='16'><img src='images/$img'></td><td class='itemTxt2'><a href='$v2[1]'";
					 		if($v2[2]>'') echo "target='$v2[2]'";
					 		echo ">$k2</a></td></tr>";
					 }
					 echo "</table></div>";
					 echo "</td></tr>";
				} 
			?>
			</table>
		</td>
	</tr>
</table>	
</body>
</html>
