<?php 
	include("init.php"); 
?>
<Html>
<Head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="<?=$extfiles?>append.css">
	<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
	<style type="text/css">
		.ui-widget {font-size:12px; }
		.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
		#d_time{padding-top:6px;}
		.colLabel{	width:120px;}
		.title{	text-align:center;	background-color:#E1E1E1;}
		.sTable{margin:10px;}
		#d_msg{color:#C00;padding-top:3px;}

		table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
		table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center;}

		.divBorder2 { /* 內容區 第二層外框 */
			padding:8px; 
			border:1px solid #CCC;
			background:#FFFFFF;
			font-size:12px;
			width:372px;
		}
		.delBtn{
			width:26px;
			cursor:pointer;
		}
		.divTitle{
			color:#fff;
			background-color:#900;
			border: 0px;
			padding: 5px;
			margin: 0px;
			font-size:12px;
			width:95%;
		}
		#d_setting_time{	
			position:fixed;
			left:20%;
			/*margin-left:-200px;*/
			top:30%;
			width:400px;
			display:none;
		}
		#tb_setting_time{	width:98%;}
		.dBottom{	background:#dedede; text-align:center;padding:8px;border:1px solid #ccc;width:372px;}
		.gray{
			cursor:pointer;
			display:inline-block;
			text-decoration:none;
			font-size:12px;
			color:#fff;
			color:rgba(255,255,255,1);
			padding:0.4em 0.2em 0.2em 0.2em;
			
			border-style:solid;
			border-width:1px;
			border-radius:4px;
			box-shadow:0 1px 1px rgba(255,255,255,0.5) inset;
			
			background:#666;
			border-color:#999;
			border-color:rgba(0,0,0,0.1);
			color:#fff;
		}
		.nonTip{	color:#C00;}
		.tip{color:#c00;}
	</style>
	<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
	<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>
	<script Language="JavaScript" src="<?=$extfiles?>append.js"></script> 
	<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
	<title><?=$pageTitle?> - 新增</title>
	<script>
		$(function() {
			$('input[name="id"]').attr('required', true);
			$('input[name="sId"]').attr('required', true);
			$('input[name="title"]').attr('required', true);

			$('input[name="size"]').attr('pattern', '[0-9]+');

			$('input[name="peopleNum"]').attr('pattern', '[0-9]+');
			
			$('input[name="cost"]').attr('pattern', '[0-9]+');
			$('input[name="cost"]').attr('required', true);
		});
	</script>
	<script type="text/javascript">
		var idList = <?php  echo json_encode($idList); ?>;
		$(document).ready(function(){
			$('input[name="id"]').change(function() {
				var valid = 0;
				var a = $(this).val();
				var b = a.toLowerCase();
				var c = a.toUpperCase();
				// console.log(a,b,c);
				
				for(var i in idList){
					if(a == idList[i] || b == idList[i] || c == idList[i])
					{
						valid = 1;
					}
				}
				if(valid == 1){
					$(this)[0].setCustomValidity('編碼重複，請重新填寫');
				}else{
					$(this)[0].setCustomValidity('');
				}
			});
		});
	</script>
	<script type="application/javascript">
		var dayData=[];		//記錄有哪幾天有開放時段
		var timeStart=[];	//記錄開始時間
		var timeEnd=[];		//記錄結束時間
		var day;			//目前正在編輯哪一天
		$(function() {
			var icons=[];
			icons['doc']='icon_doc.gif';
			icons['docx']='icon_doc.gif';
			icons['pdf']='icon_pdf.gif';
			icons['ppt']='icon_ppt.gif';
			icons['pptx']='icon_ppt.gif';
			icons['txt']='icon_txt.gif';
			icons['xls']='icon_xls.gif';
			icons['xlsx']='icon_xls.gif';
			icons['zip']='icon_zip.gif';
			icons['rar']='icon_rar.gif';
		 	$(".multiPhoto").change(function(){
				var that = $(this);
				var oTD = that.parents("td").eq(0);
				var fName;
				$(this.files).each(function(k,v){
					var fReader = new FileReader();
					fName=v['name'];
					var ary=fName.split(".");
					fReader.readAsDataURL(this);			
					fReader.onloadend = function(event){
						var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
						oItem.after(oItem.clone(true)).show();
						var img = oItem.find("img").get(0);
						img.style.padding='1';
						if(icons[ary[1]]){
							img.height=16;
							img.src='/images/'+icons[ary[1]];
						}else if(ary[1].toLowerCase()=="jpg" || ary[1].toLowerCase()=="png" || ary[1].toLowerCase()=="gif"){
							img.src=event.target.result;	
							img.height='80';
						}else{
							img.height=16;
							img.src='/images/icon_txt.gif';
						}
						var span=oItem.find("span").get(0);
						$(span).html(v['name']);
						oItem.get(0).input = that;
						var oNewFile = that.clone(true);
						oNewFile.val('');
						that.hide().after(oNewFile);
					}
				});
			});
			var delARR = new Array();
			$(".photoDel").click(function(){
				// alert($(this).prev("span").text());
				console.log('do del');
				var oItem = $(this).parents(".photoItem");
				delARR.push($(this).prev("span").text());
				// if (oItem.get(0).input){
				// 	oItem.get(0).input.remove();
				// }
				oItem.remove();
				console.log(delARR);
				$('input[name="NotInsertImg"]').val(delARR);
			});
		});
		function formvalid(tfm) { 
			var uperLimit=parseInt(tfm.uper_limit.value);
			if(uperLimit=='') {alert('請輸入研習人數上限'); tfm.uper_limit.focus();	return false;}
			if(tfm.start_booking.value=="") {alert('請輸入開放預約'); tfm.start_booking.focus();return false;}
			if(tfm.early_cancel.value=="") {alert('請輸入取消預約天數'); tfm.early_cancel.focus();return false;}
			if(isNaN(tfm.start_booking.value)) {alert('開放預約請輸入數字'); tfm.start_booking.focus();return false;}
			//::檢查是否有設定時段
			var objs=document.getElementsByName('week_day');
			var ck=false;
			var day;
			var dData=[];
			var tData=[];
			var str;
			var bTime=true;
			for(var i=0;i<objs.length;i++){
				if(objs[i].checked){
					day=objs[i].value 
					ck=true;
					str=[];
					if(dayData[day]){
						for(var k in timeStart[day]){
							str.push(timeStart[day][k]+","+timeEnd[day][k]);
						}
						dData.push(day);
						tData.push(str.join(";"));
					}else{
						bTime=false; break;
					}
				}
			}
			if(!ck){	alert('請選擇每週開放日期');	return false;}
			if(!bTime){	alert('開放時段未設定');		return false;}
			$('#week_day_v').val(dData.join(","));
			$('#tData').val(tData.join("#"));
			return true;
		}
		function settingAll(day){
			var frist = '';
			$.each($("input[name='week_day']:checked"),function(k,v){
				if(day == 'on'){
					if($(v).val() == 0 || $(v).val() == 6) return;
					if(frist == '') frist = $(v).val();
					if(dayData[frist] == 0) addItem();
					dayData[$(v).val()] = dayData[frist];
					timeStart[$(v).val()] = timeStart[frist];
					timeEnd[$(v).val()] = timeEnd[frist];
				}
				if(day == 'off'){
					if($(v).val() == 0 || $(v).val() == 6){
						if(frist == '') frist = 6;
						dayData[$(v).val()] = dayData[frist];
						timeStart[$(v).val()] = timeStart[frist];
						timeEnd[$(v).val()] = timeEnd[frist];
					}
				}
			});
			showList();
		}
		function settingTime(d,obj){//設定時段
			if(timeStart[d]==undefined){	dayData[d]=undefined;}
			$('#d_mask').show();
			day=d;
			var word=['日','一','二','三','四','五','六'];
			var str="<table class='grid' id='tb_setting_time'>";
			str+="<tr><td colspan='3'><input type='button' value='新增時段' onclick='addItem()' style='float:left'/></td></tr>";
			str+="<tr><td >開放時間</td><td>結束時間</td><td>刪除</td></tr>";
			str+="</table>";
			$("#d_container").html(str);
			var start,end;
			if(dayData[day]==undefined){
				addItem();
			}else{//秀出已設好的時段
				for(var i in timeStart[day]){ 
					start=timeStart[day][i].split(":");
					end=timeEnd[day][i].split(":");
					newItem(i,start[0],start[1],end[0],end[1]);
				}
			}
			
			$('#d_setting_time').show();
			$('#dTitle').html("週"+word[d]);
		}
		function showList(){//讓使用者更好識別
			var objs=document.getElementsByName('week_day');	
			var str=[];
			var tStr;
			var d;
			var word=['日','一','二','三','四','五','六'];
			for(var i=0;i<objs.length;i++){
				if(objs[i].checked){
					tStr=[];
					d=objs[i].value;
					if(timeStart[d]!=undefined){	
						for(var k in timeStart[d]){
							if(timeStart[d][k].length == 4){
								timeStart[d][k] = "0"+timeStart[d][k];
							}
							tStr.push(timeStart[d][k]+" ~ "+timeEnd[d][k]);
						}
						tStr.sort();
					}else{
						tStr.push("<span class='nonTip'>時段尚未設定</span>");
					}
					str.push("週"+word[d]+"："+tStr.join(" , "));
				}
			}
			if(str.length==0){
				$('#td_list').html("<span class='nonTip'>尚未設定</span>");
			}else{
				$('#td_list').html(str.join("<br>"));
			}
		}
		
		function newItem(index,start_h,start_m,end_h,end_m){
			var tb=document.getElementById('tb_setting_time');
			var tbObj=tb.tBodies[0];
			var newRow=document.createElement('tr');
			var newcell1=document.createElement('td');
			var newcell2=document.createElement('td');
			var newcell3=document.createElement('td');
			newRow.style.height='30px';
			newRow.bgColor='#E3E3E3'; 
			var str="<select class='input' id='sel_start_h_"+index+"'/>"; 
			str+="<option selected></option>";
			var selected='';
			for(var j=1;j<=24;j++){
				if(start_h==j){	selected='selected';}else{selected='';}
				str+="<option "+selected+">"+j+"</option>";
			}
			str+="</select>&nbsp;時&nbsp;";
			str+="<select class='input' id='sel_start_m_"+index+"'/>"; 
			str+="<option selected></option>";
			
			var  k='';
			for(var j=0;j<=60;j+=10){
				if(start_m==j){	selected='selected';}else{selected='';}
				if(j==0){	k="00";}else{	k=j;}
				str+="<option "+selected+">"+k+"</option>";
			}
			str+="</select>&nbsp;分&nbsp;";
			newcell1.align='center';
			newcell1.innerHTML=str
			str="<select class='input' id='sel_end_h_"+index+"'/>"; 
			str+="<option selected></option>";
			for(var j=1;j<=24;j++){
				if(end_h==j){	selected='selected';}else{selected='';}
				str+="<option "+selected+">"+j+"</option>";
			}
			str+="</select>&nbsp;時&nbsp;";
			str+="<select class='input' id='sel_end_m_"+index+"'/>"; 
			str+="<option selected></option>";
			for(var j=0;j<60;j+=10){
				if(end_m==j){	selected='selected';}else{selected='';}
				if(j==0){	k="00";}else{	k=j;}
				str+="<option "+selected+">"+k+"</option>";
			}
			str+="</select>&nbsp;分&nbsp;";
			newcell2.align='center';
			newcell2.innerHTML=str;
			newcell3.align='center'; 
			newcell3.innerHTML='<img src="/images/del.png" class="delBtn" onClick="delItem(this,event)">'; 
			newRow.appendChild(newcell1);
			newRow.appendChild(newcell2);
			newRow.appendChild(newcell3);
			tbObj.appendChild(newRow);
		}
		function addItem(){//新增開放時段
			var index=0;
			if(dayData[day]){
				index=parseInt(dayData[day])+1;
				dayData[day]=index;
			}else{
				dayData[day]=index.toString();
			}
			newItem(index,"9","","12","");
		}
		function delItem(obj,e){//刪除開放時段
			var evt = e || window.event;
			var current = evt.target || evt.srcElement;
			var currRowIndex=current.parentNode.parentNode.rowIndex; 
			var tb = document.getElementById("tb_setting_time");
			if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
			if(timeStart[day]!=undefined) {timeStart[day].splice(currRowIndex, 1);}
			if(timeEnd[day]!=undefined) {timeEnd[day].splice(currRowIndex, 1);}
			if(dayData[day]>0){	
				dayData[day]=parseInt(dayData[day])-1;
			}else if(dayData[day]==0){	dayData[day]=undefined;}
		}
		function doCancel(){
			if(timeStart[day]==undefined){	dayData[day]=undefined;}
			$('#d_setting_time').hide();
		}
		function saveTime(){
			var bSave=true;
			if(dayData[day]!=undefined){	
				timeStart[day]=[];
				timeEnd[day]=[];
				var start_h,start_m,end_h,end_m;
			}
			for(var i=0;i<=dayData[day];i++){
				if($('#sel_start_h_'+i).val()==undefined)	continue;
				if($('#sel_start_h_'+i).val()==""){alert('請選擇幾點開放');	bSave=false;break;}
				if($('#sel_end_h_'+i).val()==""){alert('請選擇幾點結束');	bSave=false;break;}
				start_h=parseInt($('#sel_start_h_'+i).val());
				start_m=parseInt($('#sel_start_m_'+i).val());
				end_h=parseInt($('#sel_end_h_'+i).val());
				end_m=parseInt($('#sel_end_m_'+i).val());
				if(start_h>end_h){	alert('開始時間與結束時間有誤，請重新選擇');	bSave=false;break;}
				if(start_h==end_h && start_m>=end_m){	alert('開始時間與結束時間有誤，請重新選擇');bSave=false;	break;}
				timeStart[day][i]=$('#sel_start_h_'+i).val()+":"+$('#sel_start_m_'+i).val();
				timeEnd[day][i]=$('#sel_end_h_'+i).val()+":"+$('#sel_end_m_'+i).val();
			}
			if(bSave){	$('#d_setting_time').hide(); }
			showList();
		}
	</script>
</Head>

<body class="page">
<form action="doappend.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return formvalid(this)">
	<input type="hidden" name="NotInsertImg" value="" />
	<table align="center" class="sTable" width="100%" border="0" CellSpacing="0" CellPadding="4">
		<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【新增作業】</td></tr>
		<?php for ($i=0; $i<count($newfnA); $i++) { 
			?><tr class="<?= $newetA[$i]=='hidden'?'colLabel_Hidden':''?>">
			<td align="right" class="colLabel"><?=$newftA[$i]?></td>
  		<td><?php switch ($newetA[$i]) {
			case "readonly": echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input' readonly>"; break;
			case "text"    : 
				echo "<input type='text' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				if($newfnA[$i]=="start_booking"){	echo "<span class='tip'> * 設定幾個月前，開放預約(單位:月)</span>";}
				if($newfnA[$i]=="notice_day"){	echo " (單位:天)</span>";}
				if($newfnA[$i]=="early_cancel"){	echo "<span class='tip'> * 設定登錄日期後幾天，可以取消預約(單位:天)</span>";}
				break;
			case "textbox" : echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='input'>$newfvA[$i]</textarea>"; break;
 			case "textarea": echo "<textarea name='$newfnA[$i]' cols='$newflA[$i]' rows='$newfhA[$i]' class='ckeditor'>$newfvA[$i]</textarea>"; break;
  			case "checkbox": 
				if($newfnA[$i]=="week_day"){
					echo "<input type='checkbox' name='$newfnA[$i]' value='1' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週一<input value="設定時段" type="button" class="gray" onClick="settingTime(1,this)" />&nbsp;';
					echo "<input type='checkbox' name='$newfnA[$i]' value='2' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週二<input value="設定時段" type="button" class="gray" onClick="settingTime(2,this)" />&nbsp;';
					echo "<input type='checkbox' name='$newfnA[$i]' value='3' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週三<input value="設定時段" type="button" class="gray" onClick="settingTime(3,this)" />&nbsp;';
					echo "<input type='checkbox' name='$newfnA[$i]' value='4' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週四<input value="設定時段" type="button" class="gray" onClick="settingTime(4,this)" />&nbsp;';
					echo "<input type='checkbox' name='$newfnA[$i]' value='5' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週五<input value="設定時段" type="button" class="gray" onClick="settingTime(5,this)" />';
					echo "<input type='button' value='平日時段套用' onClick='settingAll(\"on\")'><br><br>";
					echo "<input type='checkbox' name='$newfnA[$i]' value='6' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週六<input value="設定時段" type="button" class="gray" onClick="settingTime(6,this)" />&nbsp;';
					echo "<input type='checkbox' name='$newfnA[$i]' value='0' onclick='showList()' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>週日<input value="設定時段" type="button" class="gray" onClick="settingTime(0,this)" />&nbsp;';
					echo "<input type='button' value='假日時段套用' onClick='settingAll(\"off\")'>";
                }else{
					echo "<input type='checkbox' name='$newfnA[$i]' value='$newfvA[$i]' class='input'"; if ($newfvA[$i]=="true") echo " checked"; echo '>'; 
				}
				break;
			case "date"    : 
				echo "<input type='text' id='$newfnA[$i]' name='$newfnA[$i]' size='$newflA[$i]' value='$newfvA[$i]' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$newfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$newfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$newfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
				break;
			case "select"  : echo "<select name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; foreach($newfvA[$i] as $k=>$v) echo '<option value="'.$k.'">'.$v.'</option>'; echo "</select>"; break;	
			case "file"    : echo "<input type='file' name='$newfnA[$i]' size='$newflA[$i]' class='input'>"; break;	
			case "hidden"  : echo "<input type='hidden' name='$newfnA[$i]' value='$newfvA[$i]'>"; break;
			case "multiPhoto":
				echo "<input type='file' name='".$newfnA[$i]."[]' size='$newflA[$i]' class='input multiPhoto' multiple>"; 
				echo "<div class='photoPreview' >";
				echo "<div class='photoItem'><img /><span id='fName' name='fName[]'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;	
			
  	} ?></td>
	</tr><?php } ?>
  	<tr>
	    <td class="colLabel" align="right" >目前開放</td>
	    <td id='td_list'><span class='nonTip'>尚未設定</span></td>
    </tr>
	<tr>
		<td colspan="2" align="center" class="rowSubmit">
			<input type="submit" value="確定新增" class="btn">&nbsp;
      		<input type="reset" value="重新輸入" class="btn">&nbsp;
      		<button class="btn" onClick="history.back()">取消新增</button>
      		<input type="hidden" name='week_day_v' id='week_day_v'>
            <input type="hidden" name='tData' id='tData'>
		</td>
	</tr>
</table>
</form>

<div id='d_setting_time'>
    <div id='dTitle' class='divTitle'></div>
    <div class='divBorder2' id='d_container'>
    </div>
    <div class='dBottom'><input type="button" value='確定' onclick='saveTime()'/>&nbsp;<input type="button" value='取消' onclick='doCancel()'/></div>
</div>

</body>
</html>
