<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<16) {
		header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');
    include_once('../../connect_db.inc.php');	
	$bE = true;	//異動權限
	$pageTitle = "場地空間";
	$tableName = "space";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/space/';
	$delField = 'Ahead';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$sql = "select id,title from spaceclass order by id asc";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	// $rs = db_query($sql,$conn);
	$data = array();
	// while($r=db_fetch_array($rs)){
	while($r = $stmt->fetch()) {
		$data[ $r['id'] ] = $r['title'];
	}

	$compareSql = "select id from space";
	// $rsx = db_query($compareSql,$conn);
	$stmt = $db->prepare($compareSql);
	$stmt->execute();
	$idList = array();
	// while($result=db_fetch_array($rsx)){
	while($result = $stmt->fetch()) {
		$idList[$result['id']] = $result['id'];
	}

	$userGroup = array('0'=>"全部",'1'=>"民眾",'2'=>"員工");
	// echo implode(',',$idList);
	// var_dump($id);
	// print_r($data);
	$filter = 'sId';
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "notes";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,sId,title,size,peopleNum,cost,notes,userGroup");
	$ftAry = explode(',',"編號,樓層,場地樓層名稱,坪數,人數,費用,空間設備,可借用分類");
	$flAry = explode(',',"10,,,,,,,,");
	$ftyAy = explode(',',"id,select,text,text,text,text,text,select2");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"id,sId,title,size,peopleNum,cost,notes,pic,userGroup,uper_limit,start_booking,notice_day,early_cancel,week_day");
	$newftA = explode(',',"編號,樓層,場地樓層名稱,坪數,人數,費用,空間設備,照片,可借用分類,研習人數上限,開放預約,需提前預約的天數,取消預約,每週開放");
	$newflA = explode(',',"10,,,,,,,,,,,,");
	$newfhA = explode(',',",,,,,,,,");
	$newfvA = array('',$data,'',0,0,'','','',$userGroup,'','','','','');
	$newetA = explode(',',"text,select,text,text,text,text,textbox,multiPhoto,select,text,text,text,text,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,sId,title,size,peopleNum,cost,notes,pic,userGroup,uper_limit,start_booking,notice_day,early_cancel,week_day");
	$editftA = explode(',',"編號,樓層,場地樓層名稱,坪數,人數,費用,空間設備,照片,可借用分類,研習人數上限,開放預約,需提前預約的天數,取消預約,每週開放");
	$editflA = explode(',',"10,,,,,,,,,,,,,");
	$editfhA = explode(',',",,,,,,,,,,,,,");
	$editfvA = array('',$data,'','','','','','',$userGroup,'','','','','');
	$editetA = explode(',',"text,select,text,text,text,text,textbox,multiPhoto,select,text,text,text,text,checkbox");
?>