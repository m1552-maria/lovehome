<?php
	header("Content-Type:text/html; charset=utf-8");
	session_start();
	// include "../../config.php";
    include_once('../../connect_db.inc.php');
	
	if(isset($_REQUEST['visitId'])){    
        $visitId = $_REQUEST['visitId'];
    }else{  
        $visitId = 'all';
    }
    
	//場地資料
	$spaceArr = array();
	$spaceArr[] = "<option value='all' selected>全部</option>";
	$sql = "SELECT a.id, a.title, b.title as FloorName FROM space a join spaceclass b on(a.sId = b.id)";
	// $rs  = db_query($sql);
	$stmt = $db->prepare($sql);
	$stmt->execute();
	// if(!db_eof($rs)){
		// while($r=db_fetch_array($rs)){
		while($r = $stmt->fetch()) {
			if($r['id'] == $visitId) $spaceArr[] = "<option value='".$r['id']."' selected>".$r['FloorName']." - ".$r['title']."</option>";
			else $spaceArr[] = "<option value='".$r['id']."'>".$r['FloorName']." - ".$r['title']."</option>";
		}
	// }

	//::萬年曆
	$day_array=array("日","一","二","三","四","五","六"); 
	$this_year=date("Y",time()); 
	$this_month=date("m",time());

	if(!isset($_SESSION['sched_y'])){//處理年份
		$y = $this_year; 
		$_SESSION['sched_y'] = $this_year;
	}else{	
		$y = intval($_SESSION['sched_y']);
	}
	if(!isset($_SESSION['sched_m'])){//處理月份
		$m = $this_month; 
		$_SESSION['sched_m']=$this_month;
	}else{	
		$m=intval($_SESSION['sched_m']);
	}

	$this_day=date("j",time()); 
	$title_string=$this_year."年".$this_month."月"; 
	$past_to_today=(intval(date("U",mktime(0,0,0,$m,$this_day,$y))/86400)); //1970年1月1日至今的天數 
	$past_to_this_month_first_day=(intval(date("U",mktime(0,0,0,$m,1,$y))/86400)); //1970年1月1日至本月份第一天的天數 
	$what_day_is_fisrt_day=date("w",mktime(0,0,0,$m,1,$y)); //這個月第一天是星期幾 
	$day_counter=1-$what_day_is_fisrt_day; //顯示這個月第一天的位置
	$how_many_days=mktime(0,0,0,$m+1,0,$y); 
	$how_many_days_this_month=(strftime("%d",$how_many_days)); //這個月有幾天 
	$displayY = $y;

	if($m>12){	
		$displayM=$m%12;
		$yAdd = floor($m/12);
		if($displayM == 0){ 
			$displayM = 12; 
			$yAdd = floor($m/12)-1;
		}
		$displayY+=$yAdd; 
	}

	if($m<=12 && $m>=1){ 
		$displayM = $m;
	}
	if($m<1) {
		$displayM=$m%12;
		$displayY-=1;
	}

	//當天是否有關閉時段
	// $sql = "select * from space_close where visitId = '$visitId' or visitId = 'all'";
	$query = "Select * "
			."From space_close "
			."Where visitId = :visitId "
			."Or visitId = 'all' ";
	$stmt = $db->prepare($query);
	$stmt->execute(array(
		'visitId' => $visitId
	));
	// $rs  = db_query($sql);
	$closeY=array();
	$closeM=array();
	$closeD=array();
	$notes =array();
	// if(!db_eof($rs)){
		// while($r=db_fetch_array($rs)){
		while($r = $stmt->fetch()) {
			array_push($closeY, $r['close_y']);
			array_push($closeM, $r['close_m']);
			array_push($closeD, $r['close_d']);
			array_push($notes,  $r['notes']);
		}
	// }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>

<link rel="stylesheet" href="../../Scripts/jquery-ui-timepicker-addon.css">
<link rel="stylesheet" href="../../Scripts/jquery-ui-1.7.2.custom.css">
<link rel="stylesheet" href="../../onlinebooking.css">
<style type="text/css">
table.grid td {   
	border: 1px solid #ccc;	
	text-align:center;
	font-family:Verdana, Geneva, sans-serif;
	font-size:12px;
	width:150px;
	padding-top:5px;
	padding-bottom:5px;
}
#tdTitle{	background:#a91d3b; color:#fff;padding:8px;}
#tdWeek{	background:#d59416; color:#fff;padding:8px;}
#sel_unit{	float:left;}
.tip{	color:#999;  }
.tip2{	color:#FC3;  fonts-size:12px; float:right;}
.tip3{	color:#C00;  font-weight:bold;}
.tip4{	 font-weight:bold;}
.tip5{	color:#F30; font-weight:bold;}
.tdNone{	background:#f0f0f0;color:#999;}
.tdNone2{	background:#ffffff;color:#999;}
.btnBack{	width:100px;}
.btn2 {
	width:60px;
	background: #808080;
	padding: 3px 6px 3px 6px;
	float:right;
}
.btn2:hover {
	background: #999;
	text-decoration: none;
}
.divTitle{	
	width:950px;
}
.divBorder2{
	width:954px;
}
.btn3 {
	width:120px;
}
.btnGreen{
  width:120px;
  -webkit-border-radius:5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  font-family: Arial;
  color: #ffffff;
  font-size: 12px;
  background: #005B00;
  padding: 4px 7px 4px 5px;
  text-decoration: none;
}
</style><!--<script type="text/javascript" src="/Scripts/jquery.js"></script>-->
<script type="text/javascript" src="/Scripts/jquery.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script>
<!--<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> -->
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script type="text/javascript">
function nextM(){
	$.ajax({ 
        type: "POST", 
        url: "../../rent_lib.php", 
        data: "act=doNexAndPrev&type=next"
    }).done(function( rs ) {
		location.href='booking_close_View.php?visitId='+$('[name="visitId"]').val();
	});
	
}
function prevM(){
	$.ajax({ 
	  type: "POST", 
	  url: "../../rent_lib.php", 
	  data: "act=doNexAndPrev&type=prev"
	}).done(function( rs ) {
		location.href='booking_close_View.php?visitId='+$('[name="visitId"]').val();
	});
	
}

function goBack(){
	location.href='list.php';
}

function chgPlace(){
	$("#chgPlaceFM").submit();
}

</script>
</head>
<body>
<form id="chgPlaceFM">
	關閉場地：<select name='visitId' onchange='chgPlace()'><?php echo join("",$spaceArr);?></select><br><br>
	<div class='divTitle' style="height:25px;">
	<?php echo $_SESSION["VISIT_UNITS"][$visitId]."關閉時段";	?>&nbsp;&nbsp;
	</div>
	<div class='divBorder2'>
		<table class='grid'>
		    <tr> <td colspan="7" id='tdTitle'>
		    <?php  $L=true;
				if($this_month<$m){	$L=true;}else{	$L=false;} 
			?>
			<?php if($L){	?><input type="button" value='<' class='btn' onclick="prevM()"/><?php	}	?>
		        &nbsp;&nbsp;<?=$displayY?>年<?=$displayM?>月&nbsp;&nbsp;
		        <input type="button" value='>' class="btn" onclick="nextM()" />
		        </td>
		    </tr>
		    <tr>
		    <?php
		    for($list_week=0;$list_week<7;$list_week++){ 
		         echo "<td id='tdWeek'>".$day_array[$list_week]."</td>"; //列出 日、一、二、三、四、五、六 
		    } 
		    ?>
		    </tr>
		    <?php
		    for($pcol=1;$pcol<=6;$pcol++){ //一個月最多有六週 (在月曆上顯示的)，最多印出六週（六列） 
		        if($day_counter>$how_many_days_this_month){ 
		            //只要遞增的day_counter變數大於本月份的天數（例如一月是31天），就會跳出迴圈；若是天數在6週內結束，就不會全都印完（每一列） 
		            break; 
		        } 
		        echo "<tr>"; 
		        for($prow=0;$prow<7;$prow++){ //一個星期有七天，所以是七行 
		            if($day_counter>0 && $day_counter<=$how_many_days_this_month){
		                $day=str_pad($day_counter,2,"0",STR_PAD_LEFT);
		                if($this_day>$day && $this_year==$displayY && $this_month==$displayM){
		                    $str='<br><br><span class="tip">已過期</span>';
		                	echo "<td class='tdNone2'>".$day_counter.$str."</td>"; 
		                }else{									
		                    $tmpY=str_pad($displayY,2,"0",STR_PAD_LEFT);
		                    $tmpM=str_pad($displayM,2,"0",STR_PAD_LEFT);
		                    $ck=false;
		                    $str='';
							
							$ck=true;
							$bClose=false;
							$notesStr = "";
							foreach($closeY as $k1=>$v1){
								if($tmpY!=$closeY[$k1])	continue;
								if($tmpM!=$closeM[$k1])	continue;
								if($day!=$closeD[$k1])	continue;
								if($dtlId[$k1]!=$k)	continue;
								$bClose   = true;
								$notesStr = $notes[$k1];
								break;
							}
							$d=$tmpY.'/'.$tmpM.'/'.$day;
							if($bClose){
								$str.="<br><br><label style='color:red;'>".$notesStr."</label><br><label style='color:red;'>關閉中</label>";
							}else{
								$str.="<br><br><label style='color:#005B00;'>開放中</label>";
							}
		                    if($ck){
		                        echo "<td><span class='tip4'>".$day_counter."</span>".$str."</td>"; 
		                    }else{
		                        echo "<td>".$day_counter.$str."</td>"; 
		                    }
		                }
		            }else{ 
		        		echo "<td>　</td>"; 
		            } 
		            $day_counter++; 
		        } 
		        echo "</tr>"; 
		    } 
		    ?>
		    <!--<td id='trWeek'>日</td><td id='trWeek'>一</td><td id='trWeek'>二</td><td id='trWeek'>三</td><td id='trWeek'>四</td><td id='trWeek'>五</td><td id='trWeek'>六</td></tr>-->
		</table>
	</div>
</form>
</body>
</html>