<?php
	// include "../../config.php";
    include_once('../../connect_db.inc.php');	
	session_start();
	$act = $_REQUEST['act'];

	switch($act){
		case "Range":
			$SDate = $_REQUEST['SDate'];
			$EDate = $_REQUEST['EDate'];
			$visitId = $_REQUEST['visitId'];
			echo setCloseR($SDate, $EDate, $visitId);
			break;
		case "setClose":
			$date = $_REQUEST['date'];
			$type = $_REQUEST['type'];
			$visitId = $_REQUEST['visitId'];
			echo setClose($date, $type, $visitId);
			break;
		case "RangeOpen":
			$SDate = $_REQUEST['SDate'];
			$EDate = $_REQUEST['EDate'];
			$visitId = $_REQUEST['visitId'];
			echo setOpenR($SDate, $EDate, $visitId);
			break;
	}

	function setOpenR($SDate, $EDate, $visitId){
		global $db;
		$RangeArr = array();

		$start_time = strtotime($SDate);
	    $end_time = strtotime($EDate);
	    $i=0;
	    while ($start_time <= $end_time){
	        $RangeArr[$i] = date('Y-m-d',$start_time);
	        $start_time   = strtotime('+1 day',$start_time);
	        $i++;
	    }
	    foreach ($RangeArr as $key => $value) {
	    	$DateArr = explode("-",$value);
	    	// $sql = "select * from space_close 
	    	// 		WHERE close_y='".$DateArr[0]."' and close_m='".$DateArr[1]."' and close_d='".$DateArr[2]."' and visitId = '".$visitId."'";
			// $rs  = db_query($sql);
			$r = getSpaceCloseRow($DateArr, $visitId);
			// if(!db_eof($rs)){
			if(!is_null($r)) {
				$sql = "delete from space_close where id='".$r['id']."'";
				// db_query($sql);
				$db->exec($sql);
			}
			//TO-DO: 以上這段根本可以直接下delete指令
	    }
	}

	function setCloseR($SDate, $EDate, $visitId){
		global $db;
		$RangeArr = array();

		$start_time = strtotime($SDate);
	    $end_time = strtotime($EDate);
	    $i=0;
	    while ($start_time <= $end_time){
	        $RangeArr[$i] = date('Y-m-d',$start_time);
	        $start_time   = strtotime('+1 day',$start_time);
	        $i++;
	    }
	    foreach ($RangeArr as $key => $value) {
			echo 'processing '.$value."<br/>\n";
	    	$DateArr = explode("-",$value);
	    	// $sql = "select * from space_close 
	    	// 		WHERE close_y='".$DateArr[0]."' and close_m='".$DateArr[1]."' and close_d='".$DateArr[2]."' and visitId = '".$visitId."'";
			// $rs  = db_query($sql);
			// if(db_eof($rs)){
			$row = getSpaceCloseRow($DateArr, $visitId);
			if(is_null($row)) {
				$sql = "insert into space_close (visitId,close_y,close_m,close_d,notes) 
						values ('".$visitId."','".$DateArr[0]."','".$DateArr[1]."','".$DateArr[2]."','".$_REQUEST['notes']."')";
				// db_query($sql);
				$db->exec($sql);
			}
	    }
	}

	function setClose($date, $type, $visitId){
		global $db;
		//檢查close_day是否有過期資料
		$date=explode("/",$date);
		// $sql="select * from space_close ";
		// $sql.="WHERE close_y='".$date[0]."' and close_m='".$date[1]."' and close_d='".$date[2]."' and visitId = '".$visitId."'";
		// $rs=db_query($sql);

		$r = getSpaceCloseRow($date, $visitId);
		// if(!db_eof($rs)){
			// $r=db_fetch_array($rs);
		if(!is_null($r)) {
			if($type=="remove"){
				$sql="delete from space_close where id=".$r['id'];
				// db_query($sql);
				$db->exec($sql);
			}
		}else{
			if($type=='add'){
				$sql="insert into space_close (visitId,close_y,close_m,close_d,notes) ";
				$sql.="values ('".$visitId."','".$date[0]."','".$date[1]."','".$date[2]."','".$_REQUEST['notes']."')";
				// db_query($sql);
				$db->exec($sql);
			}
		}
	}

	function getSpaceCloseRow($dateParts, $visitId) {
		global $db;
		$query = "Select * "
				."From space_close "
				."Where close_y = :closeY "
				."And close_m = :closeM "
				."And close_d = :closeD "
				."And visitId = :visitId ";
		$stmt = $db->prepare($query);
		$stmt->execute(array(
			'closeY' => $dateParts[0],
			'closeM' => $dateParts[1],
			'closeD' => $dateParts[2],
			'visitId' => $visitId
		));
		if($row = $stmt->fetch()) {
			return $row;
		}
		else {
			return null;
		}
	}
?>