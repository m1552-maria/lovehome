<?php
	include("init.php");
	$ID = $_REQUEST['ID'];
  	// $sql = "select * from $tableName where $editfnA[0]='$ID'";
	// $rs = db_query($sql, $conn);
	// $r = db_fetch_array($rs);
	$query = "select * "
			."from $tableName "
			."where ".$editfnA[0]." = :ID ";
	$stmt = $db->prepare($query);
	$stmt->execute(array(
		'ID' => $ID
	));
	$r = $stmt->fetch();
	$icons = array('doc'=>'icon_doc.gif','docx'=>'icon_doc.gif','pdf'=>'icon_pdf.gif','ppt'=>'icon_ppt.gif','pptx'=>'icon_ppt.gif',
	'txt'=>'icon_txt.gif','xls'=>'icon_xls.gif','xlsx'=>'icon_xls.gif','zip'=>'icon_zip.gif','rar'=>'icon_rar.gif');
	
	// $sql2="select * from space_detail where visitId='".$ID."'";
	// $sql2.=' order by visitId,week_day ';
	// echo $sql2."<br>";
	
	// $rs2 = db_query($sql2);
	$dayData=array();
	$timeStart=array();
	$timeEnd=array();
	// while($r2 = db_fetch_array($rs2)){
	$query = "Select * "
			."From space_detail "
			."Where visitId = :ID "
			."Order by visitId, week_day ";
	$stmt2 = $db->prepare($query);
	$stmt2->execute(array(
		'ID' => $ID
	));
	while($r2 = $stmt2->fetch()) {
		if(!isset($timeStart[$r2['week_day']]) || !is_array($timeStart[$r2['week_day']])){
			$timeStart[$r2['week_day']]=array();
		}
		if(!isset($timeEnd[$r2['week_day']]) || !is_array($timeEnd[$r2['week_day']])){
			$timeEnd[$r2['week_day']]=array();
		}
		array_push($timeStart[$r2['week_day']],$r2['startTime']);
		array_push($timeEnd[$r2['week_day']],$r2['endTime']);
	}
	foreach ($timeStart as $k=>$v){//記算每天有幾個時段
		$dayData[$k]=count($timeStart[$k]);
	}
	//print_r($timeStart);
	$dayData=json_encode($dayData);
	$timeStart=json_encode($timeStart);
	$timeEnd=json_encode($timeEnd);
?>

<Html>
<Head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="<?=$extfiles?>edit.css">
<link rel="stylesheet" href="/Scripts/jquery-ui-1.7.2.custom.css">
<style type="text/css">
		.ui-widget {font-size:12px; }
		.ui-datepicker-trigger {margin-left: 4px;	vertical-align: bottom;}
		#d_time{padding-top:6px;}
		.colLabel{	width:120px;}
		.title{	text-align:center;	background-color:#E1E1E1;}
		.sTable{margin:10px;}
		#d_msg{color:#C00;padding-top:3px;}

		table.grid{  margin:5px;	border: 1px solid #ccc; border-collapse: collapse; }
		table.grid td {   border: 1px solid #999;	padding:5px;	font-size:12px; text-align:center;}

		.divBorder2 { /* 內容區 第二層外框 */
			padding:8px; 
			border:1px solid #CCC;
			background:#FFFFFF;
			font-size:12px;
			width:372px;
		}
		.delBtn{	width:26px;	cursor:pointer;}
		.divTitle{
			color:#fff;
			background-color:#900;
			border: 0px;
			padding: 5px;
			margin: 0px;
			font-size:12px;
			width:95%;
		}
		#d_setting_time{	
			position:fixed;
			left:20%;
			/*margin-left:-200px;*/
			top:30%;
			width:400px;
			display:none;
		}
		#tb_setting_time{	width:98%;}
		.dBottom{	background:#dedede; text-align:center;padding:8px;border:1px solid #ccc;width:372px;}
		.gray{
			cursor:pointer;
			display:inline-block;
			text-decoration:none;
			font-size:12px;
			color:#fff;
			color:rgba(255,255,255,1);
			padding:0.4em 0.2em 0.2em 0.2em;
			
			border-style:solid;
			border-width:1px;
			border-radius:4px;
			box-shadow:0 1px 1px rgba(255,255,255,0.5) inset;
			
			background:#666;
			border-color:#999;
			border-color:rgba(0,0,0,0.1);
			color:#fff;
		}
		.nonTip{	color:#C00;}
		.tip{color:#c00;}
	</style>
<script type="text/javascript" src="/Scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="/Scripts/ui.datepicker.js"></script>  
<script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script> 
<title><?=$pageTitle?> - 編輯</title>
<script type="application/javascript">
	var dayData=<?=$dayData?>;		//記錄有哪幾天有開放時段
	var timeStart=<?=$timeStart?>;	//記錄開始時間
	var timeEnd=<?=$timeEnd?>;	
	var day;			//目前正在編輯哪一天
	$(function() {
		$('input[name="id"]').attr('required', true);
		$('input[name="sId"]').attr('required', true);
		$('input[name="title"]').attr('required', true);

		$('input[name="size"]').attr('pattern', '[0-9]+');

		$('input[name="peopleNum"]').attr('pattern', '[0-9]+');
		
		$('input[name="cost"]').attr('pattern', '[0-9]+');
		$('input[name="cost"]').attr('required', true);
		showList();
		var icons=[];
			icons['doc']='icon_doc.gif';
			icons['docx']='icon_doc.gif';
			icons['pdf']='icon_pdf.gif';
			icons['ppt']='icon_ppt.gif';
			icons['pptx']='icon_ppt.gif';
			icons['txt']='icon_txt.gif';
			icons['xls']='icon_xls.gif';
			icons['xlsx']='icon_xls.gif';
			icons['zip']='icon_zip.gif';
			icons['rar']='icon_rar.gif';
		$(".multiPhoto").change(function(){
			var that = $(this);
			var oTD = that.parents("td").eq(0);
			var fName;
			$(this.files).each(function(k,v){
				var fReader = new FileReader();
				fName=v['name'];
				var ary=fName.split(".");
				fReader.readAsDataURL(this);			
				fReader.onloadend = function(event){
					var oItem = oTD.find(".photoItem").eq(oTD.find(".photoItem").length-1);
					oItem.after(oItem.clone(true)).show();
					var img = oItem.find("img").get(0);
					img.style.padding='1';
					if(icons[ary[1]]){
						img.height=16;
						img.src='/images/'+icons[ary[1]];
					}else if(ary[1].toLowerCase()=="jpg" || ary[1].toLowerCase()=="png" || ary[1].toLowerCase()=="gif"){
						img.src=event.target.result;	
						img.height='80';
					}else{
						img.height=16;
						img.src='/images/icon_txt.gif';
					}
					var span=oItem.find("span").get(0);
					$(span).html(v['name']);
					oItem.get(0).input = that;
					var oNewFile = that.clone(true);
					oNewFile.val('');
					that.hide().after(oNewFile);
				}
			});
		});
		var delARR = new Array();
		$(".photoDel").click(function(){
			var oItem = $(this).parents(".photoItem");
			delARR.push($(this).prev("span").text());
			oItem.remove();
			$('input[name="NotInsertImg"]').val(delARR);
		});
	});

	function formvalid(tfm) { 
		var uperLimit = parseInt(tfm.uper_limit.value);
		if(uperLimit=='') {alert('請輸入研習人數上限'); tfm.uper_limit.focus();	return false;}
		if(tfm.start_booking.value=="") {alert('請輸入開放預約'); tfm.start_booking.focus();return false;}
		if(tfm.early_cancel.value=="") {alert('請輸入取消預約天數'); tfm.early_cancel.focus();return false;}
		if(isNaN(tfm.start_booking.value)) {alert('開放預約請輸入數字'); tfm.start_booking.focus();return false;}
		//::檢查是否有設定時段
		var objs=document.getElementsByName('week_day');
		var ck=false;
		var day;
		var dData=[];
		var tData=[];
		var str;
		var bTime=true;
		for(var i=0;i<objs.length;i++){
			if(objs[i].checked){	
				day=objs[i].value 
				ck=true;
				str=[];
				if(dayData[day]){
					for(var k in timeStart[day]){
						str.push(timeStart[day][k]+","+timeEnd[day][k]);
					}
					dData.push(day);
					tData.push(str.join(";"));
				}else{
					bTime=false;break;
				}
			}
		}
		if(!ck){	alert('請選擇每週開放日期');	return false;}	
		if(!bTime){	alert('開放時段未設定');return false;}
		$('#week_day_v').val(dData.join(","));
		$('#tData').val(tData.join("#"));
		return true;
	}
	function settingAll(day){
		var frist = '';
		$.each($("input[name='week_day']:checked"),function(k,v){
			if(day == 'on'){
				if($(v).val() == 0 || $(v).val() == 6) return;
				if(frist == '') frist = $(v).val();
				if(dayData[frist] == 0) addItem();
				dayData[$(v).val()] = dayData[frist];
				timeStart[$(v).val()] = timeStart[frist];
				timeEnd[$(v).val()] = timeEnd[frist];
			}
			if(day == 'off'){
				if($(v).val() == 0 || $(v).val() == 6){
					if(frist == '') frist = 6;
					dayData[$(v).val()] = dayData[frist];
					timeStart[$(v).val()] = timeStart[frist];
					timeEnd[$(v).val()] = timeEnd[frist];
				}
			}
		});
		showList();
	}

	function settingTime(d,obj){//設定時段
		if(timeStart[d]==undefined){ dayData[d]=undefined; }
		$('#d_mask').show();
		day=d;
		var word=['日','一','二','三','四','五','六'];
		var str="<table class='grid' id='tb_setting_time'>";
		str+="<tr><td colspan='3'><input type='button' value='新增時段' onclick='addItem()' style='float:left'/></td></tr>";
		str+="<tr><td >開放時間</td><td>結束時間</td><td>刪除</td></tr>";
		str+="</table>";
		$("#d_container").html(str);
		var start,end;
		if(dayData[day]==undefined){
			addItem();
		}else{//秀出已設好的時段
			for(var i in timeStart[day]){ 
				start=timeStart[day][i].split(":");
				end=timeEnd[day][i].split(":");
				newItem(i,start[0],start[1],end[0],end[1]);
			}
		}
		
		$('#d_setting_time').show();
		$('#dTitle').html("週"+word[d]);
	}
	function showList(){//讓使用者更好識別
		var objs=document.getElementsByName('week_day');	
		var str=[];
		var tStr;
		var d;
		var word=['日','一','二','三','四','五','六'];
		for(var i=0;i<objs.length;i++){
			if(objs[i].checked){
				tStr=[];
				d=objs[i].value;
				if(timeStart[d]!=undefined){	
					for(var k in timeStart[d]){
						if(timeStart[d][k].length == 4){
							timeStart[d][k] = "0"+timeStart[d][k];
						}
						tStr.push(timeStart[d][k]+" ~ "+timeEnd[d][k]);
					}
					tStr.sort();
				}else{
					tStr.push("<span class='nonTip'>時段尚未設定</span>");
				}
				str.push("週"+word[d]+"："+tStr.join(" , "));
			}
		}
		if(str.length==0){
			$('#td_list').html("<span class='nonTip'>尚未設定</span>");
		}else{
			$('#td_list').html(str.join("<br>"));
		}
	}
	
	function newItem(index,start_h,start_m,end_h,end_m){
		var tb=document.getElementById('tb_setting_time');
		var tbObj=tb.tBodies[0];
		var newRow=document.createElement('tr');
		var newcell1=document.createElement('td');
		var newcell2=document.createElement('td');
		var newcell3=document.createElement('td');
		newRow.style.height='30px';
		newRow.bgColor='#E3E3E3'; 
		var str="<select class='input' id='sel_start_h_"+index+"'/>"; 
		str+="<option selected></option>";
		var selected='';
		for(var j=1;j<=24;j++){
			if(start_h==j){	selected='selected';}else{selected='';}
			str+="<option "+selected+">"+j+"</option>";
		}
		str+="</select>&nbsp;時&nbsp;";
		str+="<select class='input' id='sel_start_m_"+index+"'/>"; 
		str+="<option selected></option>";
		
		var  k='';
		for(var j=0;j<=60;j+=10){
			if(start_m==j){	selected='selected';}else{selected='';}
			if(j==0){	k="00";}else{	k=j;}
			str+="<option "+selected+">"+k+"</option>";
		}
		str+="</select>&nbsp;分&nbsp;";
		newcell1.align='center';
		newcell1.innerHTML=str
		str="<select class='input' id='sel_end_h_"+index+"'/>"; 
		str+="<option selected></option>";
		for(var j=1;j<=24;j++){
			if(end_h==j){	selected='selected';}else{selected='';}
			str+="<option "+selected+">"+j+"</option>";
		}
		str+="</select>&nbsp;時&nbsp;";
		str+="<select class='input' id='sel_end_m_"+index+"'/>"; 
		str+="<option selected></option>";
		for(var j=0;j<60;j+=10){
			if(end_m==j){	selected='selected';}else{selected='';}
			if(j==0){	k="00";}else{	k=j;}
			str+="<option "+selected+">"+k+"</option>";
		}
		str+="</select>&nbsp;分&nbsp;";
		newcell2.align='center';
		newcell2.innerHTML=str;
		newcell3.align='center'; 
		newcell3.innerHTML='<img src="/images/del.png" class="delBtn" onClick="delItem(this,event)">'; 
		newRow.appendChild(newcell1);
		newRow.appendChild(newcell2);
		newRow.appendChild(newcell3);
		tbObj.appendChild(newRow);
	}
	function addItem(){//新增開放時段
		var index=0;
		if(dayData[day]){
			index=parseInt(dayData[day])+1;
			dayData[day]=index;
		}else{
			dayData[day]=index.toString();
		}
		newItem(index,"9","","12","");
	}
	function delItem(obj,e){//刪除開放時段
		var evt = e || window.event;
		var current = evt.target || evt.srcElement;
		var currRowIndex=current.parentNode.parentNode.rowIndex; 
		var tb = document.getElementById("tb_setting_time");
		if (tb.rows.length > 0) {  tb.deleteRow (currRowIndex);}
		if(timeStart[day]!=undefined) {timeStart[day].splice(currRowIndex, 1);}
		if(timeEnd[day]!=undefined) {timeEnd[day].splice(currRowIndex, 1);}
		if(dayData[day]>0){	
			dayData[day]=parseInt(dayData[day])-1;
		}else if(dayData[day]==0){	dayData[day]=undefined;}

	}
	function doCancel(){
		if(timeStart[day]==undefined){	dayData[day]=undefined;}
		$('#d_setting_time').hide();
	}
	function saveTime(){
		var bSave=true;
		if(dayData[day]!=undefined){	
			timeStart[day]=[];
			timeEnd[day]=[];
			var start_h,start_m,end_h,end_m;
		}
		for(var i=0;i<=dayData[day];i++){
			if($('#sel_start_h_'+i).val()==undefined)	continue;
			if($('#sel_start_h_'+i).val()==""){alert('請選擇幾點開放');	bSave=false;break;}
			if($('#sel_end_h_'+i).val()==""){alert('請選擇幾點結束');	bSave=false;break;}
			start_h=parseInt($('#sel_start_h_'+i).val());
			start_m=parseInt($('#sel_start_m_'+i).val());
			end_h=parseInt($('#sel_end_h_'+i).val());
			end_m=parseInt($('#sel_end_m_'+i).val());
			if(start_h>end_h){	alert('開始時間與結束時間有誤，請重新選擇');	bSave=false;break;}
			if(start_h==end_h && start_m>=end_m){	alert('開始時間與結束時間有誤，請重新選擇');bSave=false;	break;}
			timeStart[day][i]=$('#sel_start_h_'+i).val()+":"+$('#sel_start_m_'+i).val();
			timeEnd[day][i]=$('#sel_end_h_'+i).val()+":"+$('#sel_end_m_'+i).val();
		}
		if(bSave){	$('#d_setting_time').hide(); }
		showList();
	}
</script>
</Head>

<body class="page">
<form method="post" action="doedit.php" name="form1" enctype="multipart/form-data" onSubmit="return formvalid(this)">
	<input type="hidden" name="NotInsertImg" value="" />
<table align="center" class="sTable" width="96%" border="0" CellSpacing="0" CellPadding="4">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<?php for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
			case "text" : 
				if($editfnA[$i]=="early_cancel"){	echo "登錄日期後";}
				echo "<input type='text' name='".$editfnA[$i]."' id='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; 
				if($editfnA[$i]=="start_booking"){	echo "&nbsp;個月前，開放預約";}
				if($editfnA[$i]=="early_cancel"){	echo "&nbsp;天內，可以取消預約";}
				break;
			case "textbox" : echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
			case "textarea": echo "<textarea name='$editfnA[$i]' cols='$editflA[$i]' rows='$editfhA[$i]' class='ckeditor'>".$r[$editfnA[$i]]."</textarea>"; break;			
			case "checkbox": 
				if($editfnA[$i]=="week_day"){
					$day = explode(",",$r['week_day']);
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='1' class='input'"; if (in_array('1',$day)) $ck='checked'; else $ck=''; echo $ck.'>週一<input value="設定時段" type="button" class="gray" onClick="settingTime(1,this)" />&nbsp;';
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='2' class='input'"; if (in_array('2',$day)) $ck='checked'; else $ck=''; echo $ck.'>週二<input value="設定時段" type="button" class="gray" onClick="settingTime(2,this)" />&nbsp;';
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='3' class='input'"; if (in_array('3',$day)) $ck='checked'; else $ck=''; echo $ck.'>週三<input value="設定時段" type="button" class="gray" onClick="settingTime(3,this)" />&nbsp;';
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='4' class='input'"; if (in_array('4',$day)) $ck='checked'; else $ck=''; echo $ck.'>週四<input value="設定時段" type="button" class="gray" onClick="settingTime(4,this)" />&nbsp;';
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='5' class='input'"; if (in_array('5',$day)) $ck='checked'; else $ck=''; echo $ck.'>週五<input value="設定時段" type="button" class="gray" onClick="settingTime(5,this)" />';
					echo "<input type='button' value='平日時段套用' onClick='settingAll(\"on\")'><br><br>";
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='6' class='input'"; if (in_array('6',$day)) $ck='checked'; else $ck=''; echo $ck.'>週六<input value="設定時段" type="button" class="gray" onClick="settingTime(6,this)" />&nbsp;';
					echo "<input type='checkbox' name='".$editfnA[$i]."' onclick='showList()' value='0' class='input'"; if (in_array('0',$day)) $ck='checked'; else $ck=''; echo $ck.'>週日<input value="設定時段" type="button" class="gray" onClick="settingTime(0,this)" />&nbsp;';
					echo "<input type='button' value='假日時段套用' onClick='settingAll(\"off\")'>";
                }else{
					echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; 
				}
				break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $k=>$v) { $vv=$r[$editfnA[$i]]==$k?"<option selected value='$k'>$v</option>":"<option value='$k'>$v</option>"; echo $vv;} echo "</select>"; break;			
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : 
				echo "<input type='text' id='$editfnA[$i]' name='$editfnA[$i]' size='$editflA[$i]' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>";
				echo "<script type='text/javascript'>\$(function(){\$('#$editfnA[$i]').datepicker({showOn: 'button', buttonImage: '../images/calendar.jpg', buttonImageOnly: true});";
				echo "\$('#$editfnA[$i]').datepicker('option', 'duration', '');";
				echo "\$('#$editfnA[$i]').datepicker($.datepicker.regional['zh-TW']);});</script>";
			case "multiPhoto":
				echo "<input type='file' name='".$editfnA[$i]."[]' size='$editflA[$i]' class='input multiPhoto' multiple>"; 
				echo "<div class='photoPreview' >";
				if($r['pic']!=""){
					$pic=explode(",",$r['pic']);
					for($j=0;$j<count($pic);$j++){
						$finf = pathinfo($pic[$j]); 
						echo "<div class='photoItem' style='display:block;'>";
						echo "<input type='hidden' name='$pic[$j]' value='1' />";
						// echo "<input type='hidden' name='pic_".($pic[$j])."' value='1' />";
						$ext = $finf['extension']; 

						if(strtolower($ext)=="png" || strtolower($ext)=="jpg"){
							echo "<img src='".$ulpath."$ID/".($pic[$j])."' height='80' style='padding:1px'/>".$pic[$j]." ";
							// echo "<img src='".$ulpath."/".($pic[$j]).".".$finf['extension']."' height='80' style='padding:1px'/>".$pic[$j]." ";
							echo '<a href="/data/space'."/$ID/".($pic[$j]).'" target="_new">大圖</a>';
							// echo '<a href="/data/space/'.($pic[$j]).".".$finf['extension'].'" target="_new">大圖</a>';
						}else{
							$fn = $icons[$ext]?$icons[$ext]:'icon_default.gif'; 
							echo "<img src='/images/$fn' align='absmiddle'/>".$pic[$j]." ";
							echo '<a href="/data/space'."/$ID/".($pic[$j]).'" target="_new">下載</a>';
							// echo '<a href="/data/space/'.($pic[$j]).".".$finf['extension'].'" target="_new">下載</a>';
						}
						echo '<a class="photoDel" href="#" >刪除</a>';
						echo "</div>";
					}
				}
				echo "<div class='photoItem'><img /><span id='fName'></span><a class='photoDel' href='#' >刪除</a><div class='clearfx'></div></div>";
				echo "</div>";
				break;					
				var_dump($editfvA[$i]);
  	} ?></td>
  </tr><?php } ?>
	<tr>
	    <td class="colLabel" align="right" >目前開放</td>
	    <td id='td_list'><span class='nonTip'>尚未設定</span></td>
    </tr>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
		<input type="hidden" name="selid" value="<?=$ID?>">
		<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
		<input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
		<input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
		<input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
		<input type="submit" value="確定變更" class="btn">&nbsp;	
		<input type="reset" value="重新輸入" class="btn">&nbsp; 
		<input type="button" class="btn" onClick="history.back()" value="取消編輯">
		<input type="hidden" name='week_day_v' id='week_day_v'>
		<input type="hidden" name='tData' id='tData'>
	</td>
</tr>
</table>
</form>
<div id='d_setting_time'>
    <div id='dTitle' class='divTitle'></div>
    <div class='divBorder2' id='d_container'>
    </div>
    <div class='dBottom'><input type="button" value='確定' onclick='saveTime()'/>&nbsp;<input type="button" value='取消' onclick='doCancel()'/></div>
</div>
</body>
</html>
