<?php	
	//::for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');	
    include_once('../../connect_db.inc.php');	
	$bE = true;	//異動權限
	$pageTitle = "場地租借文件";
	$tableName = "rentPlaceFiles";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../download/';
	$delField = 'file';
	$delFlag = true;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "title";
	
	// $status = array(0=>'下架',1=>'上架');
	$status = array(2=>'下架',1=>'上架');	//modified by Hao-Tung 20220907,資料庫看起來這樣才對
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,status,file,rdate");
	$ftAry = explode(',',"編號,名稱,狀態,檔案,建立時間");
	$flAry = explode(',',",,,,,");
	$ftyAy = explode(',',"ID,text,select,flink,datetime");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,status,file");
	$newftA = explode(',',"名稱,狀態,檔案");
	$newflA = explode(',',",,,");
	$newfhA = explode(',',",,,");
	$newfvA = array('',array('1'=>'上架','下架'),'');
	$newetA = explode(',',"text,select,file");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,status,file");
	$editftA = explode(',',"編號,名稱,圖檔,連結,次序");
	$editflA = explode(',',",,,,");
	$editfhA = explode(',',",,,,");
	$editfvA = array('','',array('1'=>'上架','下架'),'');
	$editetA = explode(',',"text,text,select,file");
?>