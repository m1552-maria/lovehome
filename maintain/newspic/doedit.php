<?php
	include("init.php");
	
	//處理上傳的附件
	$delOld = false;	//可否刪除舊檔
	foreach($_FILES as $k=>$v) {
		$fn = $_FILES[$k]['name'];
		if (checkStrCode($fn,'utf-8')) $fn2=iconv("utf-8","big5",$fn); else $fn2=$fn;
		if ($_FILES[$k]['tmp_name'] != "") {
			if (!copy($_FILES[$k]['tmp_name'],"$ulpath$fn2")) { //上傳失敗
				echo '附件上傳作業失敗 !'; exit;
			} else $delOld=true;
		}
	}

	//刪除原先的檔案
	if($delOld) {
		$query = "select ".$delField." "
				."from ".$tableName." "
				."where ".$editfnA[0]." = :value ";
		$stmt = $db->prepare($query);
		$stmt->execute(array(
			'value' => $_REQUEST[$editfnA[0]]
		));
		if($r = $stmt->fetch()) {
		// $rs = db_query($sql,$conn);
		// if($r=db_fetch_array($rs)) {
			$fnn = $r[0];
			if(checkStrCode($fnn,'utf-8')) $fnn=iconv("utf-8","big5",$fnn); 
			$fnn = $ulpath.$fnn;
			if(file_exists($fnn)) @unlink($fnn);			
		}
	}
		
	$sql = "update $tableName set ";
  $fields = "";
	$n = count($editfnA);
  for($i=1; $i<$n; $i++) {
  	if ( ($fn=='') && ($editfnA[$i]==$delField)) continue;	//沒有上傳的檔案
  	switch($editetA[$i]) {
			case "hidden" :
			case "select" :		
  		case "text" :	$fldv = "'".addslashes($_REQUEST[$editfnA[$i]])."'"; break;
			case "textbox" :
  		case "textarea" :	$fldv = "'".$_REQUEST[$editfnA[$i]]."'"; break;
  		case "checkbox" : if(isset($_REQUEST[$editfnA[$i]])) $fldv=1; else $fldv=0; break;
			case "date" : $fldv = "'".trim($_REQUEST[$editfnA[$i]])."'"; break;
			case "file" : $fldv = "'$fn'"; break;
		}
		$fields = $fields.$editfnA[$i].'='.$fldv;
		if ($i<($n-1)) $fields = $fields.',';	
  }
	$sql = $sql.$fields." where $editfnA[0]='".$_REQUEST[$editfnA[0]]."'";
	//echo $sql; exit;
	// db_query($sql, $conn);
	// db_close($conn);	
	$db->exec($sql);
	header("Location: list.php?pages=".$_REQUEST['pages']."&keyword=".$_REQUEST['keyword']."&fieldname=".$_REQUEST['fieldname']."&sortDirection=".$_REQUEST['sortDirection']."&selid=".$_REQUEST['selid']);
?>
