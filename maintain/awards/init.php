<?php	
	//::for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');	
    include_once('../../connect_db.inc.php');	
	$bE = true;	//異動權限
	$pageTitle = "得獎紀錄";
	$tableName = "awards";
	$extfiles = '../';				//js及css檔的路徑位置
	/* for Upload files and Delete Record use
	$ulpath = '../../'.$stickers_Path;
	$delField = 'purl';
	$delFlag = true; */
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "content";
	
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,years,content");
	$ftAry = explode(',',"編號,年度,內文");
	$flAry = explode(',',"60,60,");
	$ftyAy = explode(',',"ID,text,text");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"years,content");
	$newftA = explode(',',"年度,內文");
	$newflA = explode(',',",80");
	$newfhA = explode(',',",5");
	$newfvA = array(date(Y),'');
	$newetA = explode(',',"text,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,years,content");
	$editftA = explode(',',"編號,年度,內文");
	$editflA = explode(',',",,80");
	$editfhA = explode(',',",,5");
	$editfvA = array('','','');
	$editetA = explode(',',"text,text,textbox");
?>