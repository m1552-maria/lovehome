<?php	
	//::for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<10) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include "../../connect_db.inc.php";
	include('../../miclib.php');	
	$bE = true;	//異動權限
	$pageTitle = "預約參訪報名表";
	$tableName = "visitform";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/visitform/';
	$delField = 'recept';
	$delFlag = false;
	$thumbW = 150;
	
	// Here for List.asp ======================================= //
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "title";
	
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,name,job,serveunit,tel,mobile,email,visittime,visitnum,rdate");
	$ftAry = explode(',',"編號,機關團體名稱,姓名,職務,服務單位,電話,手機,EMail,參訪時間,參訪人數,登錄日期");
	$flAry = explode(',',"60,,,,,,,,,60,");
	$ftyAy = explode(',',"ID,text,text,text,text,text,text,text,text,text,date");
	
	// Here for Append.asp =========================================================================== //
	$newfnA = explode(',',"title,purl,link,previge,target");
	$newftA = explode(',',"名稱,圖檔,連結,次序,目標");
	$newflA = explode(',',"60,60,60,,");
	$newfhA = explode(',',",,,,");
	$newfvA = array('','','','',$target);
	$newetA = explode(',',"text,file,text,text,select");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,purl,link,previge,target");
	$editftA = explode(',',"編號,名稱,圖檔,連結,次序,目標");
	$editflA = explode(',',",60,60,60,,");
	$editfhA = explode(',',",,,,,");
	$editfvA = array('','','','','',$target);
	$editetA = explode(',',"text,text,file,text,text,select");
?>