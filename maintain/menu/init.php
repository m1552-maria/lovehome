<?php
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<1) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
    include_once('../../connect_db.inc.php');
	
	$bE = true;	//異動權限	
	$pageTitle = "網站主選單維護";
	$tableName = "menus";
	$PID = 'parent';
	$extfiles = '../';
	
	// Here for List.asp ======================================= //
	if (isset($_REQUEST['filter'])) $_SESSION['productclass_pid']=$_REQUEST['filter']; 
	$filter = "parent='$_SESSION[productclass_pid]'";
	$defaultOrder = "CatNo";
	$searchField1 = "CatNo";
	$searchField2 = "cat_name";
	$pageSize = 10;	//每頁的清單數量	
	$channel = array(0=>'引入檔',1=>'直接連結');
	
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"CatNo,parent,cat_name,priority,url,channel,top_menu,status");
	$ftAry = explode(',',"代碼,父類別,名稱,次序,連結,連結方式,終端,啟用");
	$flAry = explode(',',"70,70,100,,,,50,50");
	$ftyAy = explode(',',"ID,text,text,text,text,select,checkbox,checkbox");
	
	// Here for Append.asp =========================================================================== //
	//::pre-con in append.php
	$newfnA = explode(',',"parent,CatNo,cat_name,priority,top_menu,status,channel,url");
	$newftA = explode(',',"父類別,類別代碼,類別名稱,次序,終端,上架,連結型式,連結網址");
	$newflA = explode(',',"20,20,80,,,,,80");
	$newfhA = explode(',',",,,5,,,,6");
	$newfvA = array($_SESSION[productclass_pid],'','','',0,1,$channel,'');
	$newetA = explode(',',"readonly,text,text,text,checkbox,checkbox,cklist,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"CatNo,parent,cat_name,priority,top_menu,status,channel,url");
	$editftA = explode(',',"類別代碼,父類別,類別名稱,次序,終端,上架,連結型式,連結網址");
	$editflA = explode(',',"20,20,80,70,,,,80");
	$editfhA = explode(',',",,,5,,,,6");
	$editfvA = array('','','','','','',$channel,'');
	$editetA = explode(',',"ID,text,text,text,checkbox,checkbox,cklist,text");
?>