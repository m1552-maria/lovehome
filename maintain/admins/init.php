<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<100) {
		header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "帳號設定";
	$tableName = "admins";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	if($_SESSION['privilege']<=100)	{
		$filter = "PRIVILEGE<=100";	
		$PRIVILEGE = array(1=>'基本帳號',100=>'捐物總管理');
	} else $PRIVILEGE = array(1=>'基本帳號', 11=>'參訪管理', 15=>'場地櫃檯', 16=> '場地管理員',255=>'系統總管理');
	$defaultOrder = "ACCOUNT";
	$searchField1 = "ACCOUNT";
	$searchField2 = "NAME";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ACCOUNT,NAME,R_DATE,UNIT,PRIVILEGE");
	$ftAry = explode(',',"帳號,名稱,登錄日期,所屬單位,等級");
	$flAry = explode(',',"60,,,,");
	$ftyAy = explode(',',"ID,text,date,text,select");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"ACCOUNT,NAME,UNIT,PWD,PRIVILEGE");
	$newftA = explode(',',"帳號,名稱,所屬單位,密碼,等級");
	$newflA = explode(',',",60,,,");
	$newfhA = explode(',',",,,,");
	$newfvA = array('','','','',$PRIVILEGE);
	$newetA = explode(',',"text,text,text,text,select");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ACCOUNT,NAME,UNIT,PWD,PRIVILEGE");
	$editftA = explode(',',"帳號,名稱,所屬單位,密碼,等級");
	$editflA = explode(',',",60,,,");
	$editfhA = explode(',',",,,,");
	$editfvA = array('','','','',$PRIVILEGE);	
	$editetA = explode(',',"text,text,text,text,select");
?>