<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<255) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');	
    include_once('../../connect_db.inc.php');	
	$bE = true;	//異動權限
	$pageTitle = "市府最新消息";
	$tableName = "contents";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/contents/';
	$delField = 'Ahead';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$filter = 'ClassID=1';
	$defaultOrder = "ID";
	$searchField1 = "ID";
	$searchField2 = "SimpleText";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"ID,SimpleText,R_Date,unValidDate,announcer");
	$ftAry = explode(',',"編號,內容,登錄日期,失效日期,公佈者");
	$flAry = explode(',',"50,,,,");
	$ftyAy = explode(',',"ID,text,date,date,text");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"SimpleText,Ahead,Content,unValidDate,announcer,ClassID");
	$newftA = explode(',',"內容,縮圖,全文,失效日期,公佈者,分類");
	$newflA = explode(',',"60,,60,0,0,0");
	$newfhA = explode(',',"6,,6,,,");
	$newfvA = array('','','','','','1');
	$newetA = explode(',',"textbox,file,textarea,date,text,hidden");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"ID,SimpleText,Ahead,Content,unValidDate,announcer");
	$editftA = explode(',',"編號,內容,縮圖,全文,失效日期,公佈者");
	$editflA = explode(',',",60,,60,");
	$editfhA = explode(',',",6,,6,,");
	$editetA = explode(',',"text,textbox,file,textarea,date,text");
?>