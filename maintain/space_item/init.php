<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<16) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');
    include_once('../../connect_db.inc.php');		
	$bE = true;	//異動權限
	$pageTitle = "場地借用物品/便當";
	$tableName = "space_item";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/space_item/';
	$delField = 'Ahead';
	$delFlag = true;
	$thumbW = 150;
		
	$gType = array("1"=>"器材","2"=>"便當");
	// Here for List.asp ======================================= //
	$filter = "id";
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,type");
	$ftAry = explode(',',"編號,名稱,分類");
	$flAry = explode(',',"5%,15%,10%");
	$ftyAy = explode(',',"ID,text,select");
	
	// Here for Append.asp =========================================================================== //
	// $dd = date('Y/m/d');
	$newfnA = explode(',',"title,type");
	$newftA = explode(',',"名稱,分類");
	$newflA = explode(',',"15,1");
	$newfhA = explode(',',"60,");
	$newfvA = array('',$gType);
	$newetA = explode(',',"text,select");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,type");
	$editftA = explode(',',"編號,名稱,分類");
	$editflA = explode(',',",15,1");
	$editfhA = explode(',',",60,");
	$editfvA = array('','',$gType);
	$editetA = explode(',',"text,text,select");
?>
