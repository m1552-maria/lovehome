<?php session_start(); 
	// include("../../config.php");
	include "../../connect_db.inc.php";
	include("init.php");
?>

<Html>
<Head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <link rel="stylesheet" href="edit.css">
 <script Language="JavaScript" src="<?=$extfiles?>edit.js"></script> 
 <title><?=$pageTitle?> - 編輯</title>
</Head>

<body class="page">
<?php //replymail
  if ($_REQUEST[reply]) {
		if ($_REQUEST[reply]=='回覆') { 
			$to = $_POST['email'];
			$subject = "$CompanyName 客服信件回覆";
			$msg = file_get_contents('../../email/contactm.html');
			$msg = str_replace('#name#',iconv("UTF-8","Big5",$_POST[title]),$msg);
			$msg = str_replace('#rdate#',iconv("UTF-8","Big5",$_POST[rdate]),$msg);
			$msg = str_replace('#mtype#',iconv("UTF-8","Big5",$_POST[mtype]),$msg);
			$msg = str_replace('#marks#',iconv("UTF-8","Big5",$_POST[marks]),$msg);
			$msg = str_replace('#now#',date('Y/m/d'),$msg);
			$msg = str_replace('#replaymsg#',iconv("UTF-8","Big5",$_POST[replaymsg]),$msg);
		} else {
			$id = $_POST[oid];
			$sql = "select b.email,b.title from orderform as a,vender as b where a.VenderID=b.vid and a.id='$id'";
			$stmt = $db->prepare($sql);
			$stmt->execute();
			$r = $stmt->fetch();
			$to = $r[0];
			$subject = "$CompanyName 客服信件轉寄";
			$msg = file_get_contents('../../email/contactv.html');
			$msg = str_replace('#name#',iconv("UTF-8","Big5",$r[1]),$msg);
			$msg = str_replace('#rdate#',iconv("UTF-8","Big5",$_POST[rdate]),$msg);
			$msg = str_replace('#title#',iconv("UTF-8","Big5",$_POST[title]),$msg);
			$msg = str_replace('#email#',$_POST[email],$msg);
			$msg = str_replace('#oid#',$_POST[oid],$msg);
			$msg = str_replace('#tel#',$_POST[tel],$msg);
			$msg = str_replace('#mtype#',iconv("UTF-8","Big5",$_POST[mtype]),$msg);
			$msg = str_replace('#marks#',iconv("UTF-8","Big5",$_POST[marks]),$msg);
			$msg = str_replace('#now#',date('Y/m/d'),$msg);
			$msg = str_replace('#replaymsg#',iconv("UTF-8","Big5",$_POST[replaymsg]),$msg);
		}
		send_mail($to, iconv("UTF-8","Big5",$subject), $msg); 
		echo "<p>信件已經寄出！</p>"; 
		echo "<a href='list.php'>返回清單</a>"; exit;
	}
	
	$ID = $_REQUEST['ID'];
	$sql = "select * from $tableName where $editfnA[0]='$ID'";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$r = $stmt->fetch();
?>
<form method="post" action="edit.php" name="form1" enctype="multipart/form-data">
<table align="center" class="sTable" width="640" border="0" CellSpacing="0" CellPadding="8">
	<tr><td colspan="2" class="rowHead"><font face="webdings">8</font><?=$pageTitle?> -【編輯作業】</td></tr>

	<tr>
 		<td width="100" align="right" class="colLabel"><?=$editftA[0]?></td>
 		<td width="80%" class="colContent"><input type="hidden" name="<?=$editfnA[0]?>" value="<?=$ID?>"><?=$ID?></td>
	</tr>

	<?php for ($i=1; $i<count($editfnA); $i++) { ?><tr class="<?= $editetA[$i]=='hidden'?'colLabel_Hidden':''?>">
  	<td width="100" align="right" class="colLabel"><?=$editftA[$i]?></td>
  	<td><?php switch($editetA[$i]) { 
			case "view" : echo "<input type='hidden' name='".$editfnA[$i]."' value='".($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i])."'>".$r[$editfnA[$i]]; break;
  	  		case "text" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' class='input' value='".$r[$editfnA[$i]]."'>"; break;
			case "textarea": echo "<textarea name='".$editfnA[$i]."' cols='".$editflA[$i]."' rows='16' class='input'>".$r[$editfnA[$i]]."</textarea>"; break;
  			case "checkbox": echo "<input type='checkbox' name='".$editfnA[$i]."' class='input'"; if ($r[$editfnA[$i]]) echo " checked"; echo " value='true'>"; break;
			case "file"    : echo $r[$editfnA[$i]]."<br> <input type='file' name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; break;
			case "select"  : echo "<select name='$editfnA[$i]' size='$editflA[$i]' class='input'>"; foreach($editfvA[$i] as $v) { $vv=$r[$editfnA[$i]]==$v?"<option selected>$v</option>":"<option>$v</option>"; echo $vv;} echo "</select>"; break;				 
			case "hidden"  : echo "<input type='hidden' name='".$editfnA[$i]."' value='". ($editfvA[$i]==''?$r[$editfnA[$i]]:$editfvA[$i]) ."'>"; break;
			case "date" : echo "<input type='text' name='".$editfnA[$i]."' size='".$editflA[$i]."' value='".date('Y/m/d',strtotime($r[$editfnA[$i]]))."' class='input'>"."<input type='button' name='b1' value='▼' class='btn' onclick='OpenWin(form1.".$editfnA[$i].");'>";
  	} ?></td>
  </tr><?php } ?>
<tr>
	<td colspan="2" align="center" class="rowSubmit">
  	<input type="hidden" name="selid" value="<?=$ID?>">
  	<input type="hidden" name="pages" value="<?=$_REQUEST['pages']?>">
    <input type="hidden" name="keyword" value="<?=$_REQUEST['keyword']?>">
    <input type="hidden" name="fieldname" value="<?=$_REQUEST['fieldname']?>">
    <input type="hidden" name="sortDirection" value="<?=$_REQUEST['sortDirection']?>">
    <button class="btn" onClick="history.back()">返回</button>
  </td>
</tr>
</table>
</form>
</body>
</html>
