<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<255) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	$bE = true;	//異動權限
	// 另外，會使用 config.php 的全域變數 $VenderID
	include('../../miclib.php');	
	$pageTitle = "連絡我們";
	$tableName = "contact";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../FLV/';
	$delField = 'path';
	$delFlag = false;
	
	// Here for List.asp ======================================= //
	//$filter = 'account';
	$defaultOrder = "id";
	$searchField1 = "id";
	$searchField2 = "title";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,name,subject,email,mobile,rdate");
	$ftAry = explode(',',"編碼,名稱,主題,Email,連絡電話,登錄時間");
	$flAry = explode(',',"80,,,,,120");
	$ftyAy = explode(',',"ID,text,text,text,text,datetime");
	
	// Here for Append.asp =========================================================================== //
	$dd = date('Y/m/d');
	$newfnA = explode(',',"ACCOUNT,NAME,PRIVILEGE,PWD");
	$newftA = explode(',',"帳號,名稱,權限,密碼");
	$newflA = explode(',',",,,");
	//$newfvA = array('','',,,);
	$newetA = explode(',',"text,text,text,text");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,name,subject,email,mobile,content,rdate");
	$editftA = explode(',',"編碼,名稱,主題,Email,連絡電話,內容,登錄時間");
	$editflA = explode(',',",60,60,60,60,60,60,");
	$editetA = explode(',',"view,view,view,view,view,view,view");
?>