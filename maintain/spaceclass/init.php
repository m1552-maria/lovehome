<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<16) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');	
    include_once('../../connect_db.inc.php');
	$bE = true;	//異動權限
	$pageTitle = "場地樓層";
	$tableName = "spaceclass";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/spaceclass/';
	$delField = 'Ahead';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$filter = "id";
	$defaultOrder = "id";
	$searchField1 = "title";
	$searchField2 = "";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,title,floor,isReady");
	$ftAry = explode(',',"編號,場地樓層名稱,樓層號碼,是否啟用");
	$flAry = explode(',',"5%,20%,5%,5%");
	$ftyAy = explode(',',"ID,text,text,checkbox");
	
	// Here for Append.asp =========================================================================== //
	// $dd = date('Y/m/d');
	$newfnA = explode(',',"title,floor,isReady");
	$newftA = explode(',',"場地樓層名稱,樓層號碼,是否啟用");
	$newflA = explode(',',"15,,1");
	$newfhA = explode(',',"60,,");
	$newfvA = array('','',true);
	$newetA = explode(',',"text,text,checkbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,title,floor,isReady");
	$editftA = explode(',',"編號,場地樓層名稱,樓層號碼,是否啟用");
	$editflA = explode(',',",,15,,1");
	$editfhA = explode(',',",6,,");
	$editetA = explode(',',"text,text,text,checkbox");
?>
