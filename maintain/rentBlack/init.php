<?php	
	// for Access Permission Control
	session_start();
	if ($_SESSION['privilege']<16) {
	  header('Content-type: text/html; charset=utf-8');
		echo "<script language='javascript'>alert('您沒有權限!');location.href='../Login.php';</script>";
		exit; 	
	}
	// include("../../config.php");
	include('../../miclib.php');	
    include_once('../../connect_db.inc.php');	
	$bE = true;	//異動權限
	$pageTitle = "場地借用黑名單";
	$tableName = "rentBlack";
	$extfiles = '../';				//js及css檔的路徑位置
	// for Upload files and Delete Record use
	$ulpath = '../../data/rentBlack/';
	$delField = 'Ahead';
	$delFlag = true;
	$thumbW = 150;
		
	// Here for List.asp ======================================= //
	$filter = "id";
	$defaultOrder = "id";
	$searchField1 = "applier";
	$searchField2 = "leadman";
	$pageSize = 20;	//每頁的清單數量
	// 注意 primary key shpuld be first one
	$fnAry = explode(',',"id,applier,leadman,mobile,email,note");
	$ftAry = explode(',',"編號,借用單位,負責人,電話,email,原因說明");
	$flAry = explode(',',"5%,20%,10%,10%,10%,25%");
	$ftyAy = explode(',',"ID,text,text,text,text,text");
	
	// Here for Append.asp =========================================================================== //
	// $dd = date('Y/m/d');
	$newfnA = explode(',',"applier,leadman,mobile,email,note");
	$newftA = explode(',',"借用單位,負責人,電話,email,原因說明");
	$newflA = explode(',',"50,50,50,50,150");
	$newfhA = explode(',',"60,,,,5");
	$newfvA = array('','','','','');
	$newetA = explode(',',"text,text,text,text,textbox");
	
	// Here for Edit.asp ============================================================================= //
	$editfnA = explode(',',"id,applier,leadman,mobile,email,note");
	$editftA = explode(',',"編號,借用單位,負責人,電話,email,原因說明");
	$editflA = explode(',',",50,50,50,50,150");
	$editfhA = explode(',',",6,,,,5");
	$editetA = explode(',',"text,text,text,text,text,textbox");
?>
