<?php
	include 'config.php';
    $act = $_GET['act'];

    $sql = "SELECT s.*, c.title as floorTitle FROM space s JOIN spaceclass c ON(s.sId = c.id) WHERE userGroup = '0'";
    $rs = db_query($sql,$conn);
    $rentData = array();
    while($r=db_fetch_array($rs)){
        $rentData[$r['id']] = $r['floorTitle'].' - '.$r['title'];
    }

    $sql = "SELECT * FROM space_item";
    $rs = db_query($sql,$conn);
    $itemData = array();
    while($r=db_fetch_array($rs)){
        $itemData[$r['type']][] = $r['title'];
    }

    //::各單位開放時間
    $sql="select * from space_detail ";
    $sql.="where visitId='".$_REQUEST['spaceID']."' ";
    $sql.="order by visitId,week_day ";
    //echo $sql."<br><br>";
    $rs=db_query($sql);
    $timeData=array();
    $bSettingTime=false;
    if($rs){
        $bSettingTime=true;
        while($r=db_fetch_array($rs)){  
            if(!is_array($timeData['id'])){ $timeData['id']=array();}
            if(!is_array($timeData['visitId'])){    $timeData['visitId']=array();}
            if(!is_array($timeData['startTime'])){  $timeData['startTime']=array();}
            if(!is_array($timeData['endTime'])){    $timeData['endTime']=array();}
            if(!is_array($timeData['week_day'])){   $timeData['week_day']=array();}
            if(!is_array($timeData['notice_day'])){ $timeData['notice_day']=array();}
            array_push($timeData['id'],$r['id']);
            array_push($timeData['visitId'],$r['visitId']);
            array_push($timeData['startTime'],$r['startTime']);
            array_push($timeData['endTime'],$r['endTime']);
            array_push($timeData['week_day'],$r['week_day']);
        }
    }
    //::統計目前預約記錄，計算餘額
    $bookingData=array();
    
    if($bookRS){
        while($r=db_fetch_array($bookRS)){
            $full=explode(" ",$r['bDate']);
            $vd=explode("-",$full[0]);
            $vt=explode(":",$full[1]);
            $wd=date("w",mktime(0,0,0,$vd[1],$vd[2],$vd[0])); //參訪當天是星期幾 
            $index=0;
            foreach($timeData as $k=>$v ){//判斷預約時間落在什麼時段
                foreach($v as $k1=>$v1){
                    if($k=="week_day" && $wd==$v1){
                        $tWeekDay=$v1;
                        $tId=$timeData['id'][$k1];
                        $tSt=explode(":",$timeData['startTime'][$k1]);
                        $tEt=explode(":",$timeData['endTime'][$k1]);
                        $sH=intval($tSt[0]);    $sM=intval($tSt[1]);    $eH=intval($tEt[0]);    $eM=intval($tEt[1]);
                        if(!is_array($bookingData[$vd[0]])){    $bookingData[$vd[0]]=array();   }
                        if(!is_array($bookingData[$vd[0]][$vd[1]])){    $bookingData[$vd[0]][$vd[1]]=array();   }
                        if(!is_array($bookingData[$vd[0]][$vd[1]][$vd[2]])){    $bookingData[$vd[0]][$vd[1]][$vd[2]]=array();   }
                        $bool=false;
                        if(intval($vt[0])>$sH && intval($vt[0]<$eH)){
                            $bool=true;
                        }else if((intval($vt[0])==$sH && intval($vt[1])>=$sM) || (intval($vt[0])==$eH && intval($vt[1])<=$eM)){
                            $bool=true;
                        }
                        if($bool){
                            if(!is_array($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId])){  
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]=array();
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count']=0;
                            }
                            if(!$bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['st']){  $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['st']=$timeData['startTime'][$k1];}
                            if(!$bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['et']){  $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['et']=$timeData['endTime'][$k1];}
                            $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count']=intval($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count'])+1;
                        }
                        
                    }
                }
            }
        }
    }

    if($act== 'list'){
        $sql="select a.title, c.title as floorTitle, a.early_cancel, b.bDate, b.eDate, b.applier, b.peoples, b.rdate, b.id, a.id as spaces, b.options ";
        $sql.="from `space` as a JOIN rentplace as b ON(a.id = b.places) JOIN spaceclass c ON(a.sId = c.id) ";
        $sql.="where b.id in (".$_REQUEST[id].") ";
    }else if($act== 'edit'){
        $sql = "select * from rentplace where id = '".$_REQUEST[id]."'";
    }
    $rsList = db_query($sql);
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/home2.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="Keywords" content="捐款,愛心,公益,社福,基金會,瑪利亞,身心障礙,天使,社會責任,抵稅,生命教育,社會宣導,瑪利亞社會福利基金會,智能障礙,自閉症,唐氏症,腦性麻痺,多重障礙,早期療育,麵包,輔具,清潔">
<!-- InstanceBeginEditable name="doctitle" -->
<title>臺中市愛心家園</title>
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<style>
    #invinfo { display:none; }
    .grid {
        width: 100%;
    }
    table.grid td {   
        border: 1px solid #ccc;
        text-align:center;
        font-family:Verdana, Geneva, sans-serif;
        font-size:12px;
        width:120px;
        padding-top:8px;
        padding-bottom:8px;
    }
    .tdTitle{   background:#a91d3b; color:#fff;padding:8px;}
    #tdWeek{    background:#d59416; color:#fff;padding:8px;}
    #sel_unit{  float:left;color: black;}
    .tip{   color:#C00; font-weight:bold; font-size:14px;}
    .tip2{  color:#333;}
    .tip3{  color:#C00;  font-weight:bold;}
    .tip4{   font-weight:bold;}
    .tip5{  color:#F30;text-decoration: underline; font-weight:bold;}
    .tdNone{    background:#f0f0f0;color:#999;}
    .tdNone2{   background:#ffffff;color:#999;}

    .btn2 {
      width:60px;
      background: #808080;
      padding: 3px 6px 3px 6px;
      float:right;
    }
    .btn2:hover {
      background: #999;
      text-decoration: none;
    }
</style>
<script type="text/javascript" src="Scripts/jquery.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script>
    function delBooking(id){
        if(!confirm("確定取消預約嗎?")) return; 
        $.ajax({ 
            type: "POST", 
            url: "rent_lib.php", 
            data: "act=delBooking&id="+id
        }).done(function( rs ) {
            alert('取消成功');
            location.reload();
        }).fail(function( msg ) { 
        });
    }

    function editBooking(id, spaces) {
        location.href="Booking.php?act=edit&spaceID="+spaces+"&id="+id;
    }
</script>
<script>
    $(function(){
        var timeData=<?=json_encode($timeData);?>;
        var visit_id='<?=$visitId?>';
        var tInfo='';
        var pre_weekDay='';
        var word=['日','一','二','三','四','五','六'];
        var wDay=0;
        for(var i=0;i<timeData['startTime'].length;i++){
            wDay=timeData['week_day'][i];
            if(pre_weekDay==""){
                pre_weekDay=wDay
                tInfo+="週"+word[wDay]+"：";
            }else if(pre_weekDay!=wDay){
                tInfo+="<br>";
                pre_weekDay=wDay; 
                tInfo+="週"+word[wDay]+"：";
            }else{
                tInfo+=" , ";
            }
            tInfo+=timeData['startTime'][i]+" ~ "+timeData['endTime'][i]
        }
        $('#td_time').html(tInfo);
    })
    function checkitem() {
        var pass = $('#check1').attr('checked') 
                && $('#check2').attr('checked')
                && $('#check3').attr('checked')
                && $('#check4').attr('checked')
                && $('#check5').attr('checked')
                && $('#check6').attr('checked')
                && $('#check7').attr('checked')
                && $('#check8').attr('checked')
        $('#Submit')[0].disabled = !pass;
    }

    function formvalid(tfm) {
        if(!tfm.applier.value) {alert('請填寫 申請單位'); tfm.applier.focus(); return false; }
        if(!tfm.leadman.value) {alert('請填寫 負責人'); tfm.leadman.focus(); return false; }
        if(!tfm.contactman.value) {alert('請填寫 聯絡人'); tfm.contactman.focus(); return false; }
        if(!tfm.tel.value) {alert('請填寫 聯絡電話'); tfm.tel.focus(); return false; }
        if(!tfm.email.value) {alert('請填寫 EMail'); tfm.email.focus(); return false; }
        if(!tfm.title.value) {alert('請填寫 活動名稱'); tfm.title.focus(); return false; }
        if(!tfm.peoples.value) {alert('請填寫 預估參訪人數'); tfm.peoples.focus(); return false; }
        var v = parseInt(tfm.peoples.value); 
        if(!v) { alert('參訪人數只能為數值'); return false; }
        
        for(var i=0; i<form1.isInv.length; i++) {
            var ck = (form1.isInv[i].checked);
            if(ck) {
                var v = form1.isInv[i].value;
                if(v==3) {
                    if (!$("#invoiceTitle").val()) {alert('請填寫 發票抬頭'); $("#invoiceTitle").focus(); return false;};
                    if (!$("#invoiceNo").val()) {alert('請填寫 發票編號'); $("#invoiceNo").focus(); return false;};
                }
            }
        }
        
        return true;
    }

    function checkInv() {
        for(var i=0; i<form1.isInv.length; i++) {
            var ck = (form1.isInv[i].checked);
            if(ck) {
                var v = form1.isInv[i].value;
                if(v==3) $("#invinfo").css("display","block"); else {
                    $("#invinfo").css("display","none");
                    $("#invoiceTitle").val('');
                    $("#invoiceNo").val('');
                }
            }
        }
    }
</script>
<link href="css/index.css" rel="stylesheet" type="text/css">
</head>
<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td bgcolor="#fdf1f1">&nbsp;</td>
            <td width="1020" height="120" align="center" valign="middle" bgcolor="#fdf1f1"><a href="/"><img src="images/logo2.jpg" width="328" height="94" border="0"></a></td>
            <td bgcolor="#FDF1F1">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#e16c7c">&nbsp;</td>
            <td width="1020" height="40" valign="middle" bgcolor="#E16C7C"><?php include('menu.php') ?></td>
            <td bgcolor="#E16C7C">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#f4c5bf">&nbsp;</td>
            <td width="1020">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="200" valign="top" bgcolor="#efefef">
                            <?php include('stickers.php') ?>
                        </td>
                        <td id="maintx" valign="top">
                            <table width="100%" border="0" align="center" class="mainTable">
                                <?php if($act== 'list'){ ?>
                                    <tr>
                                        <td height="80" colspan='5'>
                                            <div class="pageTitleBG">
                                                <img src="images/off.png" align="absbottom" />場地預約取消申請
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="92%" border="1" style="border:3px #cccccc solid;" align="center">
                                            <tr>
                                                <td colspan='5' style="text-align: left">
                                                    <span class='tip'> * </span>
                                                    <span class='tip2'>超過可取消的時間點，將無法取消預約</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">借用場地</td>
                                                <td width="15%">借用時間</td>
                                                <td width="10%">申請單位</td>
                                                <td width="13%">申請時間</td>
                                                <td width="10%">功能</td>
                                            </tr>
                                            <?php
                                                if(!db_eof($rsList)){
                                                    while($r = db_fetch_array($rsList)){
                                                        $early = $r['early_cancel']*24*60*60;
                                                        $t = strtotime($r['bDate']);
                                                        if(time()>($t-$early)){ 
                                                            $class='btnDisabled'; 
                                                            $disabled="disabled"; 
                                                            $val='無法取消';
                                                        }else{  
                                                            $class='btn3';
                                                            $disabled='';
                                                            $val='取消預約';
                                                        }
                                            ?>
                                                <tr>
                                                    <td><?=$r['floorTitle'].' - '.$r['title']?></td>
                                                    <td><?=$r['bDate']?>起<?=$r['eDate']?>迄</td>
                                                    <td><?=$r['applier']?></td>
                                                    <td><?=$r['rdate']?></td>
                                                    <td align="center">
                                                        <input type="button" value="修改" class='<?=$class?>' <?=$disabled?> onclick='editBooking("<?=$r[id]?>", "<?=$r['spaces']?>")'/>
                                                        <input type="button" value="<?=$val?>" class='<?=$class?>' <?=$disabled?> onclick='delBooking("<?=$r[id]?>")'/>
                                                    </td>
                                                </tr>
                                            <?php } } ?>
                                            </table>
                                        </td>
                                    </tr>
                                <?php }else if($act== 'edit'){ ?>
                                    <tr>
                                        <td height="80">
                                            <div class="pageTitleBG">
                                                <img src="images/off.png" align="absbottom" />場地預約修改
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="92%" border="1" style="border:3px #cccccc solid;" align="center">
                                                <tr>
                                                <td>
                                                    <?php
                                                    if(!db_eof($rsList)){
                                                        while($r = db_fetch_array($rsList)){
                                                            $optionArr = split('，', $r['options']);
                                                            $early = $r['early_cancel']*24*60*60;
                                                            $t = strtotime($r['bDate']);
                                                            if(time()>($t-$early)){ 
                                                                $class='btnDisabled'; 
                                                                $disabled="disabled"; 
                                                                $val='無法取消';
                                                            }else{  
                                                                $class='btn3';
                                                                $disabled='';
                                                                $val='取消預約';
                                                            }
                                                    ?>
                                                    <form id="form1" action="rentplacedo.php" method="post" style="padding:0" onsubmit="return formvalid(this)">
                                                        <table width="100%"  border="0" cellpadding="4" cellspacing="0" style="border:dashed; border-width:1px; padding:10px">
                                                            <tr hidden>
                                                                <td>
                                                                    <input type="hidden" name="places" value="<?=$r['places']?>">
                                                                    <input type="hidden" name="act" value="<?=$_GET['act']?>">
                                                                    <input type="hidden" name="id" value="<?=$_GET['id']?>">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="120" align="right">申請單位：</td>
                                                                <td colspan="3"><input name="applier" type="text" id="applier" size="60" value="<?=$r['applier']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">負責人：</td>
                                                                <td><input type="text" name="leadman" id="leadman" value="<?=$r['leadman']?>"/></td>
                                                                <td align="right">聯絡人：</td>
                                                                <td><input type="text" name="contactman" id="contactman" value="<?=$r['contactman']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">聯絡電話：</td>
                                                                <td><input type="text" name="tel" id="tel" value="<?=$r['tel']?>"/></td>
                                                                <td align="right">行動電話：</td>
                                                                <td><input type="text" name="mobile" id="mobile" value="<?=$r['mobile']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">傳真：</td>
                                                                <td colspan="3"><input type="text" name="fax" id="fax" value="<?=$r['fax']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">地址：</td>
                                                                <td colspan="3"><input name="address" type="text" id="address" size="60" value="<?=$r['address']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">電子郵件：</td>
                                                                <td colspan="3"><input name="email" type="text" id="email" size="60" value="<?=$r['email']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">活動名稱：</td>
                                                                <td colspan="3"><input name="title" type="text" id="title" size="60" value="<?=$r['title']?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">預估參加人數：</td>
                                                                <td colspan="3"><input type="text" name="peoples" id="peoples" size="10" value="<?=$r['peoples']?>"/> &nbsp;<span style="font-size:12px; color:#C30">(只能輸入阿拉伯數字,勿寫國字)</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" valign="top">借用場地：</td>
                                                                <td><?=$rentData[$r['places']]?></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class='title'>開放時間：</td>
                                                                <td id='td_time'></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">租用日期/時間：</td>
                                                                <td colspan="3">
                                                                    <?=date("Y",strtotime($r['bDate']))?> 年 <?=date("m",strtotime($r['bDate']))?> 月 <?=date("d",strtotime($r['bDate']))?> 日 
                                                                    <input type="hidden" name="date" value="<?=date("Y",strtotime($r['bDate'])).'/'.date("m",strtotime($r['bDate'])).'/'.date("d",strtotime($r['bDate']))?>">
                                                                    從<input type="time" name="bDate" value="<?=date("h:m:s",strtotime($r['bDate']))?>">
                                                                    至<input type="time" name="eDate" value="<?=date("h:m:s",strtotime($r['eDate']))?>">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">備註：</td>
                                                                <td colspan="3" style="color: #C30">
                                                                    時間中午12點請選擇下午12:00。<BR>
                                                                    全館禁止於各處牆面張貼資料，違者將按處酌收清潔費。
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">額外借用器材：($300/按項另計)</td>
                                                                <td>
                                                                    <?php foreach ($itemData['1'] as $key => $value) {
                                                                        $checkStr = '';
                                                                        if(in_array($value, $optionArr)) $checkStr = "checked";
                                                                        echo '<input name="options[]" type="checkbox" id="options[]" value="'.$value.'" '.$checkStr.'>'.$value;
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">訂購本園：</td>
                                                                <td>
                                                                    <?php foreach ($itemData['2'] as $key => $value) {
                                                                        $checkStr = '';
                                                                        if(in_array($value, $optionArr)) $checkStr = "checked";
                                                                        echo '<input name="options[]" type="checkbox" id="options[]" value="'.$value.'" '.$checkStr.'>'.$value;
                                                                    } ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" valign="top">發票：</td>
                                                                <td colspan="3">
                                                                    <input name="isInv" type="radio" id="isInv" value="2" checked onChange="checkInv()">二聯式發票
                                                                    <input type="radio" name="isInv" id="isInv" value="3" onChange="checkInv()">三聯式發票
                                                                    <div id="invinfo">
                                                                        抬頭：<input type="text" name="invoiceTitle" id="invoiceTitle"> 
                                                                        統一編號：<input type="text" name="invoiceNo" id="invoiceNo">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <input type="checkbox" name="check1" id="check1" onClick="checkitem()"> 申請單位已詳閱「<a href="index.php?incfn=inc_RentPlace.php" target="_blank">場地租借申請流程</a>」 <br>
                                                                    <input type="checkbox" name="check2" id="check2" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場地租借辦法－105.5.26修-1.doc" target="_blank">場地租借辦法</a>」 <br>
                                                                    <input type="checkbox" name="check3" id="check3" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場地租借共同遵守約定_107.01.01修.doc" target="_blank">場地租借共同遵守約定</a>」 <br>
                                                                    <input type="checkbox" name="check4" id="check4" onClick="checkitem()"> 申請單位已詳閱「<a href="download/專業空間使用要點.doc" target="_blank">專業空間借用要點</a>」<br>
                                                                    <input type="checkbox" name="check5" id="check5" onClick="checkitem()"> 申請單位已詳閱「<a href="download/台中市愛心家園停車場管理規定-107.01.01修.docx" target="_blank">愛心家園停車須知</a>」 <br>
                                                                    <input type="checkbox" name="check6" id="check6" onClick="checkitem()"> 申請單位已詳閱「<a href="download/櫃台提前或延後開關大門申請單_107.01.01修.docx" target="_blank">提前或延後開關大門申請單</a>」 <br>
                                                                    <input type="checkbox" name="check7" id="check7" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場租聯絡人.jpg" target="_blank">場地租借聯絡人資訊</a>」 <br>
                                                                    <input type="checkbox" name="check8" id="check8" onClick="checkitem()"> 申請單位已詳閱「<a href="download/水療池租借同意書108.6.21修.docx" target="_blank">水療池租借同意書</a>」 <br>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <input name="Submit" type="submit" disabled class="btnMedium" id="Submit" value="送出" />
                                                                    <input name="button" type="reset" class="btnMedium" id="button" value="重設" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                    <?php } } ?>
                                                </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td bgcolor="#F4C5BF">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ed909a">&nbsp;</td>
            <td width="1020" bgcolor="#ED909A"><?php include 'inc_foot.htm' ?></td>
            <td bgcolor="#ED909A">&nbsp;</td>
        </tr>
    </table>
</body>
<!-- InstanceEnd -->
</html>