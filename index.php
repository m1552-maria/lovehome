<?php
  session_start();
	include 'config.inc.php';
  include_once('connect_db.inc.php');
	$incfn = (isset($_REQUEST['incfn']))?$_REQUEST['incfn']:'inc_main.php';
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/home2.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="Keywords" content="捐款,愛心,公益,社福,基金會,瑪利亞,身心障礙,天使,社會責任,抵稅,生命教育,社會宣導,瑪利亞社會福利基金會,智能障礙,自閉症,唐氏症,腦性麻痺,多重障礙,早期療育,麵包,輔具,清潔">
<!-- InstanceBeginEditable name="doctitle" -->
<title>臺中市愛心家園</title>
<script type="text/javascript" src="Scripts/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/jquery.corner.js"></script>
<!-- InstanceEndEditable -->
<link href="css/index.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style>
    .gradient_bg {
        background: linear-gradient(180deg, rgba(196,154,149,1) 0%, rgba(244,197,191,1) 29%);
    }
</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#fdf1f1" class="gradient_bg">&nbsp;</td>
    <td width="1020" height="120" align="center" valign="middle" bgcolor="#fdf1f1" class="gradient_bg">
      <a href="/"><img src="images/logo2.jpg" width="328" height="94" border="0"></a>
    </td>
    <td bgcolor="#FDF1F1" class="gradient_bg">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#e16c7c">&nbsp;</td>
    <td width="1020" height="40" valign="middle" bgcolor="#E16C7C"><?php include('menu.php') ?></td>
    <td bgcolor="#E16C7C">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#f4c5bf">&nbsp;</td>
    <td width="1020">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr><td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="200" valign="top" bgcolor="#efefef"><?php include('stickers.php') ?></td>
          <td id="maintx" valign="top"><!-- InstanceBeginEditable name="content" -->
          <?php 
            if(strlen($incfn) > 0) {
              if(!file_exists(getcwd().'/'.$incfn)) {
                  // header("HTTP/1.0 404 Not Found");
                  echo "Page Not Found.\n";
                  die();
              }
              if(strpos($incfn, '=') !== false) {
                  // header("HTTP/1.0 404 Not Found");
                  echo "Page Not Found.\n";
                  die();
              }
              $fileInfo = pathInfo(getcwd().'/'.$incfn);
              $ext = $fileInfo['extension'];
              if(strcmp($ext, 'php') === 0) {
                  include(getcwd().'/'.$incfn);
              }
              else {
                  $lines = file($incfn);
                  $pageContent = implode("\n", $lines);
                  echo $pageContent;
              }
              // include($incfn); 
            }
          
          
          ?><!-- InstanceEndEditable --></td>
        </tr>
      </table></td></tr>      
    </table>
    </td>
    <td bgcolor="#F4C5BF">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#ed909a">&nbsp;</td>
    <td width="1020" bgcolor="#ED909A"><?php include 'inc_foot.htm' ?></td>
    <td bgcolor="#ED909A">&nbsp;</td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>