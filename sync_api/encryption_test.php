<?php

$key = '148492020202::024921';
$oriData = '093111414422';

$encryptedData = encrypt($key, $oriData);
$decryptedData = decrypt($key, $encryptedData);

echo $encryptedData."<br/>\n";
echo $decryptedData."<br/>\n";

echo 'enc';


function encrypt($key, $payload)
{
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($payload, 'aes-256-cbc', $key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function decrypt($key, $garble)
{
    list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
}

?>