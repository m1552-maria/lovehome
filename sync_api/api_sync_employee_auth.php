<?php
$allowedIPs = array(
    '123.240.153.70',
    '59.125.14.12'
);
//TODO: filter IP
$ip = '';
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

echo $ip."<br/>\n";
include_once('../connect_db.inc.php');

$encoded = encrypt("key", "password"); //RkxTT2J5SkVzZm5VTW9FMHViMU1xUT09OjrJfvikoZhWwSxYeErbiroN
$decoded = decrypt("key", $encoded); //password

echo $encoded."<br/>\n";
echo $decoded."<br/>\n";
// $key = openssl_random_pseudo_bytes(32);
// echo $key."<br/>\n";
// echo $a = base64_encode($key)."<br/>\n";
// echo $b = base64_encode($a)."<br/>\n";

if(isset($db)) {


    $json = file_get_contents('php://input');
    echo strlen($json)."<br/>\n";
    // Converts it into a PHP object
    // echo $json."<br/>\n";
    $data = json_decode($json, true);
    $dataTime = $data['dataTime'];
    $employeeList = $data['employees'];
    $key = md5($dataTime."::".$dataTime);
    echo 'dataTime = '.$dataTime."<br/>\n";
    echo 'key = '.$key."<br/>\n";
    // print_r($data['employees']);

    try{
        $db->exec('use admin_cache');
        $query = "Insert into sync_task "
                ."Set source_IP = :sourceIP, "
                ."source_data = :sourceData, "
                ."data_time = :dataTime, "
                ."status = :status, "
                ."note = :note ";
        $db->prepare($query)->execute(array(
            'sourceIP' => $ip,
            'sourceData' => $json,
            'dataTime' => $dataTime,
            'status' => 0,
            'note' => ''
        ));
        $taskId = $db->lastInsertId();


        echo $taskId.' inserted'."<br/>\n";

        $query = "Insert into employee "
                ."Set syncTaskId = :taskId, "
                ."empID = :employeeId, "
                ."empName = :name, "
                ."`password` = :password, "
                ."tel = :tel, "
                ."mobile = :mobile ";
        $stmt = $db->prepare($query);
        try{
            $db->beginTransaction();
            foreach ($employeeList as $employee) {
                $stmt->execute(array(
                    'taskId' => $taskId,
                    'employeeId' => $employee['employeeId'],
                    'name' => $employee['name'],
                    'password' => $employee['login_key'],
                    'tel' => $employee['tel'],
                    'mobile' => $employee['mobile']
                ));
            }
            $db->commit();
        }
        catch(Exception $ex) {
            $db->rollback();
            echo $ex;
        }


        // $sleep(1);

        $query = "Update sync_task "
                ."Set finish_time = NOW(), "
                ."status = 1 "
                ."Where task_id = :taskId ";
        $db->prepare($query)->execute(array(
            'taskId' => $taskId
        ));
        echo $taskId.' time modified'."<br/>\n";
    }
    catch( PDOException $pdoEx) {
        echo $pdoEx->getMessage();
    }




}


function encrypt($key, $payload)
{
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($payload, 'aes-256-cbc', $key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function decrypt($key, $garble)
{
    list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
}

?>