<?php
	include_once('connect_db.inc.php');
?>
<style>
#maintx {
	background-color:#f7eee5;
	padding:0; 	
}

/* #slides {	position:absolute;	top:15px;	left:4px;	z-index:100;} */
/*
	Slides container
	Important:
	Set the width of your slides container
	Set to display none, prevents content flash
*/

.slides_container {
	height: 276px;
	width:828px;
	overflow:hidden;
	position:relative;
	display:block;
}

/*
	Each slide
	Important:
	Set the width of your slides
	If height not specified height will be set by the slide content
	Set to display block
*/

.slides_container a {
	width:828px;
	height:270px;
	display:block;
}

.slides_container a img {
	display:block;
}

/*
	Next/prev buttons
*/

#slides .next,#slides .prev {
	position:absolute;
	top:107px;
	left:-39px;
	width:24px;
	height:43px;
	display:block;
	z-index:101;
}

#slides .next {
	left:585px;
}

/*
	Pagination
*/

.pagination {
	margin:8px auto 0;
	width:100px;
}

.pagination li {
	float:left;
	margin:0 1px;
	list-style:none;
}

.pagination li a {
	display:block;
	width:12px;
	height:0;
	padding-top:12px;
	background-image:url('Scripts/images/pagination.png');
	background-position:0 0;
	float:left;
	overflow:hidden;
}

.pagination li.current a {
	background-position:0 -12px;
}
</style>
<script type="text/javascript" src="Scripts/jquery.easing.min.js"></script>
<script type="text/javascript" src="Scripts/slides.jquery.js"></script>
<script type="text/javascript">
	$(function(){
		$('#slides').slides({
			preload: true,
			preloadImage: 'Scripts/images/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true
		});
	});
</script>
<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr><td>
  <div id="slides">
    <div class="slides_container">
		<?php 
			
			$query = "select * from newspic order by previge";
			$stmt = $db->prepare($query);
			$stmt->execute();
		
		while($r = $stmt->fetch()) {
			if($r['link']) {
				echo '<a href="'.$r['link'].'" target="_blank"><img src="'.$newspic_Path.$r['purl'].'" border=0/></a>'; 
			}
			else {
				echo '<img src="'.$newspic_Path.$r['purl'].'"/>';
			}	

		} 
		?>
    </div>  
  </div>  
  </td></tr>
  <tr><td valign="top">
    <table width="96%" border="0" align="center" cellspacing="0">
      <tr><td height="38" align="left" valign="middle"><img src="images/news2.jpg" width="171" height="33" /></td></tr>
      <tr><td valign="top"><?php include 'cms/viewmain.php' ?></td></tr>
    </table>
  </td></tr>
  <tr height="20"><td></td></tr>
  <tr><td><?php include "about.htm" ?></td></tr>
  <tr height="8"><td></td></tr>
  <tr><td><?php include "inc_others.htm" ?></td></tr>
</table>
