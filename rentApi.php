<?php
	// include 'config.php';
	include_once('config.inc.php');
	include_once('connect_db.inc.php');
    // $VenderDB = 'mariaadm';

	// $db->query('use '.$VenderDB);
	// mysql_select_db($VenderDB,$conn) or die("資料庫選擇失敗!");
	// mysql_query("SET NAMES 'utf8'");


	// $sql = sprintf(
	// 	"SELECT empName,tel,mobile,empID FROM emplyee WHERE empID = '%s' AND password = SHA2('%s', 256)",
	// 	trim($_POST['empID']),
	// 	getRulPwd(trim($_POST['empID']), trim($_POST['password']))
    // );
	// $rs	 = db_query($sql);

	// if(db_eof($rs)){
    // 	echo json_encode("帳號密碼錯誤");
	// }else{
	// 	session_start();
	// 	$_SESSION['empData'] = db_fetch_array($rs);
	// 	$date = date_create();
	// 	$_SESSION['timeS']   = date_timestamp_get($date);
	// 	echo $_SESSION['timeS'];
	// }

	$db->query('use admin_cache');

	$query = "Select task_id, data_time "
			."From sync_task "
			."Where status = 1 "
			."Order by task_id desc ";
	$stmt = $db->prepare($query);
	$stmt->execute();
	if($row = $stmt->fetch()) {
		$taskId = $row['task_id'];
		$dataTime = $row['data_time'];
		$key = md5($dataTime."::".$dataTime);


		$hashedPwd = hash('sha256', getRulPwd(trim($_POST['empID']), trim($_POST['password'])));
		$query = "Select empName, tel, mobile, empID "
				."From employee "
				."Where empID = :empID "
				."And `password` = :hashedPwd "
				."And syncTaskId = :taskId ";
		$stmt = $db->prepare($query);
		$stmt->execute(array(
			'empID' => trim($_POST['empID']),
			'hashedPwd' => $hashedPwd,
			'taskId' => $taskId
		));
	
		if($empData = $stmt->fetch()) {
			session_start();
			$empData['tel'] = decrypt($key, $empData['tel']);
			$empData['mobile'] = decrypt($key, $empData['mobile']);
			$_SESSION['empData'] = $empData;
			$date = date_create();
			$_SESSION['timeS']   = date_timestamp_get($date);
			echo $_SESSION['timeS'];
		}
		else {
			echo json_encode("帳號密碼錯誤");
		}

	}

	

    
	function decrypt($key, $garble)
	{
		list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
		return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
	}
?>