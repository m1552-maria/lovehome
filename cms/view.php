<?php
 //::面板模式 : 變數由 includer 設定， 並unclude config.php
 //default used variants
	if(!isset($db)) {
		include_once('../connect_db.inc.php');
	}
	$self = $_SERVER['PHP_SELF'];
	$wkmode = (isset($_REQUEST['wkmode']))? $_REQUEST['wkmode']: 0;

	$cid = (isset($_GET['ClassID']) ? $_GET['ClassID'] : 0); 
	//  if (!$cid) $cid=0; 					//類別
	if (!isset($tbl)) $tbl='contents'; 	//資料表
	if (!isset($wkmode)) $wkmode=false; //內容 或 清單
	if (!isset($ulpath)) $ulpath=''; 		//檔案上傳的路徑

	if(!$wkmode) {   //換頁處理 
		// $r = db_fetch_array(db_query("select count(*) from $tbl where classid='$cid' and unValidDate>Now()",$conn));
		$query = "select count(*) "
				."from ".$tbl." "
				."where classid=:cid "
				."and unValidDate>Now()";
		$stmt = $db->prepare($query);
		$stmt->execute(array('cid' => $cid));
		$r = $stmt->fetch();

		$recs   = $r[0];															//紀錄數
		$pageln = 20;																//每頁幾行
		$page   = (isset($_REQUEST['page']))?$_REQUEST['page']:0;										//第幾頁
		$pages  = ceil($recs / $pageln);  						//總頁數
		// $sql = "select * from $tbl where classid='$cid' and unValidDate>Now() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		// $rs = db_query($sql,$conn);
		$query = "select * "
				."from ".$tbl." "
				."where classid = :cid and unValidDate>Now() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		$stmt = $db->prepare($query);
		$stmt->execute(array('cid' => $cid));


	} else {		//單筆
		// $sql = "select * from $tbl where ID=".$_REQUEST['ID'];	
		// $rs = db_query($sql,$conn);
		// 	$r=db_fetch_array($rs);
		$query = "Select * "
				."From ".$tbl." "
				."Where ID = :ID ";
		$stmt = $db->prepare($query);
		$stmt->execute(array('ID' => $_REQUEST['ID']));
		$r = $stmt->fetch();
	} 
//  var_dump($_GET['ClassID']);
//  var_dump($cid);
?>
<link href="/cms/cms.css" rel="stylesheet" type="text/css">
<?php if(!$wkmode) { //清單模式 ?>
	<table id="news" width="100%" border="0" cellspacing="0" cellpadding="1" class="sTable">
	<?php
		// while($rs and $r=db_fetch_array($rs)) {
		while($r = $stmt->fetch()) { 
	?>
  	<tr>
    	<td width="10"><img src="images/point01.gif"/></td>
			<td class="cell"><span style="color:#993300"><?= date('Y/m/d',strtotime($r['R_Date'])) ?></span> <a href="<?=$self?>?ID=<?=$r['ID']?>&wkmode=1"><?=$r['SimpleText']?></a></td>
   	</tr>
	<?php } ?>
	
  <tr><td colspan="2">
  <?php //page row creation
	if($pages>1) {
	  if ($page>0) echo '<a href="'.$self.'?incfn=inc_news.php&page='.($page-1).'">前一頁</a>';
	  $pg = floor($page / 10); 
	  $i = 0;
		$p = $i+($pg*10);
		while ( ($p<$pages) && ($i<10) ) {
		  echo " | ";
			if ($p==$page) echo ($p+1); else echo '<a href="'.$self.'?incfn=inc_news.php&page='.$p.'">'.($p+1)."</a>";
			$i++;
			$p = $i+($pg*10);
		}
    echo " | ";
	  if ($page < $pages-1) echo '<a href="'.$self.'?incfn=inc_news.php&page='.($page+1).'">下一頁</a>';
	}	?>
  </td></tr>
	</table>
<?php } else { //內容模式 ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="4" class="sTable">
    <tr><td class="hdCell"><?=$r[Ahead]?></td></tr>
    <tr><td class="hd2Cell"><?=date('Y/m/d',strtotime($r['R_Date'])) ?></td></tr>
  	<tr><td><p><?=$r[Content]?></p></td></tr>
	</table>
  <p>&gt;&gt; <a href='javascript:history.back()'>返回</a></p>
<?php } ?>