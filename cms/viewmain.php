<?php
	//::面板模式 : 變數由 includer 設定， 並unclude config.php
	//default used variants
	if(!isset($db)) {
		include_once('../connect_db.inc.php');
	}
	$self = $_SERVER['PHP_SELF'];
	$wkmode = (isset($_REQUEST['wkmode']))?$_REQUEST['wkmode']:'';
	$newsImg = '';								//第一筆最新消息的Ahead圖

	$query = "select distinct ClassID "
			."from contents "
			."order by ClassID asc limit 2";
	$stmt = $db->prepare($query);
	$stmt->execute();
	$count = 0;
	while($row = $stmt->fetch()) {
		$count++;
	}

	// $cidCount = db_query("select distinct ClassID from contents order by ClassID asc limit 2",$conn);
	// $count = db_num_rows($cidCount); 
	//  var_dump($count);

	if (!isset($cid)) $cid=0; 					//類別
	if (!isset($cityid)) $cityid=1;				//類別
	if (!isset($tbl)) $tbl='contents'; 	 //資料表
	if (!isset($wkmode)) $wkmode=false;	 //內容 或 清單
	if (!isset($ulpath)) $ulpath=''; 		 //檔案上傳的路徑

	if(!$wkmode) {   //換頁處理 
		// $r = db_fetch_array(db_query("select count(*) from $tbl where classid='$cid' and unValidDate>Now()",$conn));
		$query = "select count(*) "
				."from ".$tbl." "
				."where classid = :cid "
				."and unValidDate > Now() ";
		$stmt = $db->prepare($query);
		$stmt->execute(array('cid' => $cid));
		$r = $stmt->fetch();

		// $Cityr =db_fetch_array(db_query("select count(*) from $tbl where classid='$cityid' and unValidDate>Now()",$conn));
		$query = "select count(*) "
				."from ".$tbl." "
				."where classid = :cityid "
				."and unValidDate > Now() ";
		$stmt = $db->prepare($query);
		$stmt->execute(array('cityid' => $cityid));
		$Cityr = $stmt->fetch();


		$recs   = $r[0];										//紀錄數
		$pageln = 5;												//每頁幾行
		$page   = (isset($_REQUEST['page']))?$_REQUEST['page']:0;					//第幾頁
		$pages  = ceil($recs / $pageln);  	//總頁數

		// $sql = "select * from $tbl where classid='$cid' and unValidDate>Now() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		// $rs = db_query($sql,$conn);

		$query = "select * "
				."from ".$tbl." "
				."where classid = :cid "
				."and unValidDate > Now() "
				."order by R_Date Desc limit ".($page * $pageln).", ".$pageln;
		$rs = $db->prepare($query);
		$rs->execute(array('cid' => $cid));


		// $Citysql = "select * from $tbl where classid='$cityid' and unValidDate>Now() order by R_Date Desc limit ".$page*$pageln.",$pageln";
		// $Cityrs = db_query($Citysql,$conn);
		$query = "select * "
				."from ".$tbl." "
				."where classid = :cityid "
				."and unValidDate > Now() "
				."order by R_Date Desc limit ".($page * $pageln).", ".$pageln;
		$Cityrs = $db->prepare($query);
		$Cityrs->execute(array('cityid' => $cityid));


	} else {		//單筆
		// $sql = "select * from $tbl where ID=".$_REQUEST['ID'];
		// $rs = db_query($sql,$conn);
		// $r=db_fetch_array($rs);

		$query = "Select * "
				."From ".$tbl." "
				."Where ID = :ID ";
		$stmt = $db->prepare($query);
		$stmt->execute(array('ID'=> $_REQUEST['ID']));
		$r = $stmt->fetch();

		// Useless codes when wkmode == true, comment out by Hao-Tung 2022-08-27
		// $Citysql = "select * from $tbl where ID=".$_REQUEST['ID'];	
		// $Cityrs = db_query($Citysql,$conn);
		// $Cityr = db_fetch_array($Cityrs);
	} 

?>
<link href="/cms/cms.css" rel="stylesheet" type="text/css">
<style>
	p.tip {
	font-weight:bold;
	font-size:16px;
	color:#0000FF;
	}
</style>
<?php if(!$wkmode) { //清單模式 ?>
	<div id="div_newsImg"><img id="newsImg" width="150"/></div>
	<table id="news" width="620" border="0" cellspacing="0" cellpadding="1" class="sTable" align="right" style="margin-top: -55px;">
	<tr ><td colspan="2"><p class="tip">最新消息:</p></td></tr>
	<?php 
		// while($rs and $r=db_fetch_array($rs)) { 
		while($r = $rs->fetch()) {
			if($r['Ahead'] && !$newsImg) $newsImg=$r['Ahead']; 
	?>
  	<tr>
    	<td class="date"><?= date('Y/m/d',strtotime($r['R_Date'])) ?></td>
			<td class="cell"><a href="<?=$self?>?ID=<?=$r['ID']?>&wkmode=1"><?=$r['SimpleText']?></a></td>
   	</tr>
	<?php } 
		 if(!$newsImg) $newsImg='news.jpg';
	?>
 	<tr><td colspan="2" class="more"><a href='index.php?incfn=inc_news.php'> &gt; 更多消息</a></td></tr>
	</table>

	<table id="cityNews" width="620" height="" border="0" cellspacing="0" cellpadding="1" class="sTable" align="right">
	<tr><td colspan="2"><p class="tip">市府最新消息:</p></td></tr>
	<?php 
		// while($Cityrs and $Cityr=db_fetch_array($Cityrs)) { 
		while($Cityr = $Cityrs->fetch()) {
			if($Cityr['Ahead'] && !$newsImg) $newsImg=$Cityr['Ahead']; 
	?>
  	<tr>
    	<td class="date"><?= date('Y/m/d',strtotime($Cityr['R_Date'])) ?></td>
			<td class="cell"><a href="<?=$self?>?ID=<?=$Cityr['ID']?>&wkmode=1"><?=$Cityr['SimpleText']?></a></td>
   	</tr>
	<?php } 
		 if(!$newsImg) $newsImg='news.jpg';
	?>
 	<tr><td colspan="2" class="more"><a href='index.php?incfn=inc_news.php&ClassID=1'> &gt; 更多消息</a></td></tr>
	</table>
<?php 
	} 
	else {
	 //內容模式 
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="4" class="sTable">
    <tr><td class="hdCell"><?=$r['SimpleText']?></td></tr>
    <tr><td class="hd2Cell"><?=date('Y/m/d',strtotime($r['R_Date'])) ?></td></tr>
  	<tr><td><p><?=$r['Content']?></p></td></tr>
	</table>
  <p>&gt;&gt; <a href='javascript:history.back()'>返回</a></p>
<?php } ?>
<script>$(function(){ $('#newsImg').attr('src','<?= '/data/bullets/'.$newsImg?>'); });</script>