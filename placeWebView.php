<?php
	// include 'config.php';
    include_once('connect_db.inc.php');

	$bDate = date("Y-m-d 00:00:00");
	$eDate = date("Y-m-d 23:59:59");

	$sql   = "SELECT b.title as classTitle, b.id, c.title as floorTitle, c.floor, a.bDate, a.eDate, a.title FROM rentplace a JOIN space b ON(a.places = b.id) JOIN spaceclass c ON(b.sId = c.id) WHERE b.userGroup <> '2' AND a.applier NOT LIKE '財團法人瑪利亞社會福利基金會%' AND a.isView = '1' AND c.floor <> '0' AND ((a.bDate BETWEEN '{$bDate}' AND '{$eDate}') AND (a.eDate BETWEEN '{$bDate}' AND '{$eDate}')) ORDER BY c.floor DESC, b.id, a.bDate ASC";
	$query = "SELECT b.title as classTitle, b.id, c.title as floorTitle, c.floor, a.bDate, a.eDate, a.title, a.applier "
			."FROM rentplace a "
			."JOIN space b "
			."	ON(a.places = b.id) "
			."JOIN spaceclass c "
			."	ON(b.sId = c.id) "
			."WHERE b.userGroup <> '2' "
			."AND a.applier NOT LIKE '財團法人瑪利亞社會福利基金會%' "
			."AND a.isView = '1' "
			."AND c.floor <> '0' "
			."AND ((a.bDate BETWEEN '{$bDate}' AND '{$eDate}') AND (a.eDate BETWEEN '{$bDate}' AND '{$eDate}')) "
			."ORDER BY c.floor DESC, b.id, a.bDate ASC";
	// $rs    = db_query($sql,$conn);
	$stmt = $db->prepare($query);
	$stmt->execute();

	$rentplace = array();
	$floorArr  = array();
	$classArr  = array();

	// while($r = db_fetch_array($rs)){
	while($r = $stmt->fetch()) {
		if(!isset($rentplace[$r['floor']])){
			$rentplace[$r['floor']] = array();
		}

		$r['bTime'] = date("H:i", strtotime($r['bDate']));
		$r['eTime'] = date("H:i", strtotime($r['eDate']));

		$floorArr[$r['floor']] = $r['floorTitle'];
		$classArr[$r['id']]    = $r['classTitle'];

		$rentplace[$r['floor']][$r['id']][] = "<b style='color:red'>" . $r['bTime'] . " ~ " . $r['eTime'] . "　" . "</b>(" . $r['applier'] . ") " .$r['title'];

	}



?>
<style type="text/css">
	body{
		background-color: #fde3df;
	}
	table{
		border: 3px #E16C7C solid;
		background-color: #fde3df;
		width: 100%;
	}
	td{
		padding: 5px;
	}
	thead td{
		font-size: 20px;
	}
	.floorTr{
		font-size: 18px;
	}
	h2{
		text-align: center;
	}
</style>
<h2>臺中市愛心家園 <b style='color:red'><?php echo date("Y-m-d") ?></b> 場地租借資訊</h2>
<table border='1'>
	<thead>
		<tr>
			<td width="75px">樓層</td>
			<td colspan="7">教室</td>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($rentplace as $key => $value) {

				echo "<tr class='floorTr'><td style='color:red;'>" . $floorArr[$key] . "</td>";

				foreach ($value as $k => $v) {

					echo "<td style='text-align: center;'>" . $classArr[$k] . "</td>";

				}

				echo "</tr><tr><td>課程內容</td>";

				foreach ($value as $k => $v) {

					echo "<td>";

					foreach ($v as $kk => $vv) {
						
						echo $vv;
						echo "<br>";
						
					}

					echo "</td>";

				}

				echo "</tr>";
				
			}
		?>
	</tbody>
</table>