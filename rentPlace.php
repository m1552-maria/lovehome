<?php
    session_start();
    if($_REQUEST['userGroup'] == 2){
        if(!isset($_REQUEST['timeS']) || ($_REQUEST['timeS'] != $_SESSION['timeS']) || empty($_REQUEST['timeS'])){
            session_unset();
            session_destroy();
            header("Location: index.php?incfn=inc_RentPlace.php");
            exit;
        }
    }
    function getSafely($key) {
        global $_REQUEST;
        return isset($_REQUEST[$key])?$_REQUEST[$key]:'';
    }
    include_once('connect_db.inc.php');
	// include 'config.php';
    // if($_REQUEST['view'] == 'maintain'){
    $query = "SELECT s.*, c.title as floorTitle "
            ."FROM space s "
            ."JOIN spaceclass c "
            ."  ON(s.sId = c.id) ";
    $conditionClause = '';
    $queryParams = array();
    if(isset($_REQUEST['view']) && strcmp($_REQUEST['view'], 'maintain') === 0){
        // $sql = "SELECT s.*, c.title as floorTitle FROM space s JOIN spaceclass c ON(s.sId = c.id) order by s.sId, s.title, s.id";
    }else{
        $conditionClause = "WHERE userGroup = '0' OR userGroup = '{$_REQUEST['userGroup']}' ";
        $queryParams['userGroup'] = $_REQUEST['userGroup'];
        // $sql = "SELECT s.*, c.title as floorTitle FROM space s JOIN spaceclass c ON(s.sId = c.id) WHERE userGroup = '0' OR userGroup = '{$_REQUEST['userGroup']}' order by s.sId, s.title, s.id";
    }
    $query .= $conditionClause." order by s.sId, s.title, s.id";
    $stmt = $db->prepare($query);
    $stmt->execute($queryParams);
    // $rs = db_query($sql,$conn);
    $rentData = array();
    // while($r=db_fetch_array($rs)){
    while($r = $stmt->fetch()) {
        $rentData[$r['id']] = $r['floorTitle'].' - '.$r['title'];
    }

    $sql = "SELECT * FROM space_item";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    // $rs = db_query($sql,$conn);
    $itemData = array();
    // while($r=db_fetch_array($rs)){
    while($r = $stmt->fetch()) {
        $itemData[$r['type']][] = $r['title'];
    }

    //::萬年曆
    $day_array=array("日","一","二","三","四","五","六"); 
    $this_year=date("Y",time()); 
    $this_month=date("m",time());
    // if($_REQUEST['act']=="chgUnit"){//換機構時 初始年、月份  !isset($_REQUEST['act']) || 
    //     unset($_SESSION['sched_y']);
    //     unset($_SESSION['sched_m']);
    // }
    if(!isset($_SESSION['sched_y'])){//處理年份
        $y = $this_year; 
        $_SESSION['sched_y']=$this_year;
    }else{  
        $y = intval($_SESSION['sched_y']);
    }
    if(!isset($_SESSION['sched_m'])){//處理月份
        $m = $this_month; 
        $_SESSION['sched_m'] = $this_month;
    }else{  
        $m = intval($_SESSION['sched_m']);
    }

    $this_day=date("j",time()); 
    $title_string=$this_year."年".$this_month."月"; 
    $past_to_today=(intval(date("U",mktime(0,0,0,$m,$this_day,$y))/86400)); //1970年1月1日至今的天數 
    $past_to_this_month_first_day=(intval(date("U",mktime(0,0,0,$m,1,$y))/86400)); //1970年1月1日至本月份第一天的天數 
    $what_day_is_fisrt_day=date("w",mktime(0,0,0,$m,1,$y)); //這個月第一天是星期幾 
    $day_counter=1-$what_day_is_fisrt_day; //顯示這個月第一天的位置
    $how_many_days=mktime(0,0,0,$m+1,0,$y); 
    $how_many_days_this_month=(strftime("%d",$how_many_days)); //這個月有幾天 
    $displayY=$this_year;
    $displayM=$m;

    if($m>12){
        $displayM = $m%12;
        if($displayM == 0) $displayM = 12;
        $displayY += floor(($m - $displayM)/12);
    }

    if($m<1){
        $displayM = 12+($m%12);
        if($displayM == 0) $displayM = 12;
        $displayY += floor(($m - $displayM)/12); 
    }

    if(isset($_REQUEST['visitId'])){    
        $visitId = $_REQUEST['visitId'];
    }else{  
        $sql="select * from space";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        // $rs=db_query($sql);
        $_SESSION["VISIT_UNITS"]=array();
        // while($r=db_fetch_array($rs)){  
        while($r = $stmt->fetch()) {
            if(!isset($visitId)) {
                $visitId=$r['id'];
            }
            // $_SESSION["VISIT_UNITS"][$r['id']] = $r['unit'].",".$r['notice_day'];
            //TO-DO: 待確認，資料庫沒有unit這欄，此行一直出錯，先改成最可能的visit_unit by Hao-Tung 2022/08/28
            $_SESSION["VISIT_UNITS"][$r['id']] = $r['visit_unit'].",".$r['notice_day'];
        }
    }
    if(isset($_REQUEST['time'])){
        $option = "<option value='{$_REQUEST['time']}'>{$_REQUEST['time']}</option>";
    }

    //::各單位資料
    $sql="select * from space where id = :visitId ";
    $stmt = $db->prepare($sql);
    $stmt->execute(array('visitId' => $visitId));
    // $rs=db_query($sql);
    $data=array();
    // $r=db_fetch_array($rs);
    $r = $stmt->fetch();
    // $data['unit']=$r['unit'];        //TO-DO: 待確認，資料庫沒有unit這欄
    $data['week_day']=$r['week_day'];
    $data['visit_unit']=$r['visit_unit'];
    $data['id']=$r['id'];
    $data['start_booking']=$r['start_booking'];

    //TO-DO: 待確認，資料庫沒有這些欄位
    // $data['clients']=$r['clients'];
    // $data['visit_content']=$r['visit_content'];
    // $data['requirement']=$r['requirement'];
    // $data['contactinfo']=$r['contact'].' '.$r['tel'];
    $data['notice_day']=$r['notice_day'];
    $data['notes']=$r['notes'];
    $data['uper_limit']=$r['uper_limit'];


    //多開分頁問題 //20220905
    if(!empty($_SESSION['sched_m']) && !empty($_SESSION['sched_y'])){
        $sessionDate = $displayY."-".$displayM."-01";
        $overTime = "+".$data['start_booking']." month";
        if(empty($_GET['view']) && strtotime(date("Y-m-t", strtotime($overTime))) < strtotime($sessionDate)){

            $_SESSION['sched_y'] = date("Y");
            $_SESSION['sched_m'] = date("m");
            $y = date("Y");
            $m = date("m");
            $displayY = date("Y");
            $displayM = date("m");
        }
    }
    
    //::各單位開放時間
    $sql="select * from space_detail ";
    $sql.="where visitId='".$visitId."' ";
    $sql.="order by visitId,week_day,CAST(startTime as time) ASC";

    
    $query = "select * from space_detail "
            ."where visitId = :visitId "
            ."order by visitId,week_day,CAST(startTime as time) ASC";

    $stmt = $db->prepare($query);
    $stmt->execute(array('visitId' => $visitId));
    // $rs=db_query($sql);
    $timeData=array();
    $weekDayData=array();
    $bSettingTime=false;
    // if($rs){
        $bSettingTime=true;
        // while($r=db_fetch_array($rs)){  
            while($r = $stmt->fetch()) {
            if(!isset($timeData['id'])          || !is_array($timeData['id'])){ $timeData['id']=array();}
            if(!isset($timeData['visitId'])     || !is_array($timeData['visitId'])){    $timeData['visitId']=array();}
            if(!isset($timeData['startTime'])   || !is_array($timeData['startTime'])){  $timeData['startTime']=array();}
            if(!isset($timeData['endTime'])     || !is_array($timeData['endTime'])){    $timeData['endTime']=array();}
            if(!isset($timeData['week_day'])    || !is_array($timeData['week_day'])){   $timeData['week_day']=array();}
            if(!isset($timeData['notice_day'])  || !is_array($timeData['notice_day'])){ $timeData['notice_day']=array();}
            array_push($timeData['id'],$r['id']);
            array_push($timeData['visitId'],$r['visitId']);
            array_push($timeData['startTime'],$r['startTime']);
            array_push($timeData['endTime'],$r['endTime']);
            array_push($timeData['week_day'],$r['week_day']);

            //計算當天有幾個時段可供預約
            if(!isset($weekDayData[$r['visitId']])  ||!is_array($weekDayData[$r['visitId']])){ $weekDayData[$r['visitId']]=array();}
            if(!isset($weekDayData[$r['visitId']][$r['week_day']])  ||!is_array($weekDayData[$r['visitId']][$r['week_day']])){ $weekDayData[$r['visitId']][$r['week_day']]=array();}
            $weekDayData[$r['visitId']][$r['week_day']][$r['id']]=1;
        }
    // }   
    
    //::取得當月預約資料
    $week_day=explode(",",$data['week_day']);
    $sql="select * from rentplace ";
    $sql.="where YEAR(bDate) = ".$displayY." and MONTH(bDate) = ".$displayM." ";
    $sql.="and places='".$visitId."'";

    $query = "select * from rentplace "
            ."where YEAR(bDate) = :displayY and MONTH(bDate) = :displayM "
            ."and places = :visitId ";
    $stmt = $db->prepare($query);
    $stmt->execute(array(
        'displayY' => $displayY,
        'displayM' => $displayM,
        'visitId' => $visitId
    ));
    
    // $rs=db_query($sql);
    // $bookRS=db_query($sql);
    //::統計目前預約記錄，計算餘額
    $bookingData=array();
    // if($bookRS){
        // while($r=db_fetch_array($bookRS)){
        while($r = $stmt->fetch()) {
            $bfull=explode(" ",$r['bDate']);
            $vd=explode("-",$bfull[0]);
            $vt=explode(":",$bfull[1]);
            $wd=date("w",mktime(0,0,0,$vd[1],$vd[2],$vd[0])); //研習當天是星期幾 

            $efull=explode(" ",$r['eDate']);
            $evd=explode("-",$efull[0]);
            $evt=explode(":",$efull[1]);

            $index=0;
            foreach($timeData as $k=>$v ){//判斷預約時間落在什麼時段
                foreach($v as $k1=>$v1){
                    if($k=="week_day" && $wd==$v1){
                        $tWeekDay=$v1;
                        $tId=$timeData['id'][$k1];
                        $tSt=explode(":",$timeData['startTime'][$k1]);
                        $tEt=explode(":",$timeData['endTime'][$k1]);
                        $sH=intval($tSt[0]);    
                        $sM=intval($tSt[1]);    
                        $eH=intval($tEt[0]);    
                        $eM=intval($tEt[1]);

                        if(!isset($bookingData[$vd[0]]) || !is_array($bookingData[$vd[0]])) $bookingData[$vd[0]]=array(); 
                        if(!isset($bookingData[$vd[0]][$vd[1]]) || !is_array($bookingData[$vd[0]][$vd[1]])) $bookingData[$vd[0]][$vd[1]]=array();
                        if(!isset($bookingData[$vd[0]][$vd[1]][$vd[2]]) || !is_array($bookingData[$vd[0]][$vd[1]][$vd[2]])) $bookingData[$vd[0]][$vd[1]][$vd[2]]=array();

                        $bool=false;
                        if(intval($vt[0])>$sH && intval($vt[0]<$eH)){
                            $bool = true;
                        }else if((intval($vt[0])==$sH && intval($vt[1])==$sM) && (intval($evt[0])==$eH && intval($evt[1])==$eM)){
                            $bool = true;
                        }
                        if($bool){
                            if(!isset($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]) || !is_array($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId])){  
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId] = array();
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count'] = 0;
                            }
                            if(!isset($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['st'])){  
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['st'] = $timeData['startTime'][$k1];
                            }
                            if(!isset($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['et'])){  
                                $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['et'] = $timeData['endTime'][$k1];
                            }
                            $bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count'] = intval($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]['count'])+1;
                        }
                    }
                }
            }
        }
    // }

    //當天是否有關閉時段
    $sql="select * from space_close where (visitId = '$visitId' or visitId = 'all')";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    // $rs=db_query($sql);
    $closeY=array();
    $closeM=array();
    $closeD=array();
    $closeN=array();

    // if(!db_eof($rs)){
        // while($r=db_fetch_array($rs)){
        while($r = $stmt->fetch()) {
            array_push($closeY,$r['close_y']);
            array_push($closeM,$r['close_m']);
            array_push($closeD,$r['close_d']);
            array_push($closeN,$r['notes']);
        }
    // }
?>
<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/home2.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="Keywords" content="捐款,愛心,公益,社福,基金會,瑪利亞,身心障礙,天使,社會責任,抵稅,生命教育,社會宣導,瑪利亞社會福利基金會,智能障礙,自閉症,唐氏症,腦性麻痺,多重障礙,早期療育,麵包,輔具,清潔">
<!-- InstanceBeginEditable name="doctitle" -->
<title>臺中市愛心家園</title>
<link href="/Scripts/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/Scripts/jquery-ui-timepicker-addon.css">
<style>
    #invinfo { display:none; }
    .grid {
        width: 100%;
    }
    table.grid td {   
        border: 1px solid #ccc;
        text-align:center;
        font-family:Verdana, Geneva, sans-serif;
        font-size:12px;
        width:120px;
        padding-top:8px;
        padding-bottom:8px;
    }
    .tdTitle{   background:#a91d3b; color:#fff;padding:8px;}
    #tdWeek{    background:#d59416; color:#fff;padding:8px;}
    #tdWeekEnd{ background:#f4c5bf; color:#fff;padding:8px;}
    .tdWeekEndClass{ background:#f4c5bf; color:#fff;padding:8px;}
    #sel_unit{  float:left;color: black;}
    .tip{   color:#999;  }
    .tip2{  color:#FC3;  fonts-size:12px; float:right;}
    .tip3{  color:#C00;  font-weight:bold;}
    .tip4{   font-weight:bold;}
    .tip5{  color:#F30;text-decoration: underline; font-weight:bold;}
    .tdNone{    background:#f0f0f0;color:#999;}
    .tdNone2{   background:#ffffff;color:#999;}

    .btn2 {
      width:60px;
      background: #808080;
      padding: 3px 6px 3px 6px;
      float:right;
    }
    .btn2:hover {
      background: #999;
      text-decoration: none;
    }
    #divforgot, #forgotList{
        position:absolute;
        left: 50%; top:50%;
        background-color:#ffa4b0;
        padding: 20px;
        display:none;
        width: 250px;
        margin-left: -125px;
        margin-top: -50px;
    }
</style>
<script type="text/javascript" src="Scripts/jquery.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.7.2.custom.min.js"></script> 
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-timepicker-zh-TW.js"></script>
<script>
    $(function(){
        var userGroup = <?=$_REQUEST['userGroup'];?>;
        var visit_id='<?=$visitId?>';
        if(visit_id != ''){
            $("#sel_unit").val(visit_id);
        }
        if(userGroup == 2){
            var reg = new RegExp("([0]*)([1-9]+[0-9]+)", "g");
            var empData = <?=(isset($_SESSION['empData']))?json_encode($_SESSION['empData']):'{}'?>;
            $('input[name="applier"]').val("財團法人瑪利亞社會福利基金會");
            $('input[name="leadman"]').val(empData['empName']);
            $('input[name="contactman"]').val(empData['empName']);
            $('input[name="tel"]').val(empData['tel']);
            $('input[name="mobile"]').val(empData['mobile']);
            $('input[name="email"]').val('m'+empData['empID'].replace(reg, "$2")+"@maria.org.tw");
            $('#check1').attr('checked',true);
            $('#check2').attr('checked',true);
            $('#check3').attr('checked',true);
            $('#check4').attr('checked',true);
            $('#check5').attr('checked',true);
            $('#check6').attr('checked',true);
            $('#check7').attr('checked',true);
            $('#check8').attr('checked',true);
            $('#Submit')[0].disabled = false;
        }
        $(".reset").bind('click',function(){
            document.getElementById("form1").reset();
            if(userGroup == 2){
                var reg = new RegExp("([0]*)([1-9]+[0-9]+)", "g");
                var empData = <?=(isset($_SESSION['empData']))?json_encode($_SESSION['empData']):'{}'?>;
                $('input[name="applier"]').val("財團法人瑪利亞社會福利基金會");
                $('input[name="leadman"]').val(empData['empName']);
                $('input[name="contactman"]').val(empData['empName']);
                $('input[name="tel"]').val(empData['tel']);
                $('input[name="mobile"]').val(empData['mobile']);
                $('input[name="email"]').val('m'+empData['empID'].replace(reg, "$2")+"@maria.org.tw");
            }
        });
        var timeData=<?=json_encode($timeData);?>;        
        var tInfo='';
        var pre_weekDay='';
        var word=['日','一','二','三','四','五','六'];
        var wDay=0;
        for(var i=0;i<timeData['startTime'].length;i++){
            wDay=timeData['week_day'][i];
            if(pre_weekDay==""){
                pre_weekDay=wDay
                tInfo+="週"+word[wDay]+"：";
            }else if(pre_weekDay!=wDay){
                tInfo+="<br>";
                pre_weekDay=wDay; 
                tInfo+="週"+word[wDay]+"：";
            }else{
                tInfo+=" , ";
            }
            tInfo+=padLeft(timeData['startTime'][i], 5)+" ~ "+padLeft(timeData['endTime'][i], 5);
        }
        
        $('#td_time').html(tInfo);
    })
    function padLeft(str,lenght){
        if(str.length >= lenght) return str;
        else return padLeft("0"+str, lenght);
    }
    function checkitem() {
    	var pass = $('#check1').attr('checked') 
    	        && $('#check2').attr('checked')
    			&& $('#check3').attr('checked')
    			&& $('#check4').attr('checked')
    			&& $('#check5').attr('checked')
                && $('#check6').attr('checked')
                && $('#check7').attr('checked')
                && $('#check8').attr('checked')
    	$('#Submit')[0].disabled = !pass;
    }

    function formvalid(tfm) {
        $('#Submit')[0].disabled = true;
    	if(!tfm.applier.value) {alert('請填寫 申請單位'); tfm.applier.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.leadman.value) {alert('請填寫 負責人'); tfm.leadman.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.contactman.value) {alert('請填寫 聯絡人'); tfm.contactman.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.tel.value) {alert('請填寫 聯絡電話'); tfm.tel.focus(); $('#Submit')[0].disabled = false; return false; }
        if(!tfm.mobile.value) {alert('請填寫 行動電話'); tfm.mobile.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.email.value) {alert('請填寫 EMail'); tfm.email.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.title.value) {alert('請填寫 活動名稱'); tfm.title.focus(); $('#Submit')[0].disabled = false; return false; }
    	if(!tfm.peoples.value) {alert('請填寫 預估研習人數'); tfm.peoples.focus(); $('#Submit')[0].disabled = false; return false; }
        if(tfm.peoples.value > <?php echo $data['uper_limit'] ? $data['uper_limit']:0; ?>) {
            alert('預估研習人數超過此場地人數上限 <?php echo $data['uper_limit'] ? $data['uper_limit']:0; ?> 人'); 
            tfm.peoples.focus();
            $('#Submit')[0].disabled = false;
            return false;
        }

    	var v = parseInt(tfm.peoples.value); 
    	if(!v) { alert('研習人數只能為數值'); $('#Submit')[0].disabled = false; return false; }
    	
    	for(var i=0; i<form1.isInv.length; i++) {
    		var ck = (form1.isInv[i].checked);
    		if(ck) {
    			var v = form1.isInv[i].value;
    			if(v==3) {
    				if (!$("#invoiceTitle").val()) {alert('請填寫 發票抬頭'); $("#invoiceTitle").focus(); $('#Submit')[0].disabled = false; return false;};
    				if (!$("#invoiceNo").val()) {alert('請填寫 發票編號'); $("#invoiceNo").focus(); $('#Submit')[0].disabled = false; return false;};
    			}
    		}
    	}
    	
    	return true;
    }

    function checkInv() {
    	for(var i=0; i<form1.isInv.length; i++) {
    		var ck = (form1.isInv[i].checked);
    		if(ck) {
    			var v = form1.isInv[i].value;
    			if(v==3) $("#invinfo").css("display","block"); else {
    				$("#invinfo").css("display","none");
    				$("#invoiceTitle").val('');
    				$("#invoiceNo").val('');
    			}
    		}
    	}
    }
    function doOver(obj){
        $(obj).css("background","#a91d3b");
        $(obj).css("color","#fff");
    }
    function doOut(obj){
        $(obj).css("background","#fff");
        $(obj).css("color","#000");
    }
    function doOverWE(obj){
        $(obj).css("background","#a91d3b");
        $(obj).css("color","#fff");
    }
    function doOutWE(obj){
        $(obj).css("background","#f4c5bf");
        $(obj).css("color","#000");
    }

    function doBooking(d, t){
        var form = $("<form></form>");
        form.attr('action', 'rentPlace.php');
        form.attr('method', 'post');

        var mode      = $("<input type='hidden' name='mode' value='form'>");
        var userGroup = $("<input type='hidden' name='userGroup' value='"+<?php echo $_REQUEST['userGroup'];?>+"'>");
        var timeStr   = '<?php echo (isset($_REQUEST['timeS']))?$_REQUEST['timeS']:'';?>';
        var timeS     = $("<input type='hidden' name='timeS' value='"+timeStr+"'>");
        var year      = $("<input type='hidden' name='y' value='"+<?php echo $displayY?>+"'>");
        var month     = $("<input type='hidden' name='m' value='"+<?php echo $displayM?>+"'>");
        var day       = $("<input type='hidden' name='d' value='"+d+"'>");
        var visitId   = $("<input type='hidden' name='visitId' value='"+$('#sel_unit').val()+"'>");
        var time      = $("<input type='hidden' name='time' value='"+t+"'>");

        form.append(mode);
        form.append(userGroup);
        form.append(timeS);
        form.append(year);
        form.append(month);
        form.append(day);
        form.append(visitId);
        form.append(time);

        form.appendTo("body");
        form.css('display', 'none');
        form.submit();
    }

    function nextM(btn){
        $(btn).attr("disabled", true);
        $.ajax({ 
            type: "POST", 
            url: "rent_lib.php", 
            data: "act=doNexAndPrev&type=next"
        }).done(function( rs ) {
            var viewGet = '<?php echo isset($_REQUEST['view']) ? $_REQUEST['view'] : ""; ?>' ;
            if(viewGet == 'maintain'){
                location.href='rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&visitId='+$('#sel_unit').val()+"&view=maintain";
            }else{
                location.href='rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&visitId='+$('#sel_unit').val();
            }
        });
    }

    function prevM(btn){
        $(btn).attr("disabled", true);
        $.ajax({ 
            type: "POST", 
            url: "rent_lib.php", 
            data: "act=doNexAndPrev&type=prev"
        }).done(function( rs ) {
            var viewGet = '<?php echo isset($_REQUEST['view']) ? $_REQUEST['view'] : ""; ?>' ;
            if(viewGet == 'maintain'){
                location.href='rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&visitId='+$('#sel_unit').val()+"&view=maintain";
            }else{
                location.href='rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&visitId='+$('#sel_unit').val();
            }
        });
    }
    function chgUnit(obj){
        var viewGet = '<?php echo isset($_REQUEST['view']) ? $_REQUEST['view'] : ""; ?>' ;
        if(viewGet == 'maintain'){
            location.href="rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&act=chgUnit&visitId="+$('#sel_unit').val()+"&view=maintain";
        }else{
            location.href="rentPlace.php?userGroup=<?=getSafely('userGroup')?>&timeS=<?=getSafely('timeS')?>&act=chgUnit&visitId="+$('#sel_unit').val();
        }
    }
    function delBooking(){
        location.href="online_booking/login.php";
    }
    function forgot(){
        $('#divforgot').css("display", "block");
    }
    function search(){
        var form = $('#statefm');
        $.ajax({
            url: "rent_lib.php",
            type: 'POST',
            data: form.serialize(),
            dataType: 'json',
            success: function(d){
                console.log(d);
                $("#divforgot").hide();
                $("#forgotList").show();
                $("#forgotList").empty();
                $("#forgotList").append("<ul>");
                $.each(d, function(k,v){
                    $("#forgotList").append("<li>"+v['id']+' - '+v['title']+' - '+v['eventDate']+"</li>");
                });
                if(d.length <= 0) $("#forgotList").append("<li>查無資料</li>");
                $("#forgotList").append("<ul>");
                $("#forgotList").append('<button type="button" onclick="$(\'#forgotList\').hide()">關閉</button>');
            }
        });
    }
</script>
<!-- InstanceEndEditable -->
<link href="css/index.css" rel="stylesheet" type="text/css">
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<style>
    .gradient_bg {
        background: linear-gradient(180deg, rgba(196,154,149,1) 0%, rgba(244,197,191,1) 29%);
    }
</style>
</head>

<body>
    <?php if(!isset($_REQUEST['mode']) || $_REQUEST['mode'] != 'form'){ 
    ?>
        <div id="divforgot">
            <form id="statefm">
                <input type="hidden" name="act" value="forgot">
                聯絡人: <input type="text" name="contactman"><br><br>
                email: <input type="text" name="email"><br><br>
                <div style="text-align: center;">
                    <button type="button" onclick="search()">查詢</button>
                    <button type="button" onclick="$('#divforgot').hide()">關閉</button><br>
                </div>
            </form>
        </div>
        <div id="forgotList">
            <button type="button" onclick="$('#divforgot').hide()">關閉</button>
        </div>
    <?php } ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="gradient_bg">&nbsp;</td>
            <td width="1020" height="120" align="center" valign="middle" class="gradient_bg"><!--#fdf1f1--><a href="/"><img src="images/logo2.jpg" width="328" height="94" border="0"></a></td>
            <td bgcolor="#FDF1F1" class="gradient_bg">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#e16c7c">&nbsp;</td>
            <td width="1020" height="40" valign="middle" bgcolor="#E16C7C"><?php include('menu.php') ?></td>
            <td bgcolor="#E16C7C">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#f4c5bf">&nbsp;</td>
            <td width="1020">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="200" valign="top" bgcolor="#efefef">
                            <?php include('stickers.php') ?>
                        </td>
                        <td id="maintx" valign="top"><!-- InstanceBeginEditable name="content" -->
                            <table width="92%" border="0" align="center" class="mainTable">
                                <tr>
                                    <td height="80">
                                        <div class="pageTitleBG">
                                            <img src="images/off.png" align="absbottom" /> 場地借用申請
                                        </div>
                                        <?php 
                                            if($_REQUEST['userGroup'] == 2 && (!isset($_REQUEST['mode']) || $_REQUEST['mode'] != 'form')){
                                                echo "<button onclick='forgot()' style='background-color: #BDD6EE; font-size: 12px;height: 20px;cursor:pointer;'>忘記單號?</button>";
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <?php if(!isset($_REQUEST['mode']) || $_REQUEST['mode'] != 'form'){ ?>
                                <tr>
                                    <td>
                                        <table class='grid table'>
                                            <tr>
                                                <td colspan="7">
                                                    <table class='grid table'>
                                                        <tr>
                                                            <td colspan="7" class='tdTitle'>
                                                                <select name='sel_unit' id='sel_unit' onchange='chgUnit(this)'>
                                                                <?php
                                                                    foreach ($rentData as $key => $value) {
                                                                        if($visitId==$key){ $selected='selected'; }
                                                                        else{ $selected=''; }
                                                                        echo "<option value='$key' $selected>$value</option>";
                                                                    }
                                                                ?>
                                                                </select>
                                                                <?php
                                                                    $L=true;
                                                                    $R=true;
                                                                    if($this_month+intval($data['start_booking'])>=$m){
                                                                        if($this_month==$m){ $L=false; }
                                                                    }
                                                                    if($this_month+intval($data['start_booking'])<=$m){
                                                                        $R=false;
                                                                    }
                                                                ?>
                                                                <?php if($L || (isset($_REQUEST['view']) && ($_REQUEST['view'] == 'maintain'))){ ?>
                                                                    <input type="button" value='<' class='btn' onclick="prevM(this)"/>
                                                                <?php } ?>
                                                                <?=$displayY?>年<?=$displayM?>月
                                                                <?php  if($R || (isset($_REQUEST['view']) && ($_REQUEST['view'] == 'maintain'))){ ?>
                                                                    <input type="button" value='>' class="btn" onclick="nextM(this)" />
                                                                <?php } ?>
                                                                    <!-- <input type="button" value="取消預約" class='btn btn2' onclick='delBooking()'/> -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <?php for($list_week=0;$list_week<7;$list_week++){ 
                                                                if($list_week == 6 || $list_week == 0){
                                                                    echo "<td id='tdWeekEnd'>".$day_array[$list_week]."</td>";
                                                                }else{
                                                                    echo "<td id='tdWeek'>".$day_array[$list_week]."</td>"; //列出 日、一、二、三、四、五、六
                                                                } 
                                                            } ?>
                                                        </tr>
                                                        <?php
                                                            for($pcol=1;$pcol<=6;$pcol++){ //一個月最多有六週 (在月曆上顯示的)，最多印出六週（六列） 
                                                                if($day_counter>$how_many_days_this_month){ 
                                                                    //只要遞增的day_counter變數大於本月份的天數（例如一月是31天），就會跳出迴圈；若是天數在6週內結束，就不會全都印完（每一列） 
                                                                    break; 
                                                                }
                                                                echo "<tr>"; 
                                                                for($prow=0;$prow<7;$prow++){ //一個星期有七天，所以是七行 
                                                                    if($prow == 6 || $prow == 0){
                                                                        $weekEndClass = "style='background:#f4c5bf;padding:8px;'";
                                                                        $weekEndEvent = "onmouseover='doOverWE(this)' onmouseout='doOutWE(this)'";
                                                                    }else{
                                                                        $weekEndClass = "";
                                                                        $weekEndEvent = "onmouseover='doOver(this)' onmouseout='doOut(this)'";
                                                                    }
                                                                    if($day_counter>0 && $day_counter<=$how_many_days_this_month){
                                                                        if($bSettingTime){
                                                                            if(in_array($prow,$week_day)){
                                                                                $day=str_pad($day_counter,2,"0",STR_PAD_LEFT);
                                                                                if((($this_day+intval($data['notice_day'])) > $day_counter) 
                                                                                    && $this_year==$displayY 
                                                                                    && $this_month==$displayM 
                                                                                    && (!isset($_REQUEST['view']) 
                                                                                    || $_REQUEST['view'] != 'maintain')){
                                                                                    if($this_day > $day){
                                                                                        $str='<br><br><span class="tip">已過期</span>';
                                                                                        echo "<td class='tdNone2' $weekEndClass>".$day_counter.$str."</td>"; 
                                                                                    }else{  
                                                                                        $str='<br><br><span class="tip">超過可預約的時間</span>';
                                                                                        echo "<td class='tdNone2' $weekEndClass>".$day_counter.$str."</td>"; 
                                                                                    }
                                                                                }else{
                                                                                    $tmpY=str_pad($displayY,2,"0",STR_PAD_LEFT);
                                                                                    $tmpM=str_pad($displayM,2,"0",STR_PAD_LEFT);
                                                                                    $ck=false;
                                                                                    $str='';
                                                                                    $close = true;
                                                                                    foreach($weekDayData[$visitId][$prow] as $k=>$v){
                                                                                        $timeSet='';
                                                                                        if(isset($bookingData[$tmpY][$tmpM][$day][$k])){//有預約資料
                                                                                            $c=$bookingData[$tmpY][$tmpM][$day][$k]['count']; 
                                                                                            $timeSet=$bookingData[$tmpY][$tmpM][$day][$k]['st'].' ~ '.$bookingData[$tmpY][$tmpM][$day][$k]['et'];
                                                                                        }else{//尚未有人預約
                                                                                            $index=array_search($k,$timeData['id']);
                                                                                            $c=0;
                                                                                            $timeSet=$timeData['startTime'][$index].' ~ '.$timeData['endTime'][$index];
                                                                                        }
                                                                                        $nums=intval($weekDayData[$visitId][$prow][$k])*intval($data['visit_unit']);
                                                                                        if(($nums-$c)<=0){
                                                                                            $str.='<br><br><span class="tip3">'.$timeSet.'<br>已租借</span>';
                                                                                        }else{
                                                                                            $bClose=false;
                                                                                            foreach($closeY as $k1 => $v1){
                                                                                                if($tmpY!=$closeY[$k1]) continue;
                                                                                                if($tmpM!=$closeM[$k1]) continue;
                                                                                                if($day!=$closeD[$k1])  continue;
                                                                                                $bClose=true;
                                                                                                $bNotes = $closeN[$k1];
                                                                                                break;
                                                                                            }
                                                                                            if($bClose){
                                                                                                if($close){
                                                                                                    $str.= "<br><br><br>暫不開放<br><span style='color:red'>因：".$bNotes."</span>";
                                                                                                    $close = false;
                                                                                                }
                                                                                            }else{
                                                                                                $ck=true;
                                                                                                if(isset($_REQUEST['view']) && $_REQUEST['view'] == 'maintain'){
                                                                                                    $str.='<br><br><a style="cursor:pointer">'.$timeSet.'<br><span class="">尚可租借</span></a>';
                                                                                                }else{
                                                                                                    $str.='<br><br><a style="cursor:pointer" onclick="doBooking('."'".$day_counter."'".','."'".$timeSet."'".')">'.$timeSet.'<br><span class="">尚可租借</span></a>';
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    if($ck){
                                                                                        // echo "<td onmouseover='doOver(this)' onmouseout='doOut(this)' style='cursor:pointer' onclick='doBooking(".$day_counter.")'><span class='tip4'>".$day_counter."</span>".$str."</td>";
                                                                                        echo "<td $weekEndClass $weekEndEvent><span class='tip4'>".$day_counter."</span>".$str."</td>"; 

                                                                                    }else{
                                                                                        echo "<td $weekEndClass>".$day_counter.$str."</td>"; 
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $str='<br><br><span class="tip">未開放</span>';
                                                                                echo "<td class='tdNone' $weekEndClass>".$day_counter.$str."</td>"; 
                                                                            }
                                                                        }else{
                                                                            $str='<br><br><span class="tip">未開放</span>';
                                                                            echo "<td class='tdNone' $weekEndClass>".$day_counter.$str."</td>"; 
                                                                        }
                                                                    }else{ 
                                                                        echo "<td>　</td>"; 
                                                                    } 
                                                                    $day_counter++; 
                                                                } 
                                                                echo "</tr>"; 
                                                            } 
                                                        ?>
                                                        <tr><td colspan="7" style='text-align: left'><span class="tip3">註：需提前<?=intval($data["notice_day"]);?>天預約</span></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php }else{ ?>
                                <tr>
                                    <td>
                                        <form id="form1" action="rentplacedo.php" method="post" style="padding:0" onsubmit="return formvalid(this)">
                                            <table width="100%"  border="0" cellpadding="4" cellspacing="0" style="border:dashed; border-width:1px; padding:10px">
                                                <tr hidden>
                                                    <td>
                                                        <input type="hidden" name="userGroup" value="<?=getSafely('userGroup')?>">
                                                        <input type="hidden" name="places" value="<?=getSafely('visitId')?>">
                                                        <input type="hidden" name="act" value="append">
                                                        <?php 
                                                            if($_REQUEST['userGroup'] == 1) $isView = '1';
                                                            else $isView = '0';
                                                        ?>
                                                        <input type="hidden" name="isView" value="<?php echo $isView?>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="120" align="right">申請單位：</td>
                                                    <td colspan="3"><input name="applier" type="text" id="applier" size="60" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">負責人：</td>
                                                    <td><input type="text" name="leadman" id="leadman" /></td>
                                                    <td align="right">聯絡人：</td>
                                                    <td><input type="text" name="contactman" id="contactman" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">聯絡電話：</td>
                                                    <td><input type="text" name="tel" id="tel" /></td>
                                                    <td align="right" width="120">行動電話：</td>
                                                    <td><input type="text" name="mobile" id="mobile" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">傳真：</td>
                                                    <td colspan="3"><input type="text" name="fax" id="fax" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">地址：</td>
                                                    <td colspan="3"><input name="address" type="text" id="address" size="60" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">電子郵件：</td>
                                                    <td colspan="3"><input pattern="[^@\s]+@[^@\s]+\.[^@\s]+" name="email" type="email" id="email" size="60" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">活動名稱：</td>
                                                    <td colspan="3"><input name="title" type="text" id="title" size="60" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">預估參加人數：</td>
                                                    <td colspan="3"><input type="text" name="peoples" id="peoples" size="10" /> &nbsp;<span style="font-size:12px; color:#C30">人數上限: <?php echo $data['uper_limit'] ? $data['uper_limit']:0; ?> 人(只需輸入阿拉伯數字,勿寫國字)</span></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" valign="top">借用場地：</td>
                                                    <td><?= (isset($_REQUEST['visitId']))?$rentData[$_REQUEST['visitId']]:'' ?></td>
                                                </tr>
                                                <tr>
                                                    <td align="right">租用日期/時間：</td>
                                                    <td colspan="3">
                                                        <?php
                                                            if(isset($_REQUEST['y']) && isset($_REQUEST['m']) && isset($_REQUEST['d'])) {
                                                                echo $_REQUEST['y'].' 年 '.$_REQUEST['m'].' 月 '.$_REQUEST['d'].' 日 ';
                                                                echo '<input type="hidden" name="date" value="'.$_REQUEST['y'].'/'.$_REQUEST['m'].'/'.$_REQUEST['d'].'">';
                                                            }

                                                        ?>
                                                        
                                                        <select name="time"><?php echo (isset($option))?$option:'';?></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">注意事項：</td>
                                                    <td colspan="3" style="color: #C30">
                                                        全館禁止於各處牆面張貼資料，違者將按處酌收清潔費。
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">場地原附器材：</td>
                                                    <td colspan="3">
                                                        <?=$data['notes']?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">額外借用器材：($300/按項另計)</td>
                                                    <td>
                                                        <?php foreach ($itemData['1'] as $key => $value) {
                                                            echo '<input name="options[]" type="checkbox" id="options[]" value="'.$value.'">'.$value;
                                                        } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">訂購本園：</td>
                                                    <td>
                                                        <?php foreach ($itemData['2'] as $key => $value) {
                                                            echo '<input name="options[]" type="checkbox" id="options[]" value="'.$value.'">'.$value;
                                                        } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" valign="top">發票：</td>
                                                    <td colspan="3">
                                                        <input name="isInv" type="radio" id="isInv" value="2" checked onChange="checkInv()">二聯式發票
                                                        <input type="radio" name="isInv" id="isInv" value="3" onChange="checkInv()">三聯式發票
                                                        <div id="invinfo">
                                                            抬頭：<input type="text" name="invoiceTitle" id="invoiceTitle"> 
                                                            統一編號：<input type="text" name="invoiceNo" id="invoiceNo">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="3">
                                                        <input type="checkbox" name="check1" id="check1" onClick="checkitem()"> 申請單位已詳閱「<a href="index.php?incfn=inc_RentPlace.php" target="_blank">場地租借申請流程</a>」 <br>
                                                        <input type="checkbox" name="check2" id="check2" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場地租借辦法－105.5.26修-1.doc" target="_blank">場地租借辦法</a>」 <br>
                                                        <input type="checkbox" name="check3" id="check3" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場地租借共同遵守約定_107.01.01修.doc" target="_blank">場地租借共同遵守約定</a>」 <br>
                                                        <input type="checkbox" name="check4" id="check4" onClick="checkitem()"> 申請單位已詳閱「<a href="download/專業空間使用要點.doc" target="_blank">專業空間借用要點</a>」<br>
                                                        <input type="checkbox" name="check5" id="check5" onClick="checkitem()"> 申請單位已詳閱「<a href="download/台中市愛心家園停車場管理規定-107.01.01修.docx" target="_blank">愛心家園停車須知</a>」 <br>
                                                        <input type="checkbox" name="check6" id="check6" onClick="checkitem()"> 申請單位已詳閱「<a href="download/櫃台提前或延後開關大門申請單_107.01.01修.docx" target="_blank">提前或延後開關大門申請單</a>」 <br>
                                                        <input type="checkbox" name="check7" id="check7" onClick="checkitem()"> 申請單位已詳閱「<a href="download/場租聯絡人.jpg" target="_blank">場地租借聯絡人資訊</a>」 <br>
                                                        <input type="checkbox" name="check8" id="check8" onClick="checkitem()"> 申請單位已詳閱「<a href="download/水療池租借同意書108.6.21修.docx" target="_blank">水療池租借同意書</a>」 <br>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" valign="top">備註：</td>
                                                    <td colspan="3">
                                                        <?php if($_REQUEST['userGroup'] == 2){ 
                                                            echo "<span style='color: #C30'>如借用未借滿整個時段，請備註真正借用時間。</span>"; 
                                                            echo '<textarea name="UserNote" rows="4" cols="50">實際借用時間:　　　　　至　　　　　止</textarea>';
                                                        }else{
                                                            echo '<textarea name="UserNote" rows="4" cols="50"></textarea>';
                                                        }?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="3">
                                                        <input name="Submit" type="submit" disabled class="btnMedium" id="Submit" value="送出" />
                                                        <input name="button" type="button" class="btnMedium reset" id="button" value="重設" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td bgcolor="#F4C5BF">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#ed909a">&nbsp;</td>
            <td width="1020" bgcolor="#ED909A"><?php include 'inc_foot.htm' ?></td>
            <td bgcolor="#ED909A">&nbsp;</td>
        </tr>
    </table>
</body>
<!-- InstanceEnd -->
</html>