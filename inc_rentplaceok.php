<?php

  if(session_status() === PHP_SESSION_NONE) {
    session_start();
  }
	if(isset($_REQUEST['id'])) {
    if(isset($_SESSION['accessible_rentplaceIds']) && is_array($_SESSION['accessible_rentplaceIds'])) {
      if(!in_array($_REQUEST['id'], $_SESSION['accessible_rentplaceIds'])) {
        
                  // header("HTTP/1.0 404 Not Found");
                  echo "Page Not Found.\n";
                  die();
      }
    }
		// $sql = "select a.*, b.title as spaceName from rentplace a join space b ON(a.places = b.id) where a.id=$_REQUEST[id]";
		// $rs = db_query($sql);
		// $r = db_fetch_array($rs);

    $query = "select a.*, b.title as spaceName "
            ."from rentplace a "
            ."join space b "
            ."ON(a.places = b.id) "
            ."where a.id = :id ";
    $stmt = $db->prepare($query);
    $stmt->execute(array('id' => $_REQUEST['id']));
    $r = $stmt->fetch();
	}
?>
<table width="90%" border="0" align="center" class="mainTable">
  <tr>
    <td height="80"><div class="pageTitleBG"><img src="images/off.png" align="absbottom" /> 場地借用申請</div></td>
  </tr>
  <tr>
	<td class="indent">
		<p style="font-weight:bold; font-size:18px;color:red">
			借用完成後，如需再次借用其他時段或不繼續借用，請點選申請表下方之按鈕，請勿使用上一頁防止表單錯誤。
		</p>
	</td>
  </tr>
  <tr>	
    <td class="indent">
      <p style="font-weight:bold; font-size:18px">
        申請表已經發送完成。<span style="color: red;">【目前僅為預約申請】</span>，須待本園審核通過，<br>
        3～5日內將發送E-mail回覆確認後，始為完成借用申請。<br>
        <span style="color:red">＊請務必確認E-mail是否填寫正確並留意信件訊息。</span><br><br>
        附註：場地使用用途僅能作為研習課程使用，若辦理之活動非本園核定之<br>研習內容或與申請使用用途不同時，本園將停止活動辦理或取消申請。<br>
        附註：<span style="color:red">申請單號請確實牢記，用於取消或問題查詢。</span>
      </p>
    </td>
  </tr>
  <tr>
    <td><p class="pageTitle"><img src="images/bullg.gif" width="18" height="18" /> 線上申請</p></td>
  </tr>
  <tr>
  <td>

  <table width="600" border="0" cellpadding="8" cellspacing="0" style="border:dashed; border-width:1px; padding:10px">
  <tr>
    <th colspan="4">愛心家園 場地借用申請表</th>
    </tr>
  <tr>
    <td align="right">申請單號：</td>
    <td colspan="3"><?=$r["id"]?></td>
    </tr>  
  <tr>
    <td align="right">申請單位：</td>
    <td colspan="3"><?=$r["applier"]?></td>
    </tr>
  <tr>
    <td align="right">負責人：</td>
    <td><?=$r["leadman"]?></td>
    <td>聯絡人：</td>
    <td><?=$r["contactman"]?></td>
  </tr>
  <tr>
    <td align="right">聯絡電話：</td>
    <td><?=$r["tel"]?></td>
    <td>行動電話：</td>
    <td><?=$r["mobile"]?></td>
    </tr>
  <tr>
    <td align="right">傳真：</td>
    <td><?=$r["fax"]?></td>
    <td>電子郵件：</td>
    <td><?=$r["email"]?></td>
  </tr>
  <tr>
    <td align="right">地址：</td>
    <td colspan="3"><?=$r["address"]?></td>
    </tr>
  <tr>
    <td align="right">活動名稱：</td>
    <td colspan="3"><?=$r["title"]?></td>
    </tr>
  <tr>
    <td align="right">租用日期/時間：</td>
    <td colspan="3"><?=$r["bDate"]?> ~ <?=$r["eDate"]?></td>
  </tr>
  <tr>
    <td align="right">預估參加人數：</td>
    <td colspan="3"><?=$r["peoples"]?></td>
    </tr>
  <tr>
    <td align="right">借用場地：</td>
    <td colspan="3"><?=$r["spaceName"]?></td>
  </tr>
  <tr>
    <td align="right">加借(購)明細：</td>
    <td colspan="3"><?=$r["options"]?></td>
  </tr>
  
   <tr>
    <td align="right" valign="top">發票：</td>
    <td colspan="3">
		<?php  switch($r["isInv"]) {
			case 2: echo "二聯式發票"; break;
			case 3: echo "三聯式發票<br>抬頭：$r[invoiceTitle] &nbsp;統一編號：$r[invoiceNo]";
		}	?>
    </td>
  </tr>
    <tr>
      <td align="right">備註：</td>
      <td colspan="3"><?=$r["UserNote"]?></td>
    </tr>
  </table>
  <tr>
    <td>
      <button onclick="again()">再次借用</button>
      <button onclick="goHome()">回主頁並登出</button>
    </td>
  </tr>
</td></tr>
</table>
<p>&nbsp;</p>


<script type="text/javascript">
	window.history.pushState(null, null, "#");

	window.addEventListener("hashchange", e=> {
		console.log('Hash 更改後觸發');
	});

	window.addEventListener("popstate", e=> {
		window.location.replace('https://www.lovehome.org.tw/');
	});
  function goHome(){
    $.ajax({
        url: "rent_lib.php",
        type: 'POST',
        data: { act : 'goHome'},
        dataType: 'json',
        success: function(d){
          window.location.href='index.php?incfn=inc_RentPlace.php';
        }
    });
  }
  function again(){
    window.location.href = "rentPlace.php?userGroup=<?php echo $r['userGroup']?>&timeS=<?php echo $_SESSION['timeS']?>&visitId=<?php echo $r['places']?>";
  }
</script>