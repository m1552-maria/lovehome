// JavaScript Document
var id2proc = { 
		depID:'../department/query_id.php',
		jobID:'../jobs/query_id.php'
	};

function showDialog(aObj) {
	var param = $(aObj).prev(); 
	var procP = id2proc[param.attr('id')];
	var rzt = window.showModalDialog(procP,param.val(),"dialogWidth:300px; dialogHeight:450px; center:yes; status:no");
	param.val(rzt[0]);
	$(aObj).next().text(rzt[1]);
}

$(function() {
	$(".queryID").after("<img src='/scripts/form_images/search.png' class='queryID_img' onclick='showDialog(this)'/><span class='formTxS'></span>");

	$('.queryDate').datepicker(	{showOn: 'button', buttonImage: '/images/calendar.jpg', buttonImageOnly: true});
	$('.queryDate').datepicker('option', 'duration', '');
	$('.queryDate').datepicker($.datepicker.regional['zh-TW']);
	
  $("select[name='county']").change(function(){
		var city = $(this).next().val();	//鄉鎮市要緊接縣市
		var cd2 = this.options[this.selectedIndex].value;
		$.getJSON('/getptycd3.php',{'cd2':cd2},function(data){
			var str = '';
			$.each(data,function(idx,itm){ if(itm==city) str += '<option selected>'+itm+'</option>'; else str += '<option>'+itm+'</option>'; });
			$("select[name='city']").html(str);
		});
	});
	$("select[name='county']").change();
});

