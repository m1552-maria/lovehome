<?php
    include_once('db_config.php');

    $dsn = 'mysql:host='.$DBHOST.';dbname='.$DBNAME;
    $db = new PDO($dsn, $DBUSER, $DBPASS);
    if($db) {
        $db->exec("set names utf8mb4");

    }

?>