<?php
  include_once('connect_db.inc.php');
    $query = "select * from rentPlaceFiles where status = 1";
    // $rs = db_query($sql);
    $dataList = array();
    // while($r=db_fetch_array($rs)) {
    //     $dataList[$r['id']] = $r;
    // }
    $stmt = $db->prepare($query);
    $stmt->execute();
    while($r = $stmt->fetch()) {
      $dataList[$r['id']] = $r;
    } 
?>
<style>
#preview { 
	display: none; 
	text-align:center; vertical-align:middle;
	position:fixed; left:0; top:0;
	width:100%; height:100%;
	background-color:rgba(32,32,32,0.75);
	/* filter:alpha(Opacity=75, Style=0); -moz-opacity:0.75;	opacity: 0.75; */
}
.swipe { 
	top: 100px;
	left:50%;	margin-left: -400px; border-style:solid; border-width:20px; border-color:#ffffff;
	width: 810px;
    position:absolute;
    z-index:1;
    -webkit-perspective: 1000;
    -webkit-backface-visibility: hidden;
}
.swipe ul { margin:0; padding:0; border:0; outline:0; }
.swipe li div, .swipe div div div {
    margin:0 10px;
    padding:0px;
    background:#1db1ff;
    font-weight:bold;
    color:#fff;
    font-size:20px;
    text-align:center;
}
.swipepic{ width:800px; border:0 }
.imgBtn { cursor:pointer }
#closeBtn{ font-family:Verdana, Geneva, sans-serif; font-size:24px; font-weight:bold; color:#CCCCCC; float:left; padding: 10px; }
#div1, #div2{
    position:absolute;
    left: 50%; top:50%;
    background-color:#ffa4b0;
    padding: 20px;
    display:none;
    margin-left: -125px;
    margin-top: -50px;
}
</style>
<script src="Scripts/swipe.js"></script>
<script>
//$(function(){ document.body.style.overflow = 'hidden'; });
  function showalbum(dirnm) {
  	var slideDir='data/space/'+dirnm+'/';
  	//document.body.style.overflow = 'hidden';
  	var url = 'getpiclist.php';
  	$.get(url,{'dir':slideDir},function(data){ 
  		if(data.indexOf('Error')>=0) {
  			alert(data);
  		} else {
  			$('#preview').show();
  			var alst = eval(data); 
  			var fp = slideDir+alst[0];
  			var htmltx = '<ul style="float:left"><li><img src="'+fp+'" class="swipepic"/></li>';
  			for(i=1; i<alst.length; i++) {
  				fp = slideDir+alst[i];
  				htmltx += '<li style="display:none"><img src="'+fp+'" class="swipepic"/></li>';
  			}
  			htmltx += '</ul>'
  						 + '<div style="position:absolute; vertical-align:middle; top:40%; left: 10px; cursor:pointer" onclick="slide.prev(); return false;"><img src="Scripts/images/left.png" title="前一頁"/></div>'
  						 + '<div style="position:absolute; vertical-align:middle; top:40%; right:10px; cursor:pointer" onclick="slide.next(); return false;"><img src="Scripts/images/right.png" title="後一頁"/></div>'
  						 + '</div>';
  			var elm = document.getElementById('slider');	elm.innerHTML = htmltx;
  			var obj = document.getElementById('slider');	slide = new Swipe(obj);
  		}
  	});
  }
  function closealbum() {
  	$('#preview').hide();
  }
  function empLogin(){
    $('#div1').css("display", "block");
  }
  function login(){
    var form = $('#statefm');
    $.ajax({
        url: "rentApi.php",
        type: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function(d){
          if(Number.isInteger(d)) {
            window.location.href='rentPlace.php?userGroup=2&timeS='+d;
          }
          else {
            alert(d);
          }
          
        }
    });
  }
  function delBooking(){
    $('#div2').css("display", "block");
  }
  function login2(){
    var form = $('#delBookingfm');
    $.ajax({
        url: "rent_lib.php",
        type: 'POST',
        data: form.serialize(),
        dataType: 'json',
        success: function(d){
            switch(d){
                case 'error':
                    alert("資料驗證錯誤。");
                    break;
                case 'overTime':
                    alert("超過可取消時間,請電洽承辦人辦理。");
                    break;
                case 'success':
                    alert("取消成功。");
                    location.reload();
                    break;
            }
        }
    });
  }
</script>
<div id="preview">
	<div id="closeBtn" onclick="closealbum()">[ X ]</div>
	<div id="slider" class="swipe"></div>
</div>
<div id="div1">
  <form id="statefm">
    帳號: <input type="text" name="empID"><br><br>
    密碼: <input type="password" name="password"><br><br>
    <div style="text-align: center;">
      <button type="button" onclick="login()">登入</button>
      <button type="button" onclick="$('#div1').hide()">關閉</button><br>
    </div>
  </form>
</div>
<div id="div2">
  <form id="delBookingfm">
    申 請 單 號 : <input type="text" name="rentPlaceID"><br><br>
    聯絡人姓名 : <input type="text" name="contact"><br><br>
    聯絡人Email: <input type="text" name="email"><br><br>  
    <input type="hidden" name="act" value="delBooking">
    <div style="text-align: center;">
      <button type="button" onclick="login2()">確定</button>
      <button type="button" onclick="$('#div2').hide()">取消</button><br>
    </div>
  </form>
</div>
<table width="90%" align="center" class="mainTable">
	<tr><td height="70">
    <div class="pageTitleBG"><img src="images/off.png" align="absbottom" />
      場地租借
      <button onclick="document.location.href='rentPlace.php?userGroup=1'" style="background-color: #92D050; font-size: 16px;height: 24px;cursor:pointer;">訪客專區</button>　
      <button onclick="empLogin()" style="background-color: #BDD6EE; font-size: 16px;height: 24px;cursor:pointer;">員工專區</button>　
      <button onclick="delBooking()" style="background-color: #FF7575; font-size: 16px;height: 24px;cursor:pointer;">取消預約</button>
    </div>
  </td></tr>
  <tr>
    <td>
      <ul>
<?php foreach($dataList as $k=>$v) { ?>
        <li><a href="download/<?php echo $v['file'];?>" target="_blank"><?php echo $v['title'];?></a></li>
<?php } ?>
      </ul>
    </td>
  </tr>
  <tr><td class="pageTitle"><img src="images/bullr.gif" align="absmiddle" /> 場地租借流程圖</td></tr>
	<tr><td align="center"><p><img src="images/file1/pic1.jpg"></p><br /></td></tr>
  <tr><td class="pageTitle"><img src="images/bullb.gif" align="absmiddle" /> 各空間簡介及場地維護費</td></tr>

  <?php 
    function getTable(){
      global $db, $temp;
      $query = "select T2.title as floor , T1.title, T1.peopleNum, T1.cost, T1.notes, T1.id, T1.size ,T2.isReady "
            ."from space as T1,spaceclass as T2 "
            ."where T1.sId = T2.id "
            ."AND (T1.userGroup = '0' OR T1.userGroup = '1') "
            ."order by T1.sId asc";
      $stmt = $db->prepare($query);
      $stmt->execute();
      // $rs = db_query($sql);
      // $count = db_num_rows($rs);
      $count = 0;

      $tableList = array();
      $albumList = array();
      $tableStr  = array();
      while($r = $stmt->fetch()) {
        $tableList[] = $r;
        $albumList[] = $r['id'];
        $count++;
      }
        $tableStr[] = '<table border="1" cellspacing="0" cellpadding="2" width="100%" id="space">';
        $tableStr[] ='<tr>';
        $tableStr[] ='<th width="64"  valign="top" bgcolor="#F79646">樓層</th><th width="123" valign="top" bgcolor="#F79646">空間名稱</th><th width="104" valign="top" bgcolor="#F79646">可容納人數</th><th width="123" valign="top" bgcolor="#F79646">每場次維護費</th><th width="122" valign="top" bgcolor="#F79646">空間設備</th>';
        $tableStr[] ='</tr>';
        // var_dump($tableList);
        foreach($tableList as $key => $value){
          if(!isset($temp[$value['floor']])) $temp[$value['floor']] = 0;
          $temp[$value['floor']] +=1;
        }

        for($i=0; $i < $count; $i++){
          if($tableList[$i]['isReady'] == 1){
              $tableStr[] ='<tr>';
              if($i == 0 || ($tableList[$i-1]['floor'] != $tableList[$i]['floor'])){
                $tableStr[] = '<td width="64" align="center" valign="top" bgcolor="#F79646" rowspan="'.$temp[$tableList[$i]['floor'] ].'">'.$tableList[$i]['floor'].'</td>';    //樓層
              }
              $tableStr[] = '<td width="123" valign="top" bgcolor="#FBCAA2">'.$tableList[$i]['title'];                                                                            //空間名稱
              $tableStr[] = '<img src="images/eye.png" width="32" height="32" title="檢視照片" align="absmiddle" onclick="showalbum(' ."'$albumList[$i]'". ')" class="imgBtn"/><p>(約'.$tableList[$i]['size'].'坪)</p> <a href="/rentPlace.php?userGroup=1&act=chgUnit&visitId='.$tableList[$i]['id'].'">租借</a> </td>';

              $tableStr[] = '<td width="104" valign="top" bgcolor="#FBCAA2">'.$tableList[$i]['peopleNum'].'人'.'</td>';                                                           //可容納人數
              $tableStr[] = '<td width="123" valign="top" bgcolor="#FBCAA2">'.$tableList[$i]['cost'].'元'.'</td>';                                                                //維護費用
              $tableStr[] = '<td width="122" valign="top" bgcolor="#FBCAA2">'.$tableList[$i]['notes'].'</td>';                                                                    //空間設備
              $tableStr[] ='</tr>';
          }
        }
          $tableStr[] = '</table>';
          echo implode("",$tableStr);
     }
  ?>

  <tr><td>
    <?php
      echo getTable();
    ?>
  </td></tr>
</table>
<br>



