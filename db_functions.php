<?php

function db_data_seek($rs,$pos) {
    global $DB_Type;
    switch($DB_Type) {	
        case 0: return mysql_data_seek($rs,$pos);
        case 1: return mssql_data_seek($rs,$pos);
        case 3: return false;	//odbc 不支援 data_seek
    }		
    return false;
}

function db_num_rows($rs) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: return mysql_num_rows($rs);
        case 1: return mssql_num_rows($rs); 
        case 3: return odbc_num_rows($rs);		
    }	
    return 0;
}

function db_field_name($rs,$idx) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: return mysql_field_name($rs,$idx); break;
        case 1: return mssql_field_name($rs,$idx); break; 
        case 3: return odbc_field_name($rs,$idx);
    }		
}

function db_num_fields($rs) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: return mysql_num_fields($rs); break;
        case 1: return mssql_num_fields($rs); break; 
        case 3: return odbc_num_fields($rs);
    }	
}

function db_close($conn) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: mysql_close($conn); break;
        case 1: mssql_close($conn); break; 
        case 3: odbc_close($conn);
    }
}

function db_query($sql,$con=NULL) {
    global $DB_Type;
    global $conn;
    if(!$con) $con=$conn;
    switch($DB_Type) {
        case 0: return mysql_query($sql,$con);
        case 1: return mssql_query($sql,$con); 
        case 3: return odbc_exec($con,$sql);
    }
    return false;
}
    
function db_eof($rs) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: return mysql_num_rows($rs)==0;
        case 1: return mssql_num_rows($rs)==0; 
        case 3: return odbc_num_rows($rs)==0;
    }
    return false;		
}

function db_fetch_array($rs) {
    global $DB_Type;
    switch($DB_Type) {
        case 0: return mysql_fetch_array($rs);
        case 1: return mssql_fetch_array($rs); 
        case 3: return odbc_fetch_array($rs);
    }
    return false;	
}

function db_free_result($rs) {
    global $DB_Type;
    switch($DB_Type) {	
        case 1: return mysql_free_result($rs);
        case 3: return mssql_free_result($rs);
        case 3: return odbc_free_result($rs);
    }
    return false;
}		

?>