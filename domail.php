<?php
/* Migrate to PHPMailer 6.5 and Gmail SMTP by Ivan 2021/11/08
 * reference: https://github.com/PHPMailer/PHPMailer/blob/master/examples/gmail.phps
 *
 * PHP SMTP 寄信模組 by: michael rou from 2010/01/17
 * required class.phpmailer.php, class.smtp.php
 * surport 2 mode : Function Call & Form Post
 * 
 */

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
//Alias the League Google OAuth2 provider class
use League\OAuth2\Client\Provider\Google;

//Load Composer's autoloader
require 'vendor/autoload.php';

if(isset($_REQUEST['POSTMAIL'])) {	//:for表單大量寄發模式
	$tomail = explode(';', $_REQUEST['tomail']);	//收件人
	foreach($tomail as $v) if($v) $MailInfo["mailTo"][]=array("MailName" => $v,	"address" => $v); 
	$MailInfo["Subject"] = $_REQUEST['subject'];				//主旨
	$body = file_get_contents($_REQUEST['body']);				//內文樣板檔
	$MailInfo["Body"] = $body;								
	$msg = SMTP_Send_Mail($MailInfo); 	//寄信		
}

function send_mail($to, $subject, $body) {	//::for函數呼叫模式,可單人或多人
	global $MailInfo,$CustomHeader;
	$tomail = explode(';', $to);
	foreach($tomail as $v) if($v) $MailInfo["mailTo"][]=array("MailName"=>$v,	"address"=>$v); 
	$MailInfo["Subject"] = $subject;									 	//主旨	
	$MailInfo["Body"] = $body;													//內文
	return SMTP_Send_Mail($MailInfo); 	//寄信	
}

function SMTP_Send_Mail($MailInfo=array()){	// 預設寄信程序
	//Create a new PHPMailer instance
	$mail = new PHPMailer();

	//Tell PHPMailer to use SMTP
	$mail->IsSMTP();

	//Enable SMTP debugging
	//SMTP::DEBUG_OFF = off (for production use)
	//SMTP::DEBUG_CLIENT = client messages
	//SMTP::DEBUG_SERVER = client and server messages
	$mail->SMTPDebug = SMTP::DEBUG_OFF;

	//Set the hostname of the mail server
	$mail->Host = 'smtp.gmail.com';
	//Use `$mail->Host = gethostbyname('smtp.gmail.com');`
	//if your network does not support SMTP over IPv6,
	//though this may cause issues with TLS

	//Set the SMTP port number:
	// - 465 for SMTP with implicit TLS, a.k.a. RFC8314 SMTPS or
	// - 587 for SMTP+STARTTLS
	$mail->Port = 465;
	
	//Set the encryption mechanism to use:
	// - SMTPS (implicit TLS on port 465) or
	// - STARTTLS (explicit TLS on port 587)
	$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;

	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;

	//Set AuthType to use XOAUTH2
	$mail->AuthType = 'XOAUTH2';

	//Fill in authentication details here
	//Either the gmail account owner, or the user that gave consent
	$email = 'no_reply@maria.org.tw';
	$clientId = '475919312347-k28smdtg8g2ssulc5jue0kq6bu0c4knm.apps.googleusercontent.com';
	$clientSecret = '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';

	//Obtained by configuring and running get_oauth_token.php
	//after setting up an app in Google Developer Console.
	$refreshToken = '1//0ea8WSOyTxE5ECgYIARAAGA4SNwF-L9Ir7UgV4EOeLYkiD4TDkoaETCwHiTsahYarKJORbH3og-KKdfwlpKXufCcx6nWR37xsJ1A';

	//Create a new OAuth2 provider instance
	$provider = new Google(
    	[
        	'clientId' => $clientId,
        	'clientSecret' => $clientSecret,
    	]
	);

	//Pass the OAuth provider instance to PHPMailer
	$mail->setOAuth(
    	new OAuth(
        	[
            	'provider' => $provider,
            	'clientId' => $clientId,
            	'clientSecret' => $clientSecret,
            	'refreshToken' => $refreshToken,
            	'userName' => $email,
        	]
    	)
	);

	//Set who the message is to be sent from
	//Note that with gmail you can only use your account address (same as `Username`)
	//or predefined aliases that you have configured within your account.
	//Do not use user-submitted addresses in here
	$mail->setFrom('no_reply@maria.org.tw', '愛心家園網站通知');
	
	//執行 $mail->AddAddress() 加入收件者，可以多個收件者
	if( isset( $MailInfo["mailTo"] ) && is_array( $MailInfo["mailTo"] ) && count( $MailInfo["mailTo"] ) > 0 ) {
		foreach( $MailInfo["mailTo"] AS $vv ) $mail->AddAddress( $vv["address"], $vv["MailName"] );
	} else return "收件人資料錯誤!";
	//執行 $mail->AddCC() 加入副本收件者，可以多個收件者
	if( isset( $MailInfo["cc"] ) && is_array( $MailInfo["cc"] ) && count( $MailInfo["cc"] ) > 0 ) {
		foreach( $MailInfo["cc"] AS $vv ) $mail->AddCC( $vv["address"], $vv["MailName"] );
	}
	//執行 $mail->AddBCC() 加入密件副本收件者，可以多個收件者
	if( isset( $MailInfo["bcc"] ) && is_array( $MailInfo["bcc"] ) && count( $MailInfo["bcc"] ) > 0 ) {
		foreach( $MailInfo["bcc"] AS $vv ) $mail->AddBCC( $vv["address"], $vv["MailName"] );
	}

	$mail->WordWrap = 100; // set word wrap
	$mail->IsHTML( true ); // send as HTML
	$mail->Subject =  "=?UTF-8?B?".base64_encode( $MailInfo["Subject"] )."?=";
	$mail->CharSet = "UTF-8";

	if( isset( $MailInfo["Body"] ) && $MailInfo["Body"] != '' ) {
		$mail->Body = $MailInfo["Body"]; // HTML 格式
	} else {
		return "信件內容不可以是空字串!";
	}
	if( isset( $MailInfo["AltBody"] ) && $MailInfo["AltBody"] != '' ) {
		$mail->AltBody = $MailInfo["AltBody"]; // text 格式
	} else {
		$mail->AltBody = $MailInfo["Body"]; // text 格式
	}
	if(!$mail->Send()) {
		return "Message was not sent! Mailer Error: " . $mail->ErrorInfo;
	} else {
		return "Mail Send success";
	}
}