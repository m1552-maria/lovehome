<?php
session_start();
// include "config.php";
include_once('connect_db.inc.php');
$act = $_REQUEST['act'];

switch($act){
	case 'goHome':
		session_unset();
		session_destroy();
		break;
	case 'forgot':
		$today = date('Y-m-d 00:00:00');
		$query = "SELECT id, title, DATE_FORMAT(bDate, '%Y/%m/%d') as eventDate"
				."FROM rentplace "
				."WHERE contactman = :contactman"
				."AND email = :email"
				."AND bDate > :today";
		$stmt = $db->prepare($query);
		$stmt->execute(array(
			'contactman' => $_REQUEST['contactman'],
			'email' => $_REQUEST['email'],
			'today' => $today
		));
		$re = array();
		while($r = $stmt->fetch()){
			$re[] = $r;
		}
		echo json_encode($re);
		break;
	case "ckIsFull":
		$date=$_REQUEST['date'];
		$places=$_REQUEST['places'];
		echo ckIsFull($date,$places);
		break;
	case "login":
		$contact = $_REQUEST['contact'];
		$email   = $_REQUEST['email'];
		login($contact, $email);
		break;
	case "delBooking":
		$id      = $_REQUEST['rentPlaceID'];
		$contact = $_REQUEST['contact'];
		$email   = $_REQUEST['email'];

		// $sql   = "SELECT count(1) as _count from rentplace where id = '".$id."' AND contactman = '".$contact."' AND email = '".$email."'";
		// $rs    = db_query($sql);
		// $r     = db_fetch_array($rs);

		$query = "SELECT * "
				."From rentplace "
				."where id = :id "
				."And contactman = :contact "
				."And email = :email ";
		$stmt = $db->prepare($query);
		$stmt->execute(array(
			'id' => $id,
			'contact' => $contact,
			'email' => $email
		));

		// if($r['_count'] < 1){
		// 	echo json_encode("error");
		// }else{
		if($stmt->fetch()) {
			// $sql   = "SELECT b.early_cancel, a.rdate from rentplace a left join space b ON(a.places = b.id) where a.id = '".$id."'";
			// $rs    = db_query($sql);
			// $r     = db_fetch_array($rs);
			$query = "SELECT b.early_cancel, a.rdate "
					."from rentplace a "
					."left join space b "
					."	ON(a.places = b.id) "
					."where a.id = :id ";
			$stmt = $db->prepare($query);
			$stmt->execute(array('id' => $id));
			$r = $stmt->fetch();
			$today = date("Y-m-d H:i:s");

			$early_cancel = "+".$r['early_cancel']." day";
			$eDay  = date("Y-m-d", strtotime($early_cancel, strtotime($r['rdate'])));
			
			// $begint = strtotime(date("Y-m-d",$r['rdate']));
			$begint = strtotime($r['rdate']);		//modified by Hao-Tung 2022-08-28 strtotime要的是日期字串, date()第二個參數要的是int (timestamp)
			$endt   = strtotime($eDay)+86400;
			$currt  = strtotime($today);
			if($currt>= $begint && $currt <= $endt ){

				include_once 'domail.php';
				// $sql     = "select a.*, b.title as placeName from rentplace a left join space b ON(a.places = b.id) where where a.id='".$id."'";
				// $rs      = db_query($sql);
				// $r    	 = db_fetch_array($rs);
				$query = "select a.*, b.title as placeName "
						."from rentplace a "
						."left join space b "
						."	ON(a.places = b.id) "
						."where where a.id = :id ";
				$stmt = $db->prepare($query);
				$stmt->execute(array('id' => $id));
				$r = $stmt->fetch();

				$email   = "m745@maria.org.tw";
				$fn 	 = 'mail/rentPlaceCancelMail.html'; 
				$content = file_get_contents($fn);
				$content = str_replace("{id}",   	  $id,             $content);
				$content = str_replace("{applier}",   $r["applier"],   $content);
				$content = str_replace("{leadman}",   $r["leadman"],   $content);
				$content = str_replace("{mobile}",    $r["mobile"],    $content);
				$content = str_replace("{email}",     $r["email"],     $content);
				$content = str_replace("{title}",     $r["title"],     $content);
				$content = str_replace("{placeName}", $r["placeName"], $content);
				$content = str_replace("{bDate}",     $r["bDate"], 	   $content);
				$content = str_replace("{eDate}",     $r["eDate"], 	   $content);

				$subtx   = '場地取消留存通知';
				$rzt     = send_mail($email, $subtx, $content);
				
				// $sql = "update rentplace set status = '3' where id='".$id."'";
				// db_query($sql);
				$query = "update rentplace "
						."Set status = '3' "
						."Where id = :id ";
				$stmt = $db->prepare($query);
				$stmt->execute(array('id' => $id));
				echo json_encode("success");
			}else{
				echo json_encode("overTime");
			}
		}
		else {
			echo json_encode("error");
		}
		break;
	case "doNexAndPrev":
		$type=$_REQUEST['type'];
		doNexAndPrev($type);
		break;
}

function doNexAndPrev($type){
	if(isset($_SESSION['sched_m'])){//處理月份
		$m = intval($_SESSION['sched_m']);
		if($type=="next"){	
			$m+=1;
		}else{
			$m-=1;
		}
		$_SESSION['sched_m'] = $m;
	}
}
function ckIsFull($d, $places){
	$date=explode(" ",$d);
	$date1=explode("/",$date[0]);
	$date2=explode(":",$date[1]);
	$visit_wd=date("w",mktime(0,0,0,$date1[1],$date1[2],$date1[0])); //參訪當天是星期幾
	$sql="select * ";
	$sql.="from space as s ,rentplace as b ";
	$sql.="where s.id=b.places ";
	$sql.="and s.id='".$places."' ";
	$sql.="and MONTH(b.visitTime)= ".$date1[1]." ";
	//echo $sql."\n";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$bookRS = $stmt->fetch();
	$wkAry=array();
	if($rs){
		while($r=db_fetch_array($rs)){
			$full=explode(" ",$r['visitTime']);
			$vd=explode("-",$full[0]);
			$vt=explode(":",$full[1]);
			$wd=date("w",mktime(0,0,0,$vd[1],$vd[2],$vd[0])); //參訪當天是星期幾
			if(!in_array($wd,$wkAry)){	array_push($wkAry,$wd);	}
		}
	}
	$sql="select * ";
	$sql.="from space ";
	$sql.="where id=".$places;
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$r = $stmt->fetch();
	$c=$r['visit_unit'];
	//echo 'c:'.$c."<br>";
	//::各單位開放時間
	$sql="select * from space_detail ";
	$sql.="where places=".$places." ";
	$sql.="and week_day in (".join(",",$wkAry).") ";
	$sql.="order by places,week_day ";
	//echo $sql."<br>";
	$stmt = $db->prepare($sql);
	$stmt->execute();
	$timeData=array();
	if($rs){
		while($r=$stmt->fetch()){	
			if(!is_array($timeData['id'])){	$timeData['id']=array();}
			if(!is_array($timeData['places'])){	$timeData['places']=array();}
			if(!is_array($timeData['startTime'])){	$timeData['startTime']=array();}
			if(!is_array($timeData['endTime'])){	$timeData['endTime']=array();}
			if(!is_array($timeData['week_day'])){	$timeData['week_day']=array();}
			array_push($timeData['id'],$r['id']);
			array_push($timeData['places'],$r['places']);
			array_push($timeData['startTime'],$r['startTime']);
			array_push($timeData['endTime'],$r['endTime']);
			array_push($timeData['week_day'],$r['week_day']);
		}
	}
	
	//::統計目前預約記錄，計算餘額
	$bookingData=array();
	$TID=0;
	if($bookRS){
		while($r=db_fetch_array($bookRS)){
			$full=explode(" ",$r['visitTime']);
			$vd=explode("-",$full[0]);
			$vt=explode(":",$full[1]);
			$wd=date("w",mktime(0,0,0,$vd[1],$vd[2],$vd[0])); //參訪當天是星期幾 
			$index=0;
			foreach($timeData as $k=>$v ){//判斷預約時間落在什麼時段
				foreach($v as $k1=>$v1){
					if($k=="week_day" && $wd==$v1){
						$tWeekDay=$v1;
						$tId=$timeData['id'][$k1];
						$tSt=explode(":",$timeData['startTime'][$k1]);
						$tEt=explode(":",$timeData['endTime'][$k1]);
						$sH=intval($tSt[0]);	$sM=intval($tSt[1]);	$eH=intval($tEt[0]);	$eM=intval($tEt[1]);
						if(!is_array($bookingData[$vd[0]])){	$bookingData[$vd[0]]=array();	}
						if(!is_array($bookingData[$vd[0]][$vd[1]])){	$bookingData[$vd[0]][$vd[1]]=array();	}
						if(!is_array($bookingData[$vd[0]][$vd[1]][$vd[2]])){	$bookingData[$vd[0]][$vd[1]][$vd[2]]=array();	}
						$bool=false;
						if(intval($vt[0])>$sH && intval($vt[0]<$eH)){
							$bool=true;
						}else if((intval($vt[0])==$sH && intval($vt[1])>=$sM) || (intval($vt[0])==$eH && intval($vt[1])<=$eM)){
							$bool=true;
						}
						if($bool){
							if(!$bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]){	$bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]=0;}
							$bookingData[$vd[0]][$vd[1]][$vd[2]][$tId]=intval($bookingData[$vd[0]][$vd[1]][$vd[2]][$tId])+1;
						}
						if($visit_wd==$v1){
							$bDate=false;
							if(intval($date2[0])>$sH && intval($date2[0]<$eH)){
								$bDate=true;
							}else if((intval($date2[0])==$sH && intval($date2[1])>=$sM) || (intval($date2[0])==$eH && intval($date2[1])<=$eM)){
								$bDate=true;
							}
							if($bDate){
								$TID=$tId;
							}
						}
					}
				}
			}
		}
	}
	//echo $date1[2].','.$TID."\n";
	//echo $c."-".$bookingData[$date1[2]][$TID]."\n";
	if (!$bookingData[$date1[0]][$date1[1]][$date1[2]][$TID]){
		$bookingData[$date1[0]][$date1[1]][$date1[2]][$TID]=0;
	}
	//print_r($timeData);
	//print_r($bookingData);
	//echo $bookingData[$date1[2]][$TID]."\n";
	$c=$c-$bookingData[$date1[0]][$date1[1]][$date1[2]][$TID];
	return $c;
}
function login($contact, $email){
	// $sql="select * from rentplace ";
	// $sql.="where leadman='".$contact."' ";
	// $sql.="and email='".$email."'";

	// $rs=db_query($sql);
	
	// if(!db_eof($rs)){
	// 	$id=array();
	// 	while($r=db_fetch_array($rs)){
	// 		array_push($id,$r[id]);
	// 	}
	// 	echo join(",",$id);
	// }else{
	// 	echo '-1';
	// }

	//Modified by Hao-Tung 2022-08-28
	global $db;
	$query = "Select * "
			."From rentplace "
			."Where leadman = :contact "
			."And email = :email ";
	$stmt = $db->prepare($query);
	$stmt->execute(array(
		'contact' => $contact,
		'email' => $email
	));

	$ids = array();
	while($r = $stmt->fetch()) {
		array_push($ids, $r['id']);
	}
	if(count($ids) > 0) {
		echo join(',', $ids);
	}
	else {
		echo '-1';
	}
}
?>