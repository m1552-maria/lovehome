<?php
	// include 'config.php';
	include_once 'domail.php';
    include_once('connect_db.inc.php');
	//print_r($_REQUEST); exit;

	$options = is_array($_REQUEST["options"])?join('，',$_REQUEST["options"]):$_REQUEST["options"];
	$time  = array();
	$time  = explode(" ~ ", $_REQUEST['time']);
	$bDate = $_REQUEST["date"]." ".$time['0'];
	$eDate = $_REQUEST["date"]." ".$time['1'];

	$sql  = "SELECT start_booking FROM space where id = :places ";
	$stmt = $db->prepare($sql);
	$stmt->execute(array(
		'places' => $_REQUEST['places']
	));
	$r = $stmt->fetch();

	$overTime = "+".$r['start_booking']." month";
	
	if(strtotime(date("Y-m-t", strtotime($overTime))) < strtotime($_REQUEST['date'])){
		header("Location: index.php?incfn=inc_RentPlace.php&error=m");
		exit;
	}

	$sql  = "SELECT count(1) as _count FROM rentplace where place = :places AND bDate = :bDate AND eDate = :eDate";
	$stmt = $db->prepare($sql);
	$stmt->execute(array(
		'places' => $_REQUEST['places'],
		'bDate' => $bDate,
		'eDate' => $eDate
	));
	$r = $stmt->fetch();

	if($r["_count"] > 0){
		header("Location: index.php?incfn=inc_RentPlace.php&error=m");
		exit;
	}


	switch ($_REQUEST['act']) {
		case 'append':
			$isView = 1;
			$status = 1;
			$isPay  = 0;

			if($_REQUEST['userGroup'] == 2){
				$status = 2;
				$isPay  = 2;
			}

			$sql = "insert into rentplace(applier,leadman,address,contactman,tel,mobile,fax,email,title,peoples,isInv,invoiceNo,invoiceTitle,places,options,bDate,eDate,status,isPay,UserNote,isView) values('"
					 . $_REQUEST["applier"]."','"
					 . $_REQUEST["leadman"]."','"
					 . $_REQUEST["address"]."','"
					 . $_REQUEST["contactman"]."','"
					 . $_REQUEST["tel"]."','"
					 . $_REQUEST["mobile"]."','"
					 . $_REQUEST["fax"]."','"
					 . $_REQUEST["email"]."','"
					 . $_REQUEST["title"]."','"
					 . $_REQUEST["peoples"]."','"
					 . $_REQUEST["isInv"]."','"
					 . $_REQUEST["invoiceTitle"]."','"
					 . $_REQUEST["invoiceNo"]."','"
					 . $_REQUEST["places"]."','"
					 . $options."','"
					 . $bDate."','"
					 . $eDate."','"
					 . $status."','"
					 . $isPay."','"
					 . $_REQUEST["UserNote"]."')";
			$query = "Insert into rentplace Set "
					."applier 		= :applier, "
					."leadman 		= :leadman, "
					."address 		= :address, " 
					."contactman 	= :contactman, "
					."tel 			= :tel, "
					."mobile 		= :mobile, "
					."fax 			= :fax, "
					."email 		= :email, "
					."title 		= :title, "
					."peoples 		= :peoples, "
					."isInv 		= :isInv, "
					."invoiceNo 	= :invoiceNo, "
					."invoiceTitle 	= :invoiceTitle, "
					."places 		= :places, "
					."options 		= :options, "
					."bDate 		= :bDate, "
					."eDate 		= :eDate, "
					."status 		= :status, "
					."isPay 		= :isPay, "
					."UserNote 		= :UserNote, "
					."userGroup     = :userGroup, "
					."isView        = :isView ";
			$stmt = $db->prepare($query);
			$stmt->execute(array(
				'applier'		=>	htmlspecialchars($_REQUEST["applier"], ENT_QUOTES),
				'leadman'		=>	htmlspecialchars($_REQUEST["leadman"], ENT_QUOTES),
				'address'		=>	htmlspecialchars($_REQUEST["address"], ENT_QUOTES),
				'contactman'	=>	htmlspecialchars($_REQUEST["contactman"], ENT_QUOTES),
				'tel'			=>	htmlspecialchars($_REQUEST["tel"], ENT_QUOTES),
				'mobile'		=>	htmlspecialchars($_REQUEST["mobile"], ENT_QUOTES),
				'fax'			=>	htmlspecialchars($_REQUEST["fax"], ENT_QUOTES),
				'email'			=>	htmlspecialchars($_REQUEST["email"], ENT_QUOTES),
				'title'			=>	htmlspecialchars($_REQUEST["title"], ENT_QUOTES),
				'peoples'		=>	htmlspecialchars($_REQUEST["peoples"], ENT_QUOTES),
				'isInv'			=>	htmlspecialchars($_REQUEST["isInv"], ENT_QUOTES),
				'invoiceNo'		=>	htmlspecialchars($_REQUEST["invoiceNo"], ENT_QUOTES),
				'invoiceTitle'	=>	htmlspecialchars($_REQUEST["invoiceTitle"], ENT_QUOTES),
				'places'		=>	htmlspecialchars($_REQUEST["places"], ENT_QUOTES),
				'options'		=>	htmlspecialchars($options, ENT_QUOTES),
				'bDate'			=>	htmlspecialchars($bDate, ENT_QUOTES),
				'eDate'			=>	htmlspecialchars($eDate, ENT_QUOTES),
				'status'		=>	htmlspecialchars($status, ENT_QUOTES),
				'isPay'			=>	htmlspecialchars($isPay, ENT_QUOTES),
				'UserNote'		=>	htmlspecialchars($_REQUEST["UserNote"], ENT_QUOTES),
				'userGroup'		=>	htmlspecialchars($_REQUEST["userGroup"], ENT_QUOTES),
				'isView'        =>  htmlspecialchars($isView, ENT_QUOTES)
			));
			$id = $db->lastInsertId();
			// echo $sql; exit;
			// $rs = db_query($sql);
			// $id = mysql_insert_id();
			if($_REQUEST['userGroup'] != 2){
				// $sql       = "select title from space where id='{$_REQUEST['places']}'";
				// $rs        = db_query($sql);
				// $r    	   = db_fetch_array($rs);
				$query = "Select title from space "
						."Where id = :id ";
				$stmt = $db->prepare($query);
				$stmt->execute(array('id' => $_REQUEST['places']));
				$r = $stmt->fetch();

				$placeName = $r['title'];

				$email   = "m745@maria.org.tw";
				$fn 	 = 'mail/rentPlaceAdmMail.html'; 
				$content = file_get_contents($fn);
				$content = str_replace("{id}",   	  	$id,  $content);
				$content = str_replace("{applier}",   	htmlspecialchars($_REQUEST["applier"], ENT_QUOTES),  $content);
				$content = str_replace("{leadman}",   	htmlspecialchars($_REQUEST["leadman"], ENT_QUOTES),  $content);
				$content = str_replace("{contactman}",  htmlspecialchars($_REQUEST["contactman"], ENT_QUOTES),  $content);
				$content = str_replace("{mobile}",    	htmlspecialchars($_REQUEST["mobile"], ENT_QUOTES),   $content);
				$content = str_replace("{email}",     	htmlspecialchars($_REQUEST["email"], ENT_QUOTES),    $content);
				$content = str_replace("{title}",     	htmlspecialchars($_REQUEST["title"], ENT_QUOTES),    $content);
				$content = str_replace("{placeName}", 	$placeName,            $content);
				$content = str_replace("{bDate}",     	$bDate, 				 $content);
				$content = str_replace("{eDate}",     	$eDate, 				 $content);
				$content = str_replace("{UserNote}",  	htmlspecialchars($_REQUEST["UserNote"], ENT_QUOTES), $content);

				$subtx   = '場地借用通知';
				$rzt     = send_mail($email, $subtx, $content);
			}
			break;
		case "edit":
			$sql = "UPDATE rentplace SET 
					applier = '" . $_REQUEST["applier"] . "',
					leadman = '" . $_REQUEST["leadman"] . "',
					address = '" . $_REQUEST["address"] . "',
					contactman = '" . $_REQUEST["contactman"] . "',
					tel = '" . $_REQUEST["tel"] . "',
					mobile = '" . $_REQUEST["mobile"] . "',
					fax = '" . $_REQUEST["fax"] . "',
					email = '" . $_REQUEST["email"] . "',
					title = '" . $_REQUEST["title"] . "',
					periodTime = '" . $_REQUEST["periodTime"] . "',
					peoples = '" . $_REQUEST["peoples"] . "',
					isInv = '" . $_REQUEST["isInv"] . "',
					invoiceNo = '" . $_REQUEST["invoiceNo"] . "',
					invoiceTitle = '" . $_REQUEST["invoiceTitle"] . "',
					places = '" . $_REQUEST["places"] . "',
					options = '" . $options . "',
					bDate = '" . $bDate . "',
					eDate = '" . $eDate . "',
					UserNote = '" . $_REQUEST["UserNote"] . "' 
					WHERE id = '" . $_REQUEST["id"] . "'";
			// echo $sql; exit;
			$rs = db_query($sql);
			$id = $_REQUEST["id"];				
			break;
		default:
			# code...
			break;
	}
	
	if (session_status() === PHP_SESSION_NONE) {
		session_start();
	}
	if(isset($_SESSION['accessible_rentplaceIds']) && is_array($_SESSION['accessible_rentplaceIds'])) {
		$_SESSION['accessible_rentplaceIds'][] = $id;
	}
	else {
		$_SESSION['accessible_rentplaceIds'] = array($id);
	}
	
	header("Location: index.php?incfn=inc_rentplaceok.php&id=$id");	
	
?>