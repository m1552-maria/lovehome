<?php
include_once('config.inc.php');
include_once('connect_db.inc.php');
//   $rs = db_query("select * from menus where parent='0' order by priority",$conn);
// 	while($r=db_fetch_array($rs)) {
// 		$id[$r[CatNo]]  = $r[cat_name];	$url[$r[CatNo]] = $r[url]; $channel[$r[CatNo]]=$r[channel];		
// 		$rs2 = db_query("select * from menus where parent='$r[CatNo]' order by priority",$conn);
// 		if(!db_eof($rs2)){
// 			while($r2=db_fetch_array($rs2)) {
// 				$id[$r2[CatNo]] = $r2[cat_name]; $url[$r2[CatNo]] = $r2[url]; $channel[$r2[CatNo]]=$r2[channel];
// 				$rs3 = db_query("select * from menus where parent='$r2[CatNo]' order by priority",$conn);
// 				if(!db_eof($rs3)) {
// 				  while($r3=db_fetch_array($rs3)) {
// 						$id[$r3[CatNo]] = $r3[cat_name]; $url[$r3[CatNo]] = $r3[url]; $channel[$r3[CatNo]]=$r3[channel];
// 						$tree[$r[CatNo]][$r2[CatNo]][$r3[CatNo]]=$r3[cat_name];
// 					}
// 				} else $tree[$r[CatNo]][$r2[CatNo]] = $r2[cat_name];
// 			} 
// 		} else $tree[$r[CatNo]]=$r[cat_name]; 
// 	}		
  //print_r($url); exit; 


  $query = "SELECT "
			."	M1.CatNo as CatNo0, M1.cat_name as catName0, M1.channel as channel0, M1.url as url0, "
			."	M2.CatNo as CatNo1, M2.cat_name as catName1, M2.channel as channel1, M2.url as url1, "
			."	M3.CatNo as CatNo2, M3.cat_name as catName2, M3.channel as channel2, M3.url as url2, "
			."	M4.CatNo as CatNo3, M4.cat_name as catName3, M4.channel as channel3, M4.url as url3 "
			."FROM `menus` as M1 "
			."LEFT JOIN menus as M2 "
			."ON M2.parent = M1.CatNo "
			."AND M2.status = 1 "
			."LEFT JOIN menus as M3 "
			."ON M3.parent = M2.CatNo "
			."AND M3.status = 1 "
			."LEFT JOIN menus as M4 "
			."ON M4.parent = M3.CatNo "
			."AND M4.status = 1 "
			."WHERE M1.parent = '0' "
			."AND M1.status = 1 "
			."ORDER BY M1.priority, M1.CatNo, M2.priority, M2.CatNo, M3.priority, M3.CatNo, M4.priority";
	
	$stmt = $db->prepare($query);
	$stmt->execute();

	$tree = [];
	$indexes = [null, null, null, null];
	while($row = $stmt->fetch()) {
		$depth = 0;

		for($i = 0; $i < 4; $i++) {
			$indexes[$i] = $row['CatNo'.$i];			
			if(!is_null($row['CatNo'.$i])) {
				$depth = $i;
			}
		}

		//確認本節點的祖先都已定義，若未定義，這些祖先節點值一定要是array，不是字串
		for($i = 0; $i < $depth; $i++) {
			if($i === 0 && !isset($tree[$indexes[0]])) {
				$tree[$indexes[0]] = [];
			}
			else if($i === 1 && !isset($tree[ $indexes[0] ][ $indexes[1] ])) {
				$tree[ $indexes[0] ][ $indexes[1] ] = [];
			}
			else if($i === 2 && !isset($tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ])) {
				$tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ] = [];
			}
			$checkingNodeKey = $row['CatNo'.$i];
			if(!isset($id[$checkingNodeKey])) {
				$id[$checkingNodeKey] = $row['catName'.$i]; 
				$url[$checkingNodeKey] = $row['url'.$i]; 
				$channel[$checkingNodeKey] = $row['channel'.$i];
			}
		}

		$nodeKey = $row['CatNo'.$depth];
		$id[$nodeKey] = $row['catName'.$depth]; 
		$url[$nodeKey] = $row['url'.$depth]; 
		$channel[$nodeKey] = $row['channel'.$depth];

		switch($depth) {
			case 0:
				if(!isset($tree[ $indexes[0] ])) {
					$tree[ $indexes[0] ] = $id[$nodeKey];
				}
				break;
			case 1:
				if(!isset($tree[ $indexes[0] ][ $indexes[1] ])) {
					$tree[ $indexes[0] ][ $indexes[1] ] = $id[$nodeKey];
				}
				break;
			case 2:
				if(!isset($tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ])) {
					$tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ] = $id[$nodeKey];
				}
				break;
			case 3:
				if(!isset($tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ][ $indexes[3] ])) {
					$tree[ $indexes[0] ][ $indexes[1] ][ $indexes[2] ][ $indexes[3] ] = $id[$nodeKey];
				}
				break;
		}
	}
?>
<link href="/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
<script src="/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>

<ul id="MenuBar1" class="MenuBarHorizontal">
<?php foreach($tree as $k=>$v) { 
	   echo "<li><a href='".($channel[$k]==0?($url[$k]?"index.php?incfn=$url[$k]":"#"):$url[$k])."' class='MenuBarItemSubmenu'>$id[$k]</a>";
		 if (is_array($v)) {
		 	 echo "<ul>";
			 foreach($v as $k2=>$v2) {	 
				 if (is_array($v2)) {
				   echo "<li><a href='".($channel[$k2]==0?($url[$k2]?"index.php?incfn=$url[$k2]":"#"):$url[$k2])."' class='MenuBarItemSubmenu'>$id[$k2]</a>";
				   echo "<ul>";
					 foreach($v2 as $k3=>$v3) echo "<li><a href='".($channel[$k3]==0?($url[$k3]?"index.php?incfn=$url[$k3]":"#"):$url[$k3])."'>$v3</a></li>";
					 echo "</ul></li>";
				 } else echo "<li><a href='".($channel[$k2]==0?($url[$k2]?"index.php?incfn=$url[$k2]":"#"):$url[$k2])."'>$v2</a></li>";
			 }	 
		   echo "</ul></li>";
	   }
   } ?>  
</ul>

<!-- ul id="MenuBar1" class="MenuBarHorizontal">
	<li><a href="#" class="MenuBarItemSubmenu"><span class="style2 style3">捐款捐物</span><span class="style4">．</span>徵信</a>
    <ul>
      <li style="text-align:left"><a href="donate.php" >我要捐款</a></li>
      <li style="text-align:left"><a href="find.php" >捐款徵信</a></li>
      <li style="text-align:left"><a href="stuff.php" >我要捐物</a></li>
      <li style="text-align:left"><a href="credit_2.php" >捐物徵信</a></li>
      <li style="text-align:left"><a href="../download/99年度財務報表.pdf" target="_blank">財務報表徵信</a></li>
      <li style="text-align:left"><a href="result.php">服務成果徵信</a></li>
    </ul>
    </li>
</ul -->
<script type="text/javascript">
	var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"/SpryAssets/SpryMenuBarDownHover.gif", imgRight:"/SpryAssets/SpryMenuBarRightHover.gif"});
	/*
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24736342-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })(); */
</script>